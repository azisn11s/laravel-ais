<?php

namespace Hybrings\Providers;

use Illuminate\Support\ServiceProvider;
use Hybrings\Http\ViewComposers\AppMenuComposer;
use Hybrings\Http\ViewComposers\AppProfileComposer;
use Hybrings\Http\ViewComposers\UserLoggedComposer;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \View::composer(['layouts._left-bar'], AppMenuComposer::class);
        \View::composer(['layouts.app','layouts._footer', 'layouts._header', 'auth-sentinel.login'], AppProfileComposer::class);
        \View::composer(['layouts._header', 'layouts._user-panel'], UserLoggedComposer::class);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
