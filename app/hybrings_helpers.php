<?php

/*
 |--------------------------------------------------------------------------
 | HASANAH LAND Helpers
 |--------------------------------------------------------------------------
 |
 | Here, we will define the global functions (helpers) that will be
 | used in our application. Give a try to add your awesome helpers!
 |
 | 
 |
 */

 use Hybrings\Classes\Models\User;



 if (! function_exists('user_info')) {
    /**
     * Get logged in user info.
     *
     * @param  string  $column  The column in `users` table.
     * @return mixed
     */
    function user_info($column = null)
    {
        if ($user = Sentinel::check()) {
            // Return the User instance if $column is null.
            if (is_null($column)) {
                return $user;
            }

            // If $column is 'role', we will return the Role instance that belong to the user.
            if ('role' == $column) {
                return user_info()->roles[0];
            }

            // Return the value of the column.
            return $user->{$column};
        }

        // Return null if user is not logged in.
        return null;
    }
}


 if (! function_exists('date_sql')) {
    /**
     * Convert date to SQL date format.
     *
     * @param  string  $date  The date in `d/m/Y` to be converted.
     * @return string date 'Y-m-d'
     */
    function date_sql($date = null)
    {
        $convertedDate = '';
        if (!is_null($date)) {
            $convertedDate = date_format(date_create_from_format('d/m/Y', $date), 'Y-m-d');
        }else{
            $convertedDate = date('Y-m-d');
        }

        return $convertedDate;
    }
}


if (! function_exists('custom_date')) {
    /**
     * Convert date to custome format.
     *
     * @param  string  $date  .
     * @param  string $src_format as source date format
     * @param  string $final_format as desired date format
     * @return string date converted
     *
     * ref: https://www.w3schools.com/sql/func_date_format.asp
     */
    function custom_date($date = null, $src_format = 'Y-m-d', $final_format = 'd/m/Y')
    {
        if(!is_null($date)){
            $convertedDate = date_format(date_create_from_format($src_format, $date), $final_format);
        }else{
            $convertedDate = date_format(date_create_from_format($src_format, date('Y-m-d')), $final_format);
        }
        

        return $convertedDate;
    }
}


if (! function_exists('reformat_money')) {
    /**
     * Convert money format.
     *
     * @param  string  $value as XXX,XXX.00
     * @return string money XXXXXX
     *
     */
    function reformat_money($value)
    {
        $output     = "";
        $re_value   = substr($value, 0, strpos($value, ","));
        $output     = str_replace(array('.', ','), '', $re_value);

        return $output;
    }
}


if (! function_exists('trim_underscore')) {
    /**
     * Trim underscore character at right side of string.
     *
     * @param  string $value as XXX___
     * @return string XXX
     *
     */
    function trim_underscore($value)
    {
        $output = preg_replace('/[_\s]/', '', $value);

        return $output;
    }
}


if (! function_exists('random_number')) {
    /**
     * Random number with specific length.
     *
     * @param  string $length
     * @return string
     *
     */
    function random_number($length) {
        $result = '';

        for($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }

        return $result;
    }
}


