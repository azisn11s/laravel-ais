<?php

namespace Hybrings\Classes;

use Illuminate\Database\Eloquent\Model;

class AppProfile extends Model
{
    protected $fillable = ['app_name','jargon','company_name','year','app_version'];
}
