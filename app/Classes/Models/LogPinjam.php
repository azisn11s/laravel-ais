<?php

namespace Hybrings\Classes\Models;

use Illuminate\Database\Eloquent\Model;

class LogPinjam extends Model
{
    
    protected $table = 'log_pinjam';

    protected $primaryKey = 'nomor_pinjam';

    public $incrementing = false;

    protected $fillable = [
    			'nomor_pinjam',
    			'tanggal_pinjam',
    			'total_pinjam',
    			'pemohon_id', //nip
    			'user_id',
    			'note'
    		];



    public function pemohon()
    {
        return $this->belongsTo(Pemohon::class, 'pemohon_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
