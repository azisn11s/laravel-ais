<?php

namespace Hybrings\Classes\Models;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $table = 'mst_album';

    protected $fillable = ['no_album','lemari','rak','blok', 'record_status'];

    
    public function bukuTanah()
    {
        return $this->hasMany(BukuTanah::class);
    }


    public function warkah()
    {
        return $this->hasMany(Warkah::class);
    }



}
