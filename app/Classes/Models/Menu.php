<?php

namespace Hybrings\Classes\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = ['name', 'url', 'uri', 'icon', 'sort', 'parent_id', 'is_root', 'is_active'];


    public function roles()
    {
    	return $this->belongsToMany(Role::class);
    }
}
