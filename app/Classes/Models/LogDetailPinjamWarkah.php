<?php

namespace Hybrings\Classes\Models;

use Illuminate\Database\Eloquent\Model;

class LogDetailPinjamWarkah extends Model
{
    protected $table = 'log_detail_pinjam_warkah';
    
    public $incrementing = false;

    protected $fillable = [
    			'nomor_pinjam',
    			'warkah_id',
    			'jumlah_buku',
    		];



    public function logPinjam()
    {
        return $this->belongsTo(LogPinjam::class, 'nomor_pinjam');
    }

    public function warkah()
    {
        return $this->belongsTo(Warkah::class, 'warkah_id');
    }


}
