<?php

namespace Hybrings\Classes\Models;

use Illuminate\Database\Eloquent\Model;

class Warkah extends Model
{
    
    protected $table = 'mst_warkah';

    protected $fillable = [
    			'nw_barcode',
    			'jenis',
    			'no_warkah',
    			'tahun',
    			'album_id',
    			'status',
    			'record_status'
    		];


    public function album()
    {
        return $this->belongsTo(Album::class, 'album_id');
    }


}
