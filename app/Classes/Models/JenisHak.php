<?php

namespace Hybrings\Classes\Models;

use Illuminate\Database\Eloquent\Model;

class JenisHak extends Model
{
    

    protected $table = 'mst_jenis_hak';

    protected $fillable = [
    			'kode_hak',
    			'nama_hak',
    			'record_status'
    		];


    public function bukuTanah()
    {
        return $this->hasMany(BukuTanah::class);
    }

}
