<?php

namespace Hybrings\Classes\Models;

use Illuminate\Database\Eloquent\Model;

class BukuTanah extends Model
{
    protected $table = 'mst_buku_tanah';

    protected $fillable = [
    	'nb_barcode',
    	'hak_id', //jenis_hak
    	'no_hak',
    	'album_id',
    	'luas',
    	'pemegang_hak',
    	'desa_id',
    	'status',
    	'record_status'
    ];


    public function album()
    {
        return $this->belongsTo(Album::class, 'album_id');
    }


    public function desa()
    {
        return $this->belongsTo(MasterDesa::class, 'desa_id');
    }


    public function jenisHak()
    {
        return $this->belongsTo(JenisHak::class, 'hak_id');
    }

    public function detailPinjamBukuTanah()
    {
        return $this->hasMany(LogDetailPinjamBukuTanah::class);
    }

    public function detailKembaliBukuTanah()
    {
        return $this->hasMany(LogDetailKembaliBukuTanah::class);
    }


}
