<?php

namespace Hybrings\Classes\Models;

use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{
    
    protected $table = 'mst_gender';

    protected $fillable = ['kode_jekel', 'nama_jekel'];
    
}
