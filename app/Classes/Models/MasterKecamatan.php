<?php

namespace Hybrings\Classes\Models;

use Illuminate\Database\Eloquent\Model;

class MasterKecamatan extends Model
{
    protected $table = 'mst_kecamatan';

    protected $fillable = ['kode_kecamatan','nama_kecamatan','record_status'];


    public function desa()
    {
        return $this->hasMany(MasterDesa::class);
    }

}
