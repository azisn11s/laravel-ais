<?php

namespace Hybrings\Classes\Models;

use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model
{

    protected $table = 'mst_jabatan';

    protected $fillable = [
    	'kode_jabatan',
    	'nama_jabatan',
    	'record_status'
    ];


    public function pemohon()
    {
        return $this->hasMany(Pemohon::class);
    }
    
}
