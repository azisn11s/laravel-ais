<?php

namespace Hybrings\Classes\Models;

use Illuminate\Database\Eloquent\Model;

class LogDetailKembaliWarkah extends Model
{
    protected $table = 'log_detail_kembali_warkah';
    
    public $incrementing = false;

    protected $fillable = [
    			'nomor_kembali',
    			'warkah_id',
    			'jumlah_buku'
    		]; 


    public function logKembali()
    {
        return $this->belongsTo(LogKembali::class, 'nomor_kembali');
    }

    public function warkah()
    {
        return $this->belongsTo(Warkah::class, 'warkah_id');
    }

}
