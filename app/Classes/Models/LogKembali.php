<?php

namespace Hybrings\Classes\Models;

use Illuminate\Database\Eloquent\Model;

class LogKembali extends Model
{
    
    protected $table = 'log_kembali';

    protected $primaryKey = 'nomor_kembali';

    public $incrementing = false;

    protected $fillable = [
    			'nomor_kembali',
    			'tanggal_kembali',
    			'total_kembali',
    			'pemohon_id',
    			'user_id',
    			'note'
    		];



    public function pemohon()
    {
        return $this->belongsTo(Pemohon::class, 'pemohon_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
