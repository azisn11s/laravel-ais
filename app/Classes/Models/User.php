<?php

namespace Hybrings\Classes\Models;


use Sentinel;
use Hash;
use DB;
use Closure;
use Cartalyst\Sentinel\Users\EloquentUser as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{

    protected $fillable = [
        'username',
        'email',
        'password',
        'permissions',
        'type',
        'avatar',
    ];

    
	/**
     * Authenticates a user, with "remember" flag.
     *
     * @param  string  $login
     * @param  string  $password
     * @param  bool  $remember
     * @return \Cartalyst\Sentinel\Users\UserInterface|bool
     */
    public static function authenticate($login, $password, $remember = false)
    {
        if (! $user = static::findByLogin($login)) {
            return false;
        }

        if (Hash::check($password, $user->password)) {
            return Sentinel::login($user, $remember);
        }

        return false;
    }

    /**
     * Find user by given login.
     *
     * @param  string  $login
     * @return \App\Models\User|null
     */
    public static function findByLogin($login)
    {
        return static::where(function ($query) use ($login) {
            $query->orWhere('email', $login)->orWhere('username', $login);
        })->first();
    }

    
}
