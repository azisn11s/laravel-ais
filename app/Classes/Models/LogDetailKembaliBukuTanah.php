<?php

namespace Hybrings\Classes\Models;

use Illuminate\Database\Eloquent\Model;

class LogDetailKembaliBukuTanah extends Model
{
    protected $table = 'log_detail_kembali_buku_tanah';

    public $incrementing = false;

    protected $fillable = [
    			'nomor_kembali',
    			'buku_tanah_id', //nb_barcode
                'nomor_pinjam',
    			'jumlah_buku'
    		]; 



    public function logKembali()
    {
        return $this->belongsTo(LogKembali::class, 'nomor_kembali');
    }

    public function bukuTanah()
    {
        return $this->belongsTo(BukuTanah::class, 'buku_tanah_id');
    }

    public function logDetailPinjam()
    {
        return $this->belongsTo(LogDetailPinjamBukuTanah::class, 'nomor_pinjam');
    }

}
