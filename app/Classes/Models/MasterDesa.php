<?php

namespace Hybrings\Classes\Models;

use Illuminate\Database\Eloquent\Model;

class MasterDesa extends Model
{
    protected $table = 'mst_desa';

    protected $fillable = ['kode_desa','nama_desa','record_status','id_kecamatan'];



    public function kecamatan()
    {
        return $this->belongsTo(MasterKecamatan::class, 'id_kecamatan');
    }


    public function bukuTanah()
    {
        return $this->hasMany(BukuTanah::class);
    }


}
