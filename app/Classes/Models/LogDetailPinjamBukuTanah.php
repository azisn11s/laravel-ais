<?php

namespace Hybrings\Classes\Models;

use Illuminate\Database\Eloquent\Model;

class LogDetailPinjamBukuTanah extends Model
{    
    public $incrementing = false;

    protected $table = "log_detail_pinjam_buku_tanah";

    protected $primaryKey = 'nomor_pinjam';

    protected $fillable = [
    			'nomor_pinjam',
    			'buku_tanah_id',
    			'jumlah_buku',
    		];


    public function logPinjam()
    {
        return $this->belongsTo(LogPinjam::class, 'nomor_pinjam');
    }

    public function bukuTanah()
    {
        return $this->belongsTo(BukuTanah::class, 'buku_tanah_id');
    }
}
