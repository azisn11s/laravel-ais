<?php

namespace Hybrings\Classes\Models;

use Illuminate\Database\Eloquent\Model;

class Pemohon extends Model
{
    
    protected $table = 'mst_pemohon';

    protected $fillable = [
					    	'nip',
					    	'nama_lengkap',
					    	'jekel_id',
					    	'jabatan_id',
					    	'alamat',
					    	'record_status'
					    ]; 


	public function gender()
    {
        return $this->belongsTo(Gender::class, 'jekel_id');
    }

    public function jabatan()
    {
        return $this->belongsTo(Jabatan::class, 'jabatan_id');
    }

}
