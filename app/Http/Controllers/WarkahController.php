<?php

namespace Hybrings\Http\Controllers;

use Illuminate\Http\Request;

use Hybrings\Http\Requests;

use Hybrings\Classes\Models\Album;
use Hybrings\Classes\Models\Warkah;

use Datatables;

class WarkahController extends Controller
{
    

    public function index(Request $request)
    {
    	$data['no_album'] = Album::where('record_status','A')->pluck('no_album', 'id');;
    	// $data['jabatan'] = Jabatan::where('record_status','A')->pluck('nama_jabatan','id');

    	$headerContent = array(
            'title' => 'Warkah',
            'description' => 'Daftar semua Warkah',
            'breadcrumb' => array(
                array(
                    'name'=>'Warkah',
                    'routeName'=>'',
                    'icon'=>'fa-home',
                    'active'=>'',
                ),
                array(
                    'name'=>'List',
                    'routeName'=>'warkah.index',
                    'icon'=>'',
                    'active'=>'active',
                ), 
            ),
        );

    	return view('warkah.index')->with(compact('headerContent','data'));
    }


    public function bind(){
        $data = Warkah::with('album')->where('record_status', 'A')->get();

        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return "<a href='javascript:void(0)' onclick='edit(".$data->id.")' class='btn btn-xs btn-primary'><i class='glyphicon glyphicon-edit'></i> Edit</a>
                        <a href='javascript:void(0)' onclick='delConfirm(".$data->id.")' class='btn btn-xs btn-danger'>
                        <i class='glyphicon glyphicon-trash'></i> Delete</a>";
            })
            ->make(true);
    }


    public function store(Request $request)
    {
    	$newWarkah = new Warkah;
    	$newWarkah->fill($request->all())->save();

    	return response()->json([
                    "success"=> "Warkah telah ditambahkan."
                ]);
    }


    public function edit($id){
        $warkah = Warkah::findOrFail($id);

        return response()->json([
            'url_action'=>url('warkah/update/'.$warkah->id),
            'warkah'=>$warkah
        ]);
    }


    public function update(Request $request, $id)
    {
    	$warkah = Warkah::findOrFail($id);
    	$inputData = $request->all();

    	if(!$warkah->update($inputData))
            return response()->json([
                "errors"=> "Error menyimpan data warkah."
            ]);

        return response()->json([
                    "success"=> "Warkah telah diperbaharui."
                ]);
    }


    public function destroy(Request $request)
    {
        $data = Warkah::findOrFail($request->id);
        
        if($data->update(['record_status'=>'D']))
            return response()->json([
                        "success"=> "Data telah dihapus."
                    ]);
    }



}
