<?php

namespace Hybrings\Http\Controllers;

use Illuminate\Http\Request;

use Hybrings\Http\Requests;
use Sentinel;
use Session;

use Illuminate\Support\Facades\File;

use \Form;
use Datatables;

class UserController extends Controller
{
	private $user;

    private $full_name = 'Administrator';
    private $username;
    private $user_type;
    private $email;
    private $phone;
    private $address;
    private $province_id;
    private $city_id;

	function __construct()
	{
		$this->user = Sentinel::getUser();
		$this->findUser(Sentinel::getUser());
	}

    /**
     * Show logged-in user profile
     */
    public function index()
    {
    	$headerContent = array(
            'title' => 'User Profile',
            'description' => '',
            'breadcrumb' => array(
                array(
                    'name'=>'User',
                    'routeName'=>'',
                    'icon'=>'fa-home',
                    'active'=>'',
                ),
            ),
        );
   	

        $data['full_name']	= $this->full_name;
        $data['user_type']	= $this->user_type;
        $data['username']	= $this->username;
        $data['email']		= $this->email;
        $data['phone']		= $this->phone;
        $data['address']	= $this->address;
        $data['province_id']	= $this->province_id;
        $data['city_id']	= $this->city_id;

    	return view('user.index')->with(compact('headerContent', 'data'));

    }

    private function findUser($user)
    {

    	switch ($user->type) {
    		case '':
    			# code here...
    			break;

    		/*case 'agent':
    			$agent = $this->findAgent($user->id);
    			$this->full_name = $agent->agent_name;
		        $this->user_type = 'Agent';
		        $this->username = $user->username;
		        $this->email = $user->email;
		        $this->phone = $agent->phone;
		        $this->address = $agent->address;
		        $this->province_id = $agent->province_id;
		        $this->city_id = $agent->city_id;
		        $this->address = $agent->address;
    			break;*/
    		
    		default:
		        $this->user_type = $user->type;
		        $this->username = $user->username;
		        $this->email = $user->email;
    			break;
    	}

    }

    /*private function findProjectOwner($userId)
    {
    	$owner = ProjectOwner::where('user_id', $userId)->first();
    	
    	return $owner;
    }*/


    public function update(Request $request)
    {
    	$credentialsUpdate = [
    		'username' => $request->username,
    		'email' => $request->email,
    	];
    	
    	// Change Password
    	if ($request->has('old_password') && $request->has('new_password')) {

    		$hasher = Sentinel::getHasher();

    		if(!$hasher->check($request->old_password, $this->user->password)){
    			Session::flash('error_message', 'Password lama tidak sesuai.');
    			return back()->withInput();
    		}

    		$credentialsUpdate['password'] = $request->new_password;
    	}

    	// Change Profile Pict.
    	if ($request->hasFile('avatar')){
            // menambil cover yang diupload berikut ekstensinya
            $filename = null;
            $uploaded_avatar = $request->file('avatar');
            $extension = $uploaded_avatar->getClientOriginalExtension();

            // membuat nama file random dengan extension
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'avatar' . DIRECTORY_SEPARATOR . 'user';

            // memindahkan file ke folder public/img
            $uploaded_avatar->move($destinationPath, $filename);

            // hapus cover lama, jika ada
            if ($this->user->avatar) {
                $old_cover = $this->user->avatar;
                $filepath = public_path('images/avatar/user/'.$this->user->avatar);
                unlink($filepath);
            }

            // ganti field cover dengan cover yang baru
            $credentialsUpdate['avatar'] = $filename;
         }


        /*if ($this->user->type != 'admin') {

        	switch ($this->user->type) {
        		case 'agent':
        			$this->updateAgent($request);
        			break;

        		default:
        			# code...
        			break;
        	}
        }*/
         

    	if(Sentinel::validForUpdate($this->user, $credentialsUpdate)){
    		$user = Sentinel::update($this->user, $credentialsUpdate);
    	}else{
    		Session::flash('error_message', 'Gagal memperbaharui data user.');
    		return back()->withInput();
    	}


    	return back();

    }

    /*private function updateAgent($request)
    {
		$data = Agent::where('user_id', $this->user->id)->get()->first();
		$data->agent_name = $request->full_name;
		$data->address = $request->address;
		$data->phone = $request->phone;
		$data->province_id = $request->province_id;
		$data->city_id = $request->user_city_id;
		$data->update();
    }*/



    /*public function getCityList($province_id)
    {        
        $cities = Province::find($province_id)->mst_city()->get()
                    ->lists('city_name','city_id');
        
        $dropDownList = Form::select(
            'user_city_id', $cities, '', 
            array(
                'id' => 'city', 
                'class' => 'form-control js-selectize')
        );
        
        echo $dropDownList;
    }*/


}
