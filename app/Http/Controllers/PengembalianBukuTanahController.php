<?php

namespace Hybrings\Http\Controllers;

use Illuminate\Http\Request;

use Hybrings\Http\Requests;

use DB;
use Sentinel;
use Datatables;
use Carbon\Carbon;
use Hybrings\Classes\Models\BukuTanah;
use Hybrings\Classes\Models\Pemohon;
use Hybrings\Classes\Models\LogPinjam;
use Hybrings\Classes\Models\LogKembali;
use Hybrings\Classes\Models\LogDetailPinjamBukuTanah;
use Hybrings\Classes\Models\LogDetailKembaliBukuTanah;


class PengembalianBukuTanahController extends Controller
{
    public function index()
    {
    	// Base Form
        $data['nomor_kembali'] = $this->getNomorKembali();

        $data['tanggal_kembali'] = custom_date();

        $headerContent = array(
            'title' => 'Transaksi Pengembalian Buku Tanah',
            'description' => '',
            'breadcrumb' => array(
                array(
                    'name'=>'Transaksi Buku Tanah',
                    'routeName'=>'',
                    'icon'=>'fa-home',
                    'active'=>'',
                ),
                array(
                    'name'=>'List',
                    'routeName'=>'pengembalian-bt.index',
                    'icon'=>'',
                    'active'=>'active',
                ), 
            ),
        );

        return view('buku-tanah.pengembalian')->with(compact('headerContent','data'));

    }

    public function getNomorKembali()
    {
        return random_number(2).date('mdis');
    }

    public function search($term)
    {
        $result = [];

        $listBukuTanah = BukuTanah::where('nb_barcode', 'like', '%'.$term.'%')
                            ->orWhere('no_hak', 'like', '%'.$term.'%')
                            ->orWhere('pemegang_hak', 'like', '%'.$term.'%')
                            ->take(10)->get();

        foreach($listBukuTanah as $bukuTanah)
        {
            $result[] = [
                'id'    => $bukuTanah->id,
                'value' => $bukuTanah->nb_barcode.' - Pemegang Hak: '.$bukuTanah->pemegang_hak
            ];
        }

        return response()->json($result);

    }

    public function store(Request $request)
    {
        // 1. Cari dan buat instance pemohon | nip
        $pemohon = Pemohon::where('nip', $request->nip)->first();

        if(!$pemohon){
            return response()->json([
                'errors' => "Tidak ada data Pemohon dengan NIP $request->nip",
            ]);
        }

        // 2. Cari & Jika tidak ada, buat instance log kembali | nomor_kembali

        
        $logKembali = LogKembali::where('nomor_kembali', $request->nomor_kembali)->first();

        if(!$logKembali){
            $newLogKembali = new LogKembali;
            $newLogKembali->nomor_kembali = $request->nomor_kembali;
            $newLogKembali->tanggal_kembali = date_sql($request->tanggal_kembali);
            $newLogKembali->pemohon_id = $pemohon->id;
            $newLogKembali->note = $request->note;
            $newLogKembali->user_id = Sentinel::getUser()->id;
            // $newLogPinjam->total_pinjam
        }

        // 3. Cek Buku Tanah
        $bukuTanah = BukuTanah::find($request->buku_tanah_id);

        if(!$bukuTanah){
            return response()->json([
                'errors' => "Tidak ada data Buku Tanah yang dicari.",
            ]);
        }else{
            if($bukuTanah->status === '1'){
                return response()->json([
                    'errors' => "Buku Tanah ini tidak sedang dipinjam.",
                ]);
            }
        }

        // 4. Buat instance log detail peminjaman
        $kembali = $logKembali ?: $newLogKembali;

        // Buku yang dikembalikan harus sedang dalam peminjaman
        $logPinjam = LogDetailPinjamBukuTanah::where('buku_tanah_id', $request->buku_tanah_id)
        			->orderBy('created_at', 'DESC')->first();

        // 5. Peminjaman Buku Tanah
        $logDetailPengembalian = LogDetailKembaliBukuTanah::where('nomor_kembali', $kembali->nomor_kembali)->get();

        if($logDetailPengembalian->count() > 0){

            // Jika Buku Tanah masih dipinjam
            if($logDetailPengembalian->contains('buku_tanah_id', $bukuTanah->id)){
                return response()->json([
                    'errors' => "Buku Tanah yang dimaksud sekarang sudah Anda kembalikan.",
                ]);
            }

            // Update Jumlah Pinjam
            $kembali->total_kembali = $logDetailPengembalian->count() + 1;

        }else{
            $kembali->total_kembali = 1;
        }

        // 6. Transaksi Peminjaman
        DB::beginTransaction();
        try {

            $bukuTanah->status = 1;
            $bukuTanah->save();

            $kembali->save();

            $newLogKembali = new LogDetailKembaliBukuTanah;
            $newLogKembali->nomor_kembali = $kembali->nomor_kembali;
            $newLogKembali->buku_tanah_id = $request->buku_tanah_id;
            $newLogKembali->nomor_pinjam = $logPinjam->nomor_pinjam;
            $newLogKembali->jumlah_buku = 1;
            $newLogKembali->save();

            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            DB::rollback();
        }

        if ($success) {
            return response()->json([
                    'success' => "Berhasil menyimpan data pengembalian Buku Tanah.",
                ]);
        }else{
            return response()->json([
                    'errors' => "Gagal menyimpan data Pengembalian. Mohon mencoba kembali.",
                ]);
        }


    }

    public function listPengembalian($nomor_kembali)
    {
    	$data = LogDetailKembaliBukuTanah::with('logKembali', 'bukuTanah', 'bukuTanah.jenisHak', 'bukuTanah.desa', 'bukuTanah.desa.kecamatan', 'logDetailPinjam', 'logDetailPinjam.logPinjam')->where('nomor_kembali', $nomor_kembali)->get();

    	// dd($data);

        return Datatables::of($data)
        	->editColumn('log_detail_pinjam.log_pinjam.tanggal_pinjam', function ($data) {
                return custom_date($data->logDetailPinjam->logPinjam->tanggal_pinjam, 'Y-m-d H:i:s');
            })
            ->addColumn('lama_pinjam', function ($data) {
                $tanggalPinjam = new Carbon($data->logDetailPinjam->logPinjam->tanggal_pinjam);
                $now = Carbon::now();
                $lamaPinjam = ($tanggalPinjam->diff($now)->days < 1) ? 'Hari ini' : $tanggalPinjam->diffForHumans($now);
                 
                return $lamaPinjam;
            })
            ->addColumn('action', function ($data) {
                return "<a href='javascript:void(0)' onclick='delConfirm(".$data->nomor_kembali.",".$data->buku_tanah_id.")' class='btn btn-xs btn-danger'>
                        <i class='glyphicon glyphicon-trash'></i> Delete</a>";
            })
            ->make(true);
    }

    public function allPengembalian()
    {
    	$data = LogKembali::with('pemohon')->get();

        return Datatables::of($data)
            ->editColumn('tanggal_kembali', function ($data) {
                return custom_date($data->tanggal_kembali, 'Y-m-d H:i:s');
            })
            ->filterColumn('tanggal_kembali', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(tanggal_kembali, '%d/%m/%Y') like ?", ["%$keyword%"]);
            })
            ->make(true);
    }

    public function detailDestroy(Request $request)
    {     
        $data = LogDetailKembaliBukuTanah::where('nomor_kembali', $request->id)
                ->where('buku_tanah_id', $request->buku_tanah_id);
                // ->firstOrFail();

        $bukuTanah = BukuTanah::find($request->buku_tanah_id);
        $bukuTanah->status = 2;
        
        $pengembalian = LogKembali::where('nomor_kembali', $request->id)->first();
        $pengembalian->total_pinjam = $pengembalian->total_pinjam + 1;
        
        if($data->delete() && $pengembalian->save() && $bukuTanah->save())
            return response()->json([
                        "success"=> "Data telah dihapus."
                    ]);
    
    }


}
