<?php

namespace Hybrings\Http\Controllers;

use Illuminate\Http\Request;

use Hybrings\Http\Requests;

use Hybrings\Classes\Models\BukuTanah;
use Hybrings\Classes\Models\JenisHak;
use Hybrings\Classes\Models\Album;
use Hybrings\Classes\Models\MasterDesa;
use Hybrings\Classes\Models\MasterKecamatan;

use Datatables;
use \Form;

class BukuTanahController extends Controller
{
    
    public function index(Request $request)
    {
    	$data['hak'] 		= JenisHak::where('record_status','A')->pluck('nama_hak','id');
    	$data['kecamatan'] 	= MasterKecamatan::where('record_status','A')->pluck('nama_kecamatan','id');
    	$data['desa'] 		= array(['nama_desa'=>'', 'id'=>'']);
    	$data['album'] 		= Album::where('record_status','A')->pluck('no_album','id');

    	$headerContent = array(
            'title' => 'Buku Tanah',
            'description' => 'Daftar semua Buku Tanah',
            'breadcrumb' => array(
                array(
                    'name'=>'Buku Tanah',
                    'routeName'=>'',
                    'icon'=>'fa-home',
                    'active'=>'',
                ),
                array(
                    'name'=>'List',
                    'routeName'=>'buku-tanah.index',
                    'icon'=>'',
                    'active'=>'active',
                ), 
            ),
        );

    	return view('buku-tanah.index')->with(compact('headerContent','data'));
    }


    public function bind(){
        $data = BukuTanah::with('album', 'desa', 'jenisHak', 'desa.kecamatan')
        		->where('record_status', 'A')
        		->get();

        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return "<a href='javascript:void(0)' onclick='edit(".$data->id.")' class='btn btn-xs btn-primary'><i class='glyphicon glyphicon-edit'></i> Edit</a>
                        <a href='javascript:void(0)' onclick='delConfirm(".$data->id.")' class='btn btn-xs btn-danger'>
                        <i class='glyphicon glyphicon-trash'></i> Delete</a>";
            })
            ->editColumn('status', function ($data) {
                if($data->status === '1'){
                    return '<span class="label label-success">TERSEDIA</span>';
                }else{
                    return '<span class="label label-warning">DIPINJAM</span>';
                } 
            })
            ->make(true);
    }

    public function getDesaList($kecamatan_id)
    {
        $desa = MasterDesa::where('id_kecamatan', $kecamatan_id)->get()
                    ->lists('nama_desa','id');
        
        $dropDownList = Form::select(
            'desa_id', $desa, '', 
            array(
                'id' => 'desa', 
                'class' => 'form-control js-selectize')
        );
        
        echo $dropDownList;
    }


    public function store(Request $request)
    {
    	$newBukuTanah = new BukuTanah;
    	$newBukuTanah->fill($request->all())->save();

    	return response()->json([
                    "success"=> "Buku Tanah telah ditambahkan."
                ]);
    }


    public function edit($id){
        $buku_tanah = BukuTanah::with('desa')->findOrFail($id);

        return response()->json([
            'url_action'=>url('buku-tanah/update/'.$buku_tanah->id),
            'buku_tanah'=>$buku_tanah
        ]);
    }


    public function update(Request $request, $id)
    {
    	$bukuTanah = BukuTanah::findOrFail($id);
    	$inputData = $request->all();

    	if(!$bukuTanah->update($inputData))
            return response()->json([
                "errors"=> "Error menyimpan data buku tanah."
            ]);

        return response()->json([
                    "success"=> "Buku Tanah telah diperbaharui."
                ]);
    }


    public function destroy(Request $request)
    {
        $data = BukuTanah::findOrFail($request->id);
        
        if($data->update(['record_status'=>'D']))
            return response()->json([
                        "success"=> "Data telah dihapus."
                    ]);
    }


}
