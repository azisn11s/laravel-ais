<?php

namespace Hybrings\Http\Controllers\AuthSentinel;

use Illuminate\Http\Request;

use Hybrings\Http\Requests;
use Hybrings\Http\Controllers\Controller;
use Sentinel;

use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Hybrings\Classes\Models\User;


class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('sentinel_guest', ['except' => 'postLogout']);
    }

    /**
     * Showing login form
     */
    public function login()
    {
        if(Sentinel::check())
            return redirect('/');
        	
        return view('auth-sentinel.login');
    }

	/**
	 * Authenticating user credentials
	 */
    public function postLogin(Request $request)
    {
    	$backToLogin = redirect()->action('AuthSentinel\LoginController@login')->withInput();

    	try {
            $remember = (bool) $request->input('remember_me');

            // If password is incorrect...
            if (!User::authenticate($request->input('login'), $request->input('password'), $remember)) {
            	$request->session()->put('flash_notification', [
	            	'level'=> 'warning',
	            	'message'=>'The given credentials do not match our records!'
	            ]);

                return $backToLogin;
            }

            return redirect()->intended('/');
            
        } catch (ThrottlingException $e) {
            $request->session()->put('flash_notification', [
            	'level'=>'warning',
            	'message'=>'Too many attempts.'
            ]);
        } catch (NotActivatedException $e) {
        	$request->session()->put('flash_notification', [
        		'level'=>'warning',
        		'message'=>'Please activate your account before trying to log in.'
        	]);
        }

        return $backToLogin;

    }

    /**
     * Logout proses by POST
     */
    public function postLogout()
    {
        session()->flush();
    	Sentinel::logout();

    	return redirect()->back();
    }
}
