<?php

namespace Hybrings\Http\Controllers;

use Illuminate\Http\Request;

use Hybrings\Http\Requests;
use Hybrings\Http\Requests\StoreAlbumRequest;

use Hybrings\Classes\Models\Album;

use Datatables;

class AlbumController extends Controller
{
    
    public function index(Request $request)
    {
    	$headerContent = array(
            'title' => 'Album',
            'description' => 'Daftar semua Album',
            'breadcrumb' => array(
                array(
                    'name'=>'Album',
                    'routeName'=>'',
                    'icon'=>'fa-home',
                    'active'=>'',
                ),
                array(
                    'name'=>'List',
                    'routeName'=>'album.index',
                    'icon'=>'',
                    'active'=>'active',
                ), 
            ),
        );

    	return view('album.index')->with(compact('headerContent'));
    }


    public function bind(){
        $data = Album::where('record_status', 'A')->get();

        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return "<a href='javascript:void(0)' onclick='edit(".$data->id.")' class='btn btn-xs btn-primary'><i class='glyphicon glyphicon-edit'></i> Edit</a>
                        <a href='javascript:void(0)' onclick='delConfirm(".$data->id.")' class='btn btn-xs btn-danger'>
                        <i class='glyphicon glyphicon-trash'></i> Delete</a>";
            })
            ->make(true);
    }


    public function store(StoreAlbumRequest $request)
    {
    	$newAlbum = new Album;
    	$newAlbum->fill($request->all())->save();

    	return response()->json([
                    "success"=> "Album telah ditambahkan."
                ]);
    }


    public function edit($id){
        $album = Album::findOrFail($id);

        return response()->json([
            'url_action'=>url('buku-tanah/album/update/'.$album->id),
            'album'=>$album
        ]);
    }


    public function update(Request $request, $id)
    {
    	$album = Album::findOrFail($id);
    	$inputData = $request->all();

    	if(!$album->update($inputData))
            return response()->json([
                "errors"=> "Error menyimpan data album."
            ]);

        return response()->json([
                    "success"=> "Album telah diperbaharui."
                ]);
    }


    public function destroy(Request $request)
    {
        $data = Album::findOrFail($request->id);
        
        if($data->update(['record_status'=>'D']))
            return response()->json([
                        "success"=> "Data telah dihapus."
                    ]);
    }


}
