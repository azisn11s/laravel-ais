<?php

namespace Hybrings\Http\Controllers;

use Illuminate\Http\Request;

use Hybrings\Http\Requests;
use Hybrings\Classes\Models\MasterKecamatan;
use Hybrings\Classes\Models\MasterDesa;

use \Form;
use Datatables;

class DesaController extends Controller
{
    

    public function index(Request $request)
    {
    	$data['kecamatan']  = MasterKecamatan::where('record_status','A')->pluck('nama_kecamatan','id');

    	$headerContent = array(
            'title' => 'Desa',
            'description' => 'Daftar semua Desa',
            'breadcrumb' => array(
                array(
                    'name'=>'Desa',
                    'routeName'=>'',
                    'icon'=>'fa-home',
                    'active'=>'',
                ),
                array(
                    'name'=>'List',
                    'routeName'=>'desa.index',
                    'icon'=>'',
                    'active'=>'active',
                ), 
            ),
        );

    	return view('desa.index')->with(compact('headerContent','data'));
    }


    public function bind(){
        $data = MasterDesa::with('kecamatan')->where('record_status', 'A')->get();

        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return "<a href='javascript:void(0)' onclick='edit(".$data->id.")' class='btn btn-xs btn-primary'><i class='glyphicon glyphicon-edit'></i> Edit</a>
                        <a href='javascript:void(0)' onclick='delConfirm(".$data->id.")' class='btn btn-xs btn-danger'>
                        <i class='glyphicon glyphicon-trash'></i> Delete</a>";
            })
            ->make(true);
    }


    public function store(Request $request)
    {
    	$newDesa = new MasterDesa;
    	$newDesa->fill($request->all())->save();

    	return response()->json([
                    "success"=> "Desa telah ditambahkan."
                ]);
    }


    public function edit($id){
        $desa = MasterDesa::findOrFail($id);

        return response()->json([
            'url_action'=>url('utility/lokasi/desa/update/'.$desa->id),
            'desa'=>$desa
        ]);
    }


    public function update(Request $request, $id)
    {
    	$desa = MasterDesa::findOrFail($id);
    	$inputData = $request->all();

    	if(!$desa->update($inputData))
            return response()->json([
                "errors"=> "Error menyimpan data desa."
            ]);

        return response()->json([
                    "success"=> "Desa telah diperbaharui."
                ]);
    }


    public function destroy(Request $request)
    {
        $data = MasterDesa::findOrFail($request->id);
        
        if($data->update(['record_status'=>'D']))
            return response()->json([
                        "success"=> "Data telah dihapus."
                    ]);
    }




}
