<?php

namespace Hybrings\Http\Controllers;

use Illuminate\Http\Request;

use Hybrings\Http\Requests\UpdateAppProfileRequest;

use Hybrings\Classes\AppProfile;

class AboutController extends Controller
{
	const __PROFILE_ID = 1;

	private static $profile;

    function __construct()
    {
        $this->middleware('sentinel_access:about');
    }


    public function index()
    {
    	$profile = $this->getProfile();

    	$headerContent = array(
            'title' => 'About',
            'description' => 'Brief Information about this Web Application',
            'breadcrumb' => array(
                array(
                    'name'=>'Administrator',
                    'routeName'=>'',
                    'icon'=>'fa-home',
                    'active'=>'',
                ),
                array(
                    'name'=>'About',
                    'routeName'=>'about.index',
                    'icon'=>'',
                    'active'=>'active',
                ), 
            ),
        );

    	return view('about.index')->with(compact('profile', 'headerContent'));
    }

    private function getProfile()
    {
    	self::$profile = AppProfile::find(self::__PROFILE_ID);

    	return self::$profile;
    }

    public function update(UpdateAppProfileRequest $request)
    {
    	$profile = $this->getProfile();

    	$profile->update($request->all());

    	return back();
    }
}
