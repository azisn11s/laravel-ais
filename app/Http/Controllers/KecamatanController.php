<?php

namespace Hybrings\Http\Controllers;

use Illuminate\Http\Request;

use Hybrings\Http\Requests;

use Hybrings\Classes\Models\MasterKecamatan;
use Hybrings\Http\Requests\StoreKecamatan;

use Datatables;

class KecamatanController extends Controller
{
    

    public function index(Request $request)
    {

    	$headerContent = array(
            'title' => 'Kecamatan',
            'description' => 'Daftar semua Kecamatan',
            'breadcrumb' => array(
                array(
                    'name'=>'Kecamatan',
                    'routeName'=>'',
                    'icon'=>'fa-home',
                    'active'=>'',
                ),
                array(
                    'name'=>'List',
                    'routeName'=>'kecamatan.index',
                    'icon'=>'',
                    'active'=>'active',
                ), 
            ),
        );

    	return view('kecamatan.index')->with(compact('headerContent'));
    }


    public function bind(){
        $data = MasterKecamatan::where('record_status', 'A')->get();

        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return "<a href='javascript:void(0)' onclick='edit(".$data->id.")' class='btn btn-xs btn-primary'><i class='glyphicon glyphicon-edit'></i> Edit</a>
                        <a href='javascript:void(0)' onclick='delConfirm(".$data->id.")' class='btn btn-xs btn-danger'>
                        <i class='glyphicon glyphicon-trash'></i> Delete</a>";
            })
            ->make(true);
    }


    public function store(StoreKecamatan $request)
    {
    	$newKecamatan = new MasterKecamatan;
    	$newKecamatan->fill($request->all())->save();

    	return response()->json([
                    "success"=> "Kecamatan telah ditambahkan."
                ]);
    }


    public function edit($id){
        $kecamatan = MasterKecamatan::findOrFail($id);

        return response()->json([
            'url_action'=>url('utility/lokasi/kecamatan/update/'.$kecamatan->id),
            'kecamatan'=>$kecamatan
        ]);
    }


    public function update(Request $request, $id)
    {
    	$kecamatan = MasterKecamatan::findOrFail($id);
    	$inputData = $request->all();

    	if(!$kecamatan->update($inputData))
            return response()->json([
                "errors"=> "Error menyimpan data kecamatan."
            ]);

        return response()->json([
                    "success"=> "Kecamatan telah diperbaharui."
                ]);
    }


    public function destroy(Request $request)
    {
        $data = MasterKecamatan::findOrFail($request->id);
        // if($data->delete())
        if($data->update(['record_status'=>'D']))
            return response()->json([
                        "success"=> "Data telah dihapus."
                    ]);
    }







}
