<?php

namespace Hybrings\Http\Controllers;

use Hybrings\Http\Requests;
use Illuminate\Http\Request;

use Hybrings\Classes\Models\mst_agent as Agent;
use Hybrings\Classes\Models\mst_province as Province;
use Hybrings\Classes\Models\mst_city as City;
use Hybrings\Classes\Models\mst_gender as Gender;
use Hybrings\Classes\Models\mst_marital_status as Marital;
use Hybrings\Classes\Models\mst_job as Job;

use \Form;
use Sentinel;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('sentinel_guest', ['except'=>'index']);
    }

    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function register(){
        $data['provinces']  = Province::lists('province_name','province_id');
        $data['gender']     = Gender::Lists('gender_name','gender_id'); 
        $data['marital']    = Marital::Lists('marital_status_name','marital_status_id');
        $data['job']        = Job::Lists('job_name','job_id');

        return view('register/index',compact('data'));
    }

    public function getCityByProvince($id){
        $cities = City::where('province_id',$id)->get();
        $form = Form::select('city_id', $cities->lists('city_name','city_id'), '', array('id' => 'city_id', 'class' => 'form-control js-selectize') );
        echo $form;
    }

    public function register_store(Request $request){
        $input = $request->all();
        $agent = new Agent($input);

        if ($request->has('email') && $request->has('password')) {
            $credentials = ['email'=>$request->email, 'password'=>$request->password, 'type' => 'agent'];
            $user = Sentinel::registerAndActivate($credentials);
            
            $role = Sentinel::findRoleBySlug('agent');
            $role->users()->attach($user);
        }

        $agent->user_id = $user->id;
        $agent->fill($input)->save();

        $data = $request->agent_name;

        return view('register/success', compact('data'));
    }

}
