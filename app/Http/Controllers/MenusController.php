<?php

namespace Hybrings\Http\Controllers;

use Illuminate\Http\Request;
use Hybrings\Classes\Models\Menu;
use Hybrings\Http\Requests\StoreMenuRequest;
use Yajra\Datatables\Datatables;
use Validator;
use DB;

class MenusController extends Controller
{

    function __construct()
    {
        $this->middleware('sentinel_access:menus');
    }


    public function index()
    {
    	
        $headerContent = array(
            'title' => 'Menu',
            'description' => 'List all of main menu',
            'breadcrumb' => array(
                array(
                    'name'=>'Administrator',
                    'routeName'=>'',
                    'icon'=>'fa-home',
                    'active'=>'',
                ),
                array(
                    'name'=>'Menu',
                    'routeName'=>'menus.index',
                    'icon'=>'',
                    'active'=>'active',
                ), 
            ),
        );
    		
    	return view('menus.index')->with(compact('headerContent'));
    }

    public function bind(Request $request){
        if ($request->ajax()) {
            $menus = Menu::all();
            
            return Datatables::of($menus)->make(true);
        }
    }

    public function create()
    {
    	$data['parents_menu'] = Menu::where('is_root', '=', 1)->pluck('name','id')->all();

        $headerContent = array(
            'title' => 'Create',
            'description' => 'Create new menu',
            'breadcrumb' => array(
                array(
                    'name'=>'Home',
                    'routeName'=>'',
                    'icon'=>'fa-home',
                    'active'=>'',
                ),
                array(
                    'name'=>'Menu',
                    'routeName'=>'menus.index',
                    'icon'=>'',
                    'active'=>'',
                ), 
                array(
                    'name'=>'Create',
                    'routeName'=>'menus.create',
                    'icon'=>'',
                    'active'=>'active',
                ),
            ),
        );

    	return view('menus.create', compact('data', 'headerContent'));
    }

    public function store(StoreMenuRequest $request)
    {
    	$menu = Menu::create($request->all())->save();

    	return redirect()->route('menus.index');
    }
}
