<?php

namespace Hybrings\Http\Controllers;

use Illuminate\Http\Request;

use Hybrings\Http\Requests;

use Hybrings\Classes\Models\Jabatan;
use Hybrings\Http\Requests\StoreJabatan;

use Datatables;

class JabatanController extends Controller
{
    public function index(Request $request)
    {

    	$headerContent = array(
            'title' => 'Jabatan',
            'description' => 'Daftar semua Jabatan',
            'breadcrumb' => array(
                array(
                    'name'=>'Jabatan',
                    'routeName'=>'',
                    'icon'=>'fa-home',
                    'active'=>'',
                ),
                array(
                    'name'=>'List',
                    'routeName'=>'jabatan.index',
                    'icon'=>'',
                    'active'=>'active',
                ), 
            ),
        );

    	return view('jabatan.index')->with(compact('headerContent'));
    }


    public function bind(){
        $data = Jabatan::where('record_status', 'A')->get();

        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return "<a href='javascript:void(0)' onclick='edit(".$data->id.")' class='btn btn-xs btn-primary'><i class='glyphicon glyphicon-edit'></i> Edit</a>
                        <a href='javascript:void(0)' onclick='delConfirm(".$data->id.")' class='btn btn-xs btn-danger'>
                        <i class='glyphicon glyphicon-trash'></i> Delete</a>";
            })
            ->make(true);
    }


    public function store(StoreJabatan $request)
    {
    	$newJabatan = new Jabatan;
    	$newJabatan->fill($request->all())->save();

    	return response()->json([
                    "success"=> "Jabatan baru telah ditambahkan."
                ]);
    }


    public function edit($id){
        $jabatan = Jabatan::findOrFail($id);

        return response()->json([
            'url_action'=>url('utility/jabatan/update/'.$jabatan->id),
            'jabatan'=>$jabatan
        ]);
    }


    public function update(Request $request, $id)
    {
    	$jabatan = Jabatan::findOrFail($id);
    	$inputData = $request->all();

    	if(!$jabatan->update($inputData))
            return response()->json([
                "errors"=> "Error menyimpan data jabatan."
            ]);

        return response()->json([
                    "success"=> "Jabatan telah diperbaharui."
                ]);
    }


    public function destroy(Request $request)
    {
        $data = Jabatan::findOrFail($request->id);
        // if($data->delete())
        if($data->update(['record_status'=>'D']))
            return response()->json([
                        "success"=> "Data telah dihapus."
                    ]);
    }
}
