<?php

namespace Hybrings\Http\Controllers;

use Illuminate\Http\Request;

use Hybrings\Http\Requests;
use Hybrings\Http\Requests\StorePemohonRequest;

use Hybrings\Classes\Models\Pemohon;
use Hybrings\Classes\Models\Gender;
use Hybrings\Classes\Models\Jabatan;

use Datatables;

class PemohonController extends Controller
{
    

	public function index(Request $request)
    {
    	$data['jekel'] = Gender::pluck('nama_jekel','id');
    	$data['jabatan'] = Jabatan::where('record_status','A')->pluck('nama_jabatan','id');

    	$headerContent = array(
            'title' => 'Pemohon',
            'description' => 'Daftar semua Pemohon',
            'breadcrumb' => array(
                array(
                    'name'=>'Pemohon',
                    'routeName'=>'',
                    'icon'=>'fa-home',
                    'active'=>'',
                ),
                array(
                    'name'=>'List',
                    'routeName'=>'pemohon.index',
                    'icon'=>'',
                    'active'=>'active',
                ), 
            ),
        );

    	return view('pemohon.index')->with(compact('headerContent','data'));
    }


    public function bind(){
        $data = Pemohon::with('jabatan', 'gender')->where('record_status', 'A')->get();

        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return "<a href='javascript:void(0)' onclick='edit(".$data->id.")' class='btn btn-xs btn-primary'><i class='glyphicon glyphicon-edit'></i> Edit</a>
                        <a href='javascript:void(0)' onclick='delConfirm(".$data->id.")' class='btn btn-xs btn-danger'>
                        <i class='glyphicon glyphicon-trash'></i> Delete</a>";
            })
            ->make(true);
    }


    public function store(StorePemohonRequest $request)
    {
    	$newPemohon = new Pemohon;
    	$newPemohon->fill($request->all())->save();

    	return response()->json([
                    "success"=> "Pemohon telah ditambahkan."
                ]);
    }


    public function edit($id){
        $pemohon = Pemohon::findOrFail($id);

        return response()->json([
            'url_action'=>url('pemohon/update/'.$pemohon->id),
            'pemohon'=>$pemohon
        ]);
    }


    public function update(Request $request, $id)
    {
    	$desa = Pemohon::findOrFail($id);
    	$inputData = $request->all();

    	if(!$desa->update($inputData))
            return response()->json([
                "errors"=> "Error menyimpan data pemohon."
            ]);

        return response()->json([
                    "success"=> "Pemohon telah diperbaharui."
                ]);
    }


    public function destroy(Request $request)
    {
        $data = Pemohon::findOrFail($request->id);
        
        if($data->update(['record_status'=>'D']))
            return response()->json([
                        "success"=> "Data telah dihapus."
                    ]);
    }


}
