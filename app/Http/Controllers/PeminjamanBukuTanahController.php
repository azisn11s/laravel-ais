<?php

namespace Hybrings\Http\Controllers;

use Illuminate\Http\Request;

use Hybrings\Http\Requests;

use DB;
use Sentinel;
use Datatables;
use Hybrings\Classes\Models\BukuTanah;
use Hybrings\Classes\Models\Pemohon;
use Hybrings\Classes\Models\LogPinjam;
use Hybrings\Classes\Models\LogDetailPinjamBukuTanah;

class PeminjamanBukuTanahController extends Controller
{
    

    public function index(Request $request)
    {
        // Base Form
        $data['nomor_pinjam'] = $this->getNomorPinjam();

        $data['tanggal_pinjam'] = custom_date();

    	$data['jekel'] = null;
    	// $data['jabatan'] = Jabatan::where('record_status','A')->pluck('nama_jabatan','id');

    	$headerContent = array(
            'title' => 'Transaksi Peminjaman Buku Tanah',
            'description' => '',
            'breadcrumb' => array(
                array(
                    'name'=>'Transaksi Buku Tanah',
                    'routeName'=>'',
                    'icon'=>'fa-home',
                    'active'=>'',
                ),
                array(
                    'name'=>'List',
                    'routeName'=>'peminjaman-bt.index',
                    'icon'=>'',
                    'active'=>'active',
                ), 
            ),
        );

    	return view('buku-tanah.peminjaman')->with(compact('headerContent','data'));
    }

    public function getNomorPinjam()
    {
        return random_number(2).date('mdis');
    }

    public function search($term)
    {
        $result = [];

        $listBukuTanah = BukuTanah::where('nb_barcode', 'like', '%'.$term.'%')
                            ->orWhere('no_hak', 'like', '%'.$term.'%')
                            ->orWhere('pemegang_hak', 'like', '%'.$term.'%')
                            ->take(10)->get();

        foreach($listBukuTanah as $bukuTanah)
        {
            $result[] = [
                'id'    => $bukuTanah->id,
                'value' => $bukuTanah->nb_barcode.' - Pemegang Hak: '.$bukuTanah->pemegang_hak
            ];
        }

        return response()->json($result);

    }

    public function store(Request $request)
    {
        // 1. Cari dan buat instance pemohon | nip
        $pemohon = Pemohon::where('nip', $request->nip)->first();

        if(!$pemohon){
            return response()->json([
                'errors' => "Tidak ada data Pemohon dengan NIP $request->nip",
            ]);
        }

        // 2. Cari & Jika tidak ada, buat instance log pinjam | nomor_pinjam
        $logPinjam = LogPinjam::where('nomor_pinjam', $request->nomor_pinjam)->first();

        if(!$logPinjam){
            $newLogPinjam = new LogPinjam;
            $newLogPinjam->nomor_pinjam = $request->nomor_pinjam;
            $newLogPinjam->tanggal_pinjam = date_sql($request->tanggal_pinjam);
            $newLogPinjam->pemohon_id = $pemohon->id;
            $newLogPinjam->note = $request->note;
            $newLogPinjam->user_id = Sentinel::getUser()->id;
            // $newLogPinjam->total_pinjam
        }

        // 3. Cek Buku Tanah
        $bukuTanah = BukuTanah::find($request->buku_tanah_id);

        if(!$bukuTanah){
            return response()->json([
                'errors' => "Tidak ada data Buku Tanah yang dicari.",
            ]);
        }else{
            if($bukuTanah->status !== '1'){
                return response()->json([
                    'errors' => "Buku Tanah ini sedang dipinjam.",
                ]);
            }
        }

        // 4. Buat instance log detail peminjaman
        $pinjam = $logPinjam ?: $newLogPinjam;

        // $logOtherDetailPinjam = LogDetailPinjamBukuTanah::where


        // 5. Peminjaman Buku Tanah
        $logDetailPeminjaman = LogDetailPinjamBukuTanah::where('nomor_pinjam', $pinjam->nomor_pinjam)->get();

        if($logDetailPeminjaman->count() > 0){

            // Jika Buku Tanah masih dipinjam
            if($logDetailPeminjaman->contains('buku_tanah_id', $bukuTanah->id)){
                return response()->json([
                    'errors' => "Buku Tanah yang dimaksud sekarang sudah Anda pinjam.",
                ]);
            }

            // Update Jumlah Pinjam
            $pinjam->total_pinjam = $logDetailPeminjaman->count() + 1;

        }else{
            $pinjam->total_pinjam = 1;
        }

        // 6. Transaksi Peminjaman
        DB::beginTransaction();
        try {

            $bukuTanah->status = 2;
            $bukuTanah->save();

            $pinjam->save();

            $newLogDetailPinjam = new LogDetailPinjamBukuTanah;
            $newLogDetailPinjam->nomor_pinjam = $pinjam->nomor_pinjam;
            $newLogDetailPinjam->buku_tanah_id = $request->buku_tanah_id;
            $newLogDetailPinjam->jumlah_buku = 1;
            $newLogDetailPinjam->save();

            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            DB::rollback();
        }

        if ($success) {
            return response()->json([
                    'success' => "Berhasil menyimpan data peminjaman Buku Tanah.",
                ]);
        }else{
            return response()->json([
                    'errors' => "Gagal menyimpan data Peminjaman. Mohon mencoba kembali.",
                ]);
        }


    }

    public function listPeminjaman($nomor_pinjam)
    {
        $data = LogDetailPinjamBukuTanah::with('logPinjam', 'bukuTanah', 'bukuTanah.jenisHak', 'bukuTanah.desa', 'bukuTanah.desa.kecamatan')->where('nomor_pinjam', $nomor_pinjam)->get();

        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return "<a href='javascript:void(0)' onclick='delConfirm(".$data->nomor_pinjam.",".$data->buku_tanah_id.")' class='btn btn-xs btn-danger'>
                        <i class='glyphicon glyphicon-trash'></i> Delete</a>";
            })
            ->make(true);
    }

    public function detailDestroy(Request $request)
    {     
        $data = LogDetailPinjamBukuTanah::where('nomor_pinjam', $request->id)
                ->where('buku_tanah_id', $request->buku_tanah_id);
                // ->firstOrFail();

        $bukuTanah = BukuTanah::find($request->buku_tanah_id);
        $bukuTanah->status = 1;
        
        $peminjaman = LogPinjam::where('nomor_pinjam', $request->id)->first();
        $peminjaman->total_pinjam = $peminjaman->total_pinjam - 1;
        
        if($data->delete() && $peminjaman->save() && $bukuTanah->save())
            return response()->json([
                        "success"=> "Data telah dihapus."
                    ]);
    
    }

    public function allPeminjaman()
    {       
        $data = LogPinjam::with('pemohon')->get();
        // dd($data);die;

        return Datatables::of($data)
            ->editColumn('tanggal_pinjam', function ($data) {
                return custom_date($data->tanggal_pinjam, 'Y-m-d H:i:s');
            })
            ->filterColumn('tanggal_pinjam', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(tanggal_pinjam, '%d/%m/%Y') like ?", ["%$keyword%"]);
            })
            ->make(true);
            
    }


}
