<?php

namespace Hybrings\Http\Controllers\Auth;

use Hybrings\Classes\Models\User;
use Illuminate\Http\Request;
use Hybrings\Classes\RolePermission\Role;
use Validator;
use Hybrings\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use Illuminate\Support\Facades\Session;
use Auth;
use Socialite;



class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $username = 'username';

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
        $this->middleware('user-should-verified');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'api_token' => str_random(70),
        ]);
        $memberRole = Role::where('name','member')->first();
        $user->attachRole($memberRole);
        $user->sendVerification();
        
        return $user;
    }


    /**
     * Email Account Verification
     */
    public function verify(Request $request, $token)
    {
        $email = $request->get('email');
        $user = User::where('verification_token', $token)->where('email', $email)->first();

        if ($user) {
            $user->verify();
            Session::flash("flash_notification", [
                "level" => "success",
                "message" => "Verified successfully."
            ]);
            // Auth::login($user);
        }
        return redirect('/');
    }


    /**
     * Using Socialite OAuth
     */
    public function redirectToProvider($provider)
    {
        try {
            return Socialite::driver($provider)->redirect();
        } catch (InvalidArgumentException $e) {
            return abort(404, 'Unknown Driver.');
        }
    }

    public function providerCallback($provider)
    {
        try {
            $account = Socialite::driver($provider)->user();
            $user = User::whereNotNull('provider_id')->where(['email'=>$account->email])->get()->first();

            if (count($user) < 1) {
                $user = User::firstOrCreate(['provider'=>$provider, 'provider_id'=>$account->id]);
            }else{
                $user->provider = $provider;
                $user->provider_id = $account->id;
            }       

            // Update Details
            $user->username = $account->name;
            $user->email = $account->email;
            $user->avatar = $account->avatar;
            $user->is_verified = 1;
            $user->save();

            Auth::login($user, true);
            return redirect('/');

        } catch (InvalidArgumentException $e) {
            return abort(404, 'Unknown Driver.');
        } catch (\GuzzleHttp\Exception\ClientException $e){
            return redirect('auth/login');
        }
    }
}
