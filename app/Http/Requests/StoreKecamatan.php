<?php

namespace Hybrings\Http\Requests;

use Hybrings\Http\Requests\Request;
use Sentinel;


class StoreKecamatan extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Sentinel::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'kode_kecamatan'    =>'required|unique:mst_kecamatan|max:5',
            'nama_kecamatan'    =>'required|max:125'
        ];
    }

    /**
     * Overide validation messages
     */
    public function messages()
    {
        return [
            'kode_kecamatan.required' => 'Kode Kecamatan harus diisi.',
            'nama_kecamatan.required' => 'Nama Kecamatan harus diisi.',

            'kode_kecamatan.unique' => 'Kode yang sama telah dipakai.',

            'kode_kecamatan.max' => 'Kode Kecamatan maksimal 5 karakter.',
            'nama_kecamatan.max' => 'Nama Kecamatan maksimal 125 karakter.'
        ];
    }

}
