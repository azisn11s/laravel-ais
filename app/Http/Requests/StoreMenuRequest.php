<?php

namespace Hybrings\Http\Requests;

use Hybrings\Http\Requests\Request;
use Sentinel;


class StoreMenuRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Sentinel::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'url' => 'unique:menus|max:255',
            'uri' => 'required|unique:menus|max:255',
        ];
    }
}
