<?php

namespace Hybrings\Http\Requests;

use Hybrings\Http\Requests\Request;
use Sentinel;

class StorePemohonRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Sentinel::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nip'    =>'required|unique:mst_pemohon|max:35',
            'nama_lengkap'    =>'required|max:50',
            'jekel_id'  => 'required'

        ];
    }

    /**
     * Overide validation messages
     */
    public function messages()
    {
        return [
            'nip.required' => 'NIP harus diisi.',
            'nama_lengkap.required' => 'Nama harus diisi.',
            'jekel_id.required' => 'Jenis kelamin harus diisi',

            'nip.unique' => 'NIP yang sama telah dipakai.',

            'nip.max' => 'NIP maksimal 35 karakter.',
            'nama_lengkap.max' => 'Nama maksimal 50 karakter.'
        ];
    }
}
