<?php

namespace Hybrings\Http\Requests;

use Hybrings\Http\Requests\Request;
use Sentinel;

class StoreProjectOwner extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Sentinel::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_owner_name'    =>'required',
            'address'               =>'required',
            'province_id'           =>'required',
            'city_id'               =>'required',
            'phone'                 =>'required',
            'email'                 =>'required',
            'password'              =>'required',
            'join_date'             =>'date_format:"d/m/Y"',
            'avatar'                =>'required|image|max:2048'
        ];
    }


    /**
     * Overide validation messages
     */
    public function messages()
    {
        return [
            'project_owner_name.required' => 'Nama lengkap harus diisi.',
            'address.required' => 'Alamat lengkap harus diisi.',
            'province_id.required' => 'Provinsi belum dipilih',
            'city_id.required' => 'Kota belum dipilih.',
            'phone.required' => 'Telp./HP harus diisi.',
            'email.required' => 'Email harus diisi.',
            'password.required' => 'Password harus diisi.',
            'avatar.required' => 'Foto profile harus diisi.'
        ];
    }
}
