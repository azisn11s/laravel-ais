<?php

namespace Hybrings\Http\Requests;

use Hybrings\Http\Requests\Request;

use Sentinel;

class StoreJabatan extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Sentinel::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'kode_jabatan'    =>'required|unique:mst_jabatan|max:5',
            'nama_jabatan'    =>'required|min:3|max:100'
        ];
    }
}
