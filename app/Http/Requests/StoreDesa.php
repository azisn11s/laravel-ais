<?php

namespace Hybrings\Http\Requests;

use Hybrings\Http\Requests\Request;

use Sentinel;

class StoreDesa extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Sentinel::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'kode_desa'    =>'required|unique:mst_desa|max:5',
            'nama_desa'    =>'required|max:125'
        ];
    }

    /**
     * Overide validation messages
     */
    public function messages()
    {
        return [
            'kode_desa.required' => 'Kode Desa harus diisi.',
            'nama_desa.required' => 'Nama Desa harus diisi.',

            'kode_desa.unique' => 'Kode yang sama telah dipakai.',

            'kode_desa.max' => 'Kode Desa maksimal 5 karakter.',
            'nama_desa.max' => 'Nama Desa maksimal 125 karakter.'
        ];
    }


}
