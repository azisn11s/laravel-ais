<?php

namespace Hybrings\Http\Requests;

use Hybrings\Http\Requests\Request;
use Sentinel;

class StoreAlbumRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Sentinel::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'no_album'    =>'required|unique:mst_album|max:10',
            'lemari'    =>'max:10',
            'rak'  => 'max:10',
            'blok' => 'max:10',

        ];
    }

    /**
     * Overide validation messages
     */
    public function messages()
    {
        return [
            'no_album.required' => 'Nomor Album harus diisi.',
            'no_album.unique' => 'Nomor Album yang sama telah dipakai.',

            'no_album.max' => 'Nomor Album maksimal 10 karakter.',
            'lemari.max' => 'Lemari maksimal 10 karakter.',
            'rak.max' => 'Rak maksimal 10 karakter.',
            'blok.max' => 'Blok maksimal 10 karakter.'
            
        ];
    }
}
