<?php

namespace Hybrings\Http\Requests;

use Hybrings\Http\Requests\Request;

class UpdateAppProfileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'app_name' => 'required|min:3|max:30',
            'jargon' => 'min:3|max:100',
            'company_name' => 'required|min:3|max:100',
            'year' => 'required|integer|min:2000|max:3000',
            'app_version' => 'required|string|max:5',
        ];
    }
}
