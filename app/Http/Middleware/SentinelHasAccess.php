<?php

namespace Hybrings\Http\Middleware;

use Closure;
use Sentinel;

class SentinelHasAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        if (! $this->has_access($permission)) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 403);
            }
            return response('Unauthorized.', 403);
            //return response()->view('backend.unauthorized');
        }

        return $next($request);
    }


    /**
     * Check if user has access.
     *
     * @param  array|string  $permissions
     * @param  bool  $any
     * @return bool
     */
    function has_access($permissions, $any = false)
    {
        // dd($permissions);
        $method = $any ? 'hasAnyAccess' : 'hasAccess';
        // dd($method);
        // dd(Sentinel::check()->{$method}([$permissions]));
        return Sentinel::check()->{$method}([$permissions]);
    }

}
