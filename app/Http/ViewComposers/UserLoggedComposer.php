<?php 

namespace Hybrings\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use Sentinel;


/**
* Menu Composer for left side bar
*/
class UserLoggedComposer
{


	private $user;
	private $data = [];

	public function __construct()
	{
		$this->user = Sentinel::getUser();
		$this->findUser(Sentinel::getUser());
	}

	
	public function compose(View $view)
	{
		$user = $this->user;
		$data = $this->data;
		
		$view->with(compact('user','data'));
	}


	private function findUser($user)
    {

    	switch ($user->type) {
    		case '':
    			# code here...
    			break;

    		/*case 'agent':
    			$agent = $this->findAgent($user->id);
    			$this->data['full_name'] = $agent->agent_name;
    			$this->data['created_date'] = $agent->created_at;
    			$this->data['user_type'] = 'Marketing Agent';
    			break;*/
    		
    		default:
    			$this->data['full_name'] = $this->user->username;
    			$this->data['created_date'] = $this->user->created_at;
    			$this->data['user_type'] = 'Admin';
    			break;
    	}

    }

    /*private function findProjectOwner($userId)
    {
    	$owner = ProjectOwner::where('user_id', $userId)->first();
    	
    	return $owner;
    }

    private function findAgent($userId)
    {
    	$agent = Agent::where('user_id', $userId)->first();

    	return $agent;
    }*/


}