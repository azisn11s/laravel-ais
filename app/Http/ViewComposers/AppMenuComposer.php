<?php 

namespace Hybrings\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use Hybrings\Classes\Models\Menu;
use Hybrings\Classes\Models\Role;
use Sentinel;


/**
* Menu Composer for left side bar
*/
class AppMenuComposer
{
	private $role_id;
	private $parents;
	private $childs;

	public function __construct()
	{
		$this->role_id = Sentinel::getUser()->roles()->get()->first()->id;
		$this->parents = Role::find($this->role_id)->menus()->where('is_root', '=', 1)->orderBy('sort')->get();
		$this->childs = Role::find($this->role_id)->menus()->where('is_root', '=', 0)->get();

		// $this->parents = Menu::where('is_root', '=', 1)->orderBy('sort')->get();
		// $this->childs = Menu::where('is_root', '=', 0)->get();
	}

	private function restructure()
	{
		$menus = [];
		$parents = $this->parents;
		
		for ($i=0; $i < count($parents); $i++) { 
			$childs = [];
			for ($j=0; $j < count($this->childs); $j++) { 
				if($this->childs[$j]->parent_id == $parents[$i]->id) array_push($childs, $this->childs[$j]);
			}
			array_push($menus, ['parent' => $parents[$i], 'childs' => $childs]);
		}

		return $menus;
	}
	
	public function compose(View $view)
	{
		//dd($this->restructure());
		$view->with('app_menu', $this->restructure());
	}
}