<?php namespace Hybrings\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use Hybrings\Classes\AppProfile;

/**
* Menu Composer for left side bar
*/
class AppProfileComposer
{
	private $myApplication;

	public function __construct()
	{
		$this->myApplication = AppProfile::where('id','=','1')->firstOrFail();
	}

	
	public function compose(View $view)
	{
		$view->with('app_profile', $this->myApplication);
	}
}