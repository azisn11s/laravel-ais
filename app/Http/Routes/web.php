<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/




// Authentication
Route::get('login', 'AuthSentinel\LoginController@login');
Route::post('login', 'AuthSentinel\LoginController@postLogin');
Route::post('logout', 'AuthSentinel\LoginController@postLogout');

Route::get('register','HomeController@register');
Route::post('getCityByProvince/{province_id}','HomeController@getCityByProvince');
Route::post('register/store','HomeController@register_store');
Route::get('/', 'HomeController@index')->middleware('sentinel_auth')->name('home');


// Administrator
Route::group(['prefix' => 'administrator', 'middleware'=>'sentinel_auth'], function () {

	Route::get('menus', 'MenusController@index')->name('menus.index');
	Route::get('menus/create', 'MenusController@create')->name('menus.create');
	Route::post('menus/create', 'MenusController@store')->name('menus.store');
	Route::get('menus/bind', 'MenusController@bind')->name('menus.bind');

	Route::get('about', 'AboutController@index')->name('about.index');
	Route::put('about/update', 'AboutController@update')->name('about.update');

});


// Pemohon
Route::group(['prefix' => 'pemohon', 'middleware'=>'sentinel_auth'], function () {

	Route::get('list', 'PemohonController@index')->name('pemohon.index');
	Route::get('bind', 'PemohonController@bind')->name('pemohon.bind');
    Route::post('create', 'PemohonController@store')->name('pemohon.store');
    Route::get('edit/{id}', 'PemohonController@edit')->name('pemohon.edit');
    Route::post('update/{id}','PemohonController@update')->name('pemohon.update');
    Route::post('destroy', 'PemohonController@destroy')->name('pemohon.destroy');

});


// Warkah
Route::group(['prefix' => 'warkah', 'middleware'=>'sentinel_auth'], function () {

	Route::get('list', 'WarkahController@index')->name('warkah.index');
	Route::get('bind', 'WarkahController@bind')->name('warkah.bind');
    Route::post('create', 'WarkahController@store')->name('warkah.store');
    Route::get('edit/{id}', 'WarkahController@edit')->name('warkah.edit');
    Route::post('update/{id}','WarkahController@update')->name('warkah.update');
    Route::post('destroy', 'WarkahController@destroy')->name('warkah.destroy');

});


// Buku Tanah
Route::group(['prefix' => 'buku-tanah', 'middleware'=>'sentinel_auth'], function () {

	Route::get('list', 'BukuTanahController@index')->name('buku-tanah.index');
	Route::get('bind', 'BukuTanahController@bind')->name('buku-tanah.bind');
	Route::get('desalist/{kecamatan_id}', 'BukuTanahController@getDesaList')->name('buku-tanah.desalist');
    Route::post('create', 'BukuTanahController@store')->name('buku-tanah.store');
    Route::get('edit/{id}', 'BukuTanahController@edit')->name('buku-tanah.edit');
    Route::post('update/{id}','BukuTanahController@update')->name('buku-tanah.update');
    Route::post('destroy', 'BukuTanahController@destroy')->name('buku-tanah.destroy');


    // Album
    Route::group(['prefix' => 'album'], function () {
    	Route::get('list', 'AlbumController@index')->name('album.index');
		Route::get('bind', 'AlbumController@bind')->name('album.bind');
	    Route::post('create', 'AlbumController@store')->name('album.store');
	    Route::get('edit/{id}', 'AlbumController@edit')->name('album.edit');
	    Route::post('update/{id}','AlbumController@update')->name('album.update');
	    Route::post('destroy', 'AlbumController@destroy')->name('album.destroy');
    });

});


// Transaksi
Route::group(['prefix' => 'transaksi', 'middleware'=>'sentinel_auth'], function () {

	// Peminjaman
    Route::group(['prefix' => 'peminjaman'], function () {

    	// Peminjaman Buku Tanah
    	Route::get('buku-tanah/list', 'PeminjamanBukuTanahController@index')->name('peminjaman-bt.index');
    	Route::get('buku-tanah/search/{term}', 'PeminjamanBukuTanahController@search')->name('peminjaman-bt.search');
    	Route::post('buku-tanah/store', 'PeminjamanBukuTanahController@store')->name('peminjaman-bt.store');
    	Route::get('buku-tanah/{nomor_pinjam}', 'PeminjamanBukuTanahController@listPeminjaman')->name('peminjaman-bt.listPeminjaman');
    	Route::post('buku-tanah/delete', 'PeminjamanBukuTanahController@detailDestroy')->name('peminjaman-bt.destroy-detail');
    	Route::get('all_peminjaman', 'PeminjamanBukuTanahController@allPeminjaman');

    	// Peminjaman Warkah
    	Route::get('warkah/list', 'PeminjamanWarkahController@index')->name('peminjaman-wr.index');

    });


    // Pengembalian
    Route::group(['prefix' => 'pengembalian'], function () {

    	// Pengembalian Buku Tanah
    	Route::get('buku-tanah/list', 'PengembalianBukuTanahController@index')->name('pengembalian-bt.index');
    	Route::get('buku-tanah/search/{term}', 'PengembalianBukuTanahController@search')->name('pengembalian-bt.search');
    	Route::post('buku-tanah/store', 'PengembalianBukuTanahController@store')->name('pengembalian-bt.store');
    	Route::get('buku-tanah/{nomor_kembali}', 'PengembalianBukuTanahController@listPengembalian')->name('pengembalian-bt.listPengembalian');
    	Route::post('buku-tanah/delete', 'PengembalianBukuTanahController@detailDestroy')->name('pengembalian-bt.destroy-detail');
    	Route::get('all_pengembalian', 'PengembalianBukuTanahController@allPengembalian');

    	// Pengembalian Warkah
    	Route::get('warkah/list', 'PengembalianWarkahController@index')->name('pengembalian-wr.index');

    });

});


// Utility
Route::group(['prefix' => 'utility', 'middleware'=>'sentinel_auth'], function () {
	
	// Lokasi
	Route::group(['prefix' => 'lokasi'], function() {

		// Kecamatan
	    Route::get('kecamatan/list', 'KecamatanController@index')->name('kecamatan.index');
	    Route::get('kecamatan/bind', 'KecamatanController@bind')->name('kecamatan.bind');
	    Route::post('kecamatan/create', 'KecamatanController@store')->name('kecamatan.store');
	    Route::get('kecamatan/edit/{id}', 'KecamatanController@edit')->name('kecamatan.edit');
	    Route::post('kecamatan/update/{id}','KecamatanController@update')->name('kecamatan.update');
	    Route::post('kecamatan/destroy', 'KecamatanController@destroy')->name('kecamatan.destroy');

	    // Desa
	    Route::get('desa/list', 'DesaController@index')->name('desa.index');
	    Route::get('desa/bind', 'DesaController@bind')->name('desa.bind');
	    Route::post('desa/create', 'DesaController@store')->name('desa.store');
	    Route::get('desa/edit/{id}', 'DesaController@edit')->name('desa.edit');
	    Route::post('desa/update/{id}','DesaController@update')->name('desa.update');
	    Route::post('desa/destroy', 'DesaController@destroy')->name('desa.destroy');

	});

	// Jabatan
    Route::get('jabatan/list', 'JabatanController@index')->name('jabatan.index');
    Route::get('jabatan/bind', 'JabatanController@bind')->name('jabatan.bind');
    Route::post('jabatan/create', 'JabatanController@store')->name('jabatan.store');
    Route::get('jabatan/edit/{id}', 'JabatanController@edit')->name('jabatan.edit');
    Route::post('jabatan/update/{id}','JabatanController@update')->name('jabatan.update');
    Route::post('jabatan/destroy', 'JabatanController@destroy')->name('jabatan.destroy');

	// Jenis Hak
    Route::get('jenis-hak/list', 'JenisHakController@index')->name('hak.index');
    Route::get('jenis-hak/bind', 'JenisHakController@bind')->name('hak.bind');
    Route::post('jenis-hak/create', 'JenisHakController@store')->name('hak.store');
    Route::get('jenis-hak/edit/{id}', 'JenisHakController@edit')->name('hak.edit');
    Route::post('jenis-hak/update/{id}','JenisHakController@update')->name('hak.update');
    Route::post('jenis-hak/destroy', 'JenisHakController@destroy')->name('hak.destroy');

});

// User
Route::group(['prefix' => 'user', 'middleware'=>'sentinel_auth'], function () {
	Route::get('profile', 'UserController@index')->name('user.profile');
	Route::post('update', 'UserController@update')->name('user.update');
	// Route::get('citylist/{province_id}', 'UserController@getCityList')->name('user.citylist');

});













