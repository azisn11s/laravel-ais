<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class GenderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mst_gender')->insert([
            'kode_jekel' => 'L',
            'nama_jekel' => 'Laki-laki',
        ]);

        DB::table('mst_gender')->insert([
            'kode_jekel' => 'P',
            'nama_jekel' => 'Perempuan',
        ]);

    }
}
