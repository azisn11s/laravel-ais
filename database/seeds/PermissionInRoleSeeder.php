<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class PermissionInRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Model::unguard();

        $roleSuperAdmin = Sentinel::findRoleBySlug('super-admin');
        $roleSuperAdmin->permissions = [

        	'menus.index' => true,
        	'menus.store' => true,
        	'menus.create' => true,

        	'about.index' => true,
        	'about.update' => true,
        	'about.getProfile' => true,

        	'home.index' => true

        ];
        $roleSuperAdmin->save();

        $roleAdmin = Sentinel::findRoleBySlug('admin');
        $roleAdmin->permissions = [

            'home.index' => true

        ];
        $roleAdmin->save();


        $roleOperator = Sentinel::findRoleBySlug('operator');
        $roleOperator->permissions = [
        	
        	'home.index' => true

        ];
         $roleOperator->save();

        $rolePeminjaman = Sentinel::findRoleBySlug('peminjaman');
        $rolePeminjaman->permissions = [

        	'home.index' => true

        ];
        $rolePeminjaman->save();

        $roleDivisi = Sentinel::findRoleBySlug('divisi');
        $roleDivisi->permissions = [

            'home.index' => true

        ];
        $roleDivisi->save();

    }
}
