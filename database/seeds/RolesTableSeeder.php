<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role_users')->truncate();
        // DB::table('roles')->truncate();

        Sentinel::getRoleRepository()->createModel()->create([
            'id' => '1',
            'slug' => 'super-admin',
            'name' => 'Super Administrator',
            'permissions' => [],
        ]);

        Sentinel::getRoleRepository()->createModel()->create([
            'id' => '2',
            'slug' => 'admin',
            'name' => 'admin',
            'permissions' => [],
        ]);

        Sentinel::getRoleRepository()->createModel()->create([
            'id' => '3',
            'slug' => 'operator',
            'name' => 'Operator',
            'permissions' => [],
        ]);

        Sentinel::getRoleRepository()->createModel()->create([
            'id' => '4',
            'slug' => 'peminjaman',
            'name' => 'Peminjaman',
            'permissions' => [],
        ]);

        Sentinel::getRoleRepository()->createModel()->create([
            'id' => '5',
            'slug' => 'divisi',
            'name' => 'Divisi Lain',
            'permissions' => [],
        ]);


        
    }
}
