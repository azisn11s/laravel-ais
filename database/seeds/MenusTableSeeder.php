<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menus')->insert([
            'id' => '1',
            'name' => 'Administrator',
            'url' => null,
            'uri' => 'administrator',
            'icon' => 'fa fa-home',
            'sort' => '100',
            'parent_id' => '0',
            'is_root' => '1',
            'is_active' => '1',
        ]);

        DB::table('menus')->insert([
            'id' => '2',
            'name' => 'Menu',
            'url' => '/administrator/menus',
            'uri' => 'administrator/menus',
            'icon' => 'fa fa-circle-o',
            'sort' => '1',
            'parent_id' => '1',
            'is_root' => '0',
            'is_active' => '1',
        ]);

        DB::table('menus')->insert([
            'id' => '3',
            'name' => 'About',
            'url' => '/administrator/about',
            'uri' => 'administrator/about',
            'icon' => 'fa fa-circle-o',
            'sort' => '2',
            'parent_id' => '1',
            'is_root' => '0',
            'is_active' => '1',
        ]);


        //AIS Menu
        DB::table('menus')->insert([
            'id' => '4',
            'name' => 'Pemohon',
            'url' => '/pemohon/list',
            'uri' => 'pemohon/list',
            'icon' => 'fa fa-street-view',
            'sort' => '1',
            'parent_id' => '0',
            'is_root' => '1',
            'is_active' => '1',
        ]);

        DB::table('menus')->insert([
            'id' => '5',
            'name' => 'Buku Tanah',
            'url' => '/buku-tanah/list',
            'uri' => 'buku-tanah/list',
            'icon' => 'fa fa-credit-card',
            'sort' => '2',
            'parent_id' => '0',
            'is_root' => '1',
            'is_active' => '1',
        ]);

        DB::table('menus')->insert([
            'id' => '6',
            'name' => 'Album Buku Tanah',
            'url' => '/buku-tanah/album/list',
            'uri' => 'buku-tanah/album/list',
            'icon' => 'fa fa-file-photo-o',
            'sort' => '4',
            'parent_id' => '0',
            'is_root' => '1',
            'is_active' => '1',
        ]);

        DB::table('menus')->insert([
            'id' => '7',
            'name' => 'Transaksi Buku Tanah',
            'url' => null,
            'uri' => 'transaksi',
            'icon' => 'fa fa-object-ungroup',
            'sort' => '5',
            'parent_id' => '0',
            'is_root' => '1',
            'is_active' => '1',
        ]);

        DB::table('menus')->insert([
            'id' => '8',
            'name' => 'Peminjaman',
            'url' => '/transaksi/peminjaman/buku-tanah/list',
            'uri' => 'transaksi/peminjaman/buku-tanah/list',
            'icon' => 'fa fa-circle-o',
            'sort' => '1',
            'parent_id' => '7',
            'is_root' => '0',
            'is_active' => '1',
        ]);

        DB::table('menus')->insert([
            'id' => '9',
            'name' => 'Pengembalian',
            'url' => '/transaksi/pengembalian/buku-tanah/list',
            'uri' => 'transaksi/pengembalian/buku-tanah/list',
            'icon' => 'fa fa-circle-o',
            'sort' => '2',
            'parent_id' => '7',
            'is_root' => '0',
            'is_active' => '1',
        ]);

        DB::table('menus')->insert([
            'id' => '10',
            'name' => 'Utility',
            'url' => null,
            'uri' => 'utility',
            'icon' => 'fa fa-cogs',
            'sort' => '7',
            'parent_id' => '0',
            'is_root' => '1',
            'is_active' => '1',
        ]);

        DB::table('menus')->insert([
            'id' => '11',
            'name' => 'Data Kecamatan',
            'url' => '/utility/lokasi/kecamatan/list',
            'uri' => 'utility/lokasi/kecamatan/list',
            'icon' => 'fa fa-circle-o',
            'sort' => '1',
            'parent_id' => '10',
            'is_root' => '0',
            'is_active' => '1',
        ]);

        DB::table('menus')->insert([
            'id' => '12',
            'name' => 'Data Desa',
            'url' => '/utility/lokasi/desa/list',
            'uri' => 'utility/lokasi/desa/list',
            'icon' => 'fa fa-circle-o',
            'sort' => '2',
            'parent_id' => '10',
            'is_root' => '0',
            'is_active' => '1',
        ]);

        DB::table('menus')->insert([
            'id' => '13',
            'name' => 'Jenis Hak',
            'url' => '/utility/jenis-hak/list',
            'uri' => 'utility/jenis-hak/list',
            'icon' => 'fa fa-circle-o',
            'sort' => '3',
            'parent_id' => '10',
            'is_root' => '0',
            'is_active' => '1',
        ]);

        DB::table('menus')->insert([
            'id' => '14',
            'name' => 'Warkah',
            'url' => '/warkah/list',
            'uri' => 'warkah/list',
            'icon' => 'fa fa-black-tie',
            'sort' => '3',
            'parent_id' => '0',
            'is_root' => '1',
            'is_active' => '1',
        ]);

        DB::table('menus')->insert([
            'id' => '15',
            'name' => 'Transaksi Warkah',
            'url' => null,
            'uri' => 'transaksi',
            'icon' => 'fa fa-book',
            'sort' => '6',
            'parent_id' => '0',
            'is_root' => '1',
            'is_active' => '1',
        ]);

        DB::table('menus')->insert([
            'id' => '16',
            'name' => 'Peminjaman',
            'url' => '/transaksi/peminjaman/warkah/list',
            'uri' => 'transaksi/peminjaman/warkah/list',
            'icon' => 'fa fa-circle-o',
            'sort' => '1',
            'parent_id' => '15',
            'is_root' => '0',
            'is_active' => '1',
        ]);

        DB::table('menus')->insert([
            'id' => '17',
            'name' => 'Pengembalian',
            'url' => '/transaksi/pengembalian/warkah/list',
            'uri' => 'transaksi/pengembalian/warkah/list',
            'icon' => 'fa fa-circle-o',
            'sort' => '2',
            'parent_id' => '15',
            'is_root' => '0',
            'is_active' => '1',
        ]);

        DB::table('menus')->insert([
            'id' => '18',
            'name' => 'Data Jabatan',
            'url' => '/utility/jabatan/list',
            'uri' => 'utility/jabatan/list',
            'icon' => 'fa fa-circle-o',
            'sort' => '3',
            'parent_id' => '10',
            'is_root' => '0',
            'is_active' => '1',
        ]);

        /*DB::table('menus')->insert([
            'id' => '10',
            'name' => 'Project',
            'url' => null,
            'uri' => 'project',
            'icon' => 'fa fa-file-text-o',
            'sort' => '4',
            'parent_id' => '0',
            'is_root' => '1',
            'is_active' => '1',
        ]);

        DB::table('menus')->insert([
            'id' => '11',
            'name' => 'Project List',
            'url' => '/project/list',
            'uri' => 'project/list',
            'icon' => 'fa fa-circle-o',
            'sort' => '1',
            'parent_id' => '10',
            'is_root' => '0',
            'is_active' => '1',
        ]);

         DB::table('menus')->insert([
            'id' => '12',
            'name' => 'Comission Payment Method',
            'url' => '/project/comission',
            'uri' => 'project/comission',
            'icon' => 'fa fa-circle-o',
            'sort' => '2',
            'parent_id' => '10',
            'is_root' => '0',
            'is_active' => '1',
        ]);*/

    }
}
