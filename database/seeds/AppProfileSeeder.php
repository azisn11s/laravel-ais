<?php

use Illuminate\Database\Seeder;

class AppProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('app_profiles')->insert([
            'app_name' => 'A.I.S',
            'jargon' => 'Sistem Informasi Arsip Tanah',
            'company_name' => 'HyBrings',
            'year' => '2017',
            'app_version' => '1.0.0',
        ]);
    }
}
