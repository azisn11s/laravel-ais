<?php

use Illuminate\Database\Seeder;

use Hybrings\Classes\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('log_agent_elearnings')->truncate();
        // DB::table('mst_agents')->truncate();
        // DB::table('mst_project_owners')->truncate();
        // DB::table('users')->truncate();
        DB::table('activations')->truncate();
        DB::table('persistences')->truncate();
        DB::table('reminders')->truncate();
        DB::table('role_users')->truncate();
        DB::table('throttle')->truncate();


        // Super Admin...
        $sa = Sentinel::registerAndActivate([
        	'username' => 'sa',
            'email' => 'sa@hybrings.com',
            'password' => 'admin12345',
            'type' => 'super-admin',
        ]);

        Sentinel::findRoleBySlug('super-admin')->users()->attach($sa);

        // Basic Admin...
        $admin = Sentinel::registerAndActivate([
            'username' => 'admin',
            'email' => 'admin@hybrings.com',
            'password' => 'admin12345',
            'type' => 'admin',
        ]);

        Sentinel::findRoleBySlug('admin')->users()->attach($admin);


    }
}
