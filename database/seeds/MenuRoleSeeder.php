<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class MenuRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// role_id : Super Admin
    	for ($i=0; $i < 18; $i++) { 
    		DB::table('menu_role')->insert([
	            'role_id' => 1,
	            'menu_id' => 1+$i
	        ]);
    	}

        // role_id : Admin
        for ($i=3; $i < 18; $i++) { 
            DB::table('menu_role')->insert([
                'role_id' => 2,
                'menu_id' => 1+$i
            ]);
        }

    	// role_id : Project Owner
    	/*for ($i=3; $i < 12; $i++) { 
    		DB::table('menu_role')->insert([
	            'role_id' => 2,
	            'menu_id' => 1+$i
	        ]);
    	}*/

    	// role_id : Agent
    	/*for ($i=5; $i < 12; $i++) { 
    		DB::table('menu_role')->insert([
	            'role_id' => 3,
	            'menu_id' => 1+$i
	        ]);
    	}*/
        
    }
}
