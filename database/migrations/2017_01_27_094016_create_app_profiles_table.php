<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('app_name', 30);
            $table->string('jargon', 100)->nullable();
            $table->string('company_name', 100);
            $table->integer('year')->nullable();
            $table->string('app_icon')->nullable();
            $table->char('app_version', 5)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('app_profiles');
    }
}
