<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogDetailPinjamBukuTanahTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_detail_pinjam_buku_tanah', function (Blueprint $table) {
            $table->string('nomor_pinjam', 10);
            $table->bigInteger('buku_tanah_id')->unsigned();
            $table->integer('jumlah_buku');
            $table->timestamps();
        });

        Schema::table('log_detail_pinjam_buku_tanah', function($table) {
            $table->foreign('nomor_pinjam')->references('nomor_pinjam')->on('log_pinjam');
            $table->foreign('buku_tanah_id')->references('id')->on('mst_buku_tanah');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('log_detail_pinjam_buku_tanah');
    }
}
