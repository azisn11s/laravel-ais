<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterWarkahTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_warkah', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nw_barcode', 20)->unique();
            $table->string('jenis', 10)->nullable();
            $table->string('no_warkah', 10);
            $table->char('tahun', 4);
            $table->integer('album_id')->unsigned();
            $table->string('status', 8);
            $table->char('record_status', 1)->default('A');
            $table->timestamps();
        });

        Schema::table('mst_warkah', function($table) {
            $table->foreign('album_id')->references('id')->on('mst_album');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mst_warkah');
    }
}
