<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSomeRelationshipMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mst_buku_tanah', function (Blueprint $table) {
            $table->foreign('hak_id')->references('id')->on('mst_jenis_hak')->onUpdate('cascade');
        });

        Schema::table('mst_pemohon', function (Blueprint $table) {
            $table->foreign('jekel_id')->references('id')->on('mst_gender')->onUpdate('cascade');
            $table->foreign('jabatan_id')->references('id')->on('mst_jabatan')->onUpdate('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mst_buku_tanah', function (Blueprint $table) {
            $table->dropForeign('mst_buku_tanah_hak_id_foreign');
        });

        Schema::table('mst_pemohon', function (Blueprint $table) {
            $table->dropForeign('mst_pemohon_jekel_id_foreign');
            $table->dropForeign('mst_pemohon_jabatan_id_foreign');
        });

    }
}
