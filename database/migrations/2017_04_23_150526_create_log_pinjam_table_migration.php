<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogPinjamTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_pinjam', function (Blueprint $table) {
            $table->string('nomor_pinjam', 10)->primary();
            $table->dateTime('tanggal_pinjam');
            $table->integer('total_pinjam')->nullable();
            $table->bigInteger('pemohon_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('note')->nullable();
            $table->timestamps();
        });

        Schema::table('log_pinjam', function($table) {
            $table->foreign('pemohon_id')->references('id')->on('mst_pemohon');
            $table->foreign('user_id')->references('id')->on('users');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('log_pinjam');
    }
}
