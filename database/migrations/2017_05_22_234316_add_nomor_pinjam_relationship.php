<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNomorPinjamRelationship extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_detail_kembali_buku_tanah', function (Blueprint $table) {
            $table->string('nomor_pinjam', 10);
            $table->foreign('nomor_pinjam')->references('nomor_pinjam')->on('log_detail_pinjam_buku_tanah')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_detail_kembali_buku_tanah', function (Blueprint $table) {
            $table->dropForeign('log_detail_kembali_buku_tanah_nomor_pinjam_foreign');
            $table->dropColumn('nomor_pinjam');
        });
    }
}
