<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterJabatanTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_jabatan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_jabatan', 5)->unique();
            $table->string('nama_jabatan', 100);
            $table->char('record_status', 1)->default('A');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mst_jabatan');
    }
}
