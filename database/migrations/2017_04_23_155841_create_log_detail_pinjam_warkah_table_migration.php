<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogDetailPinjamWarkahTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_detail_pinjam_warkah', function (Blueprint $table) {
            $table->string('nomor_pinjam', 10);
            $table->bigInteger('warkah_id')->unsigned();
            $table->integer('jumlah_buku');
            $table->timestamps();
        });

        Schema::table('log_detail_pinjam_warkah', function($table) {
            $table->foreign('nomor_pinjam')->references('nomor_pinjam')->on('log_pinjam');
            $table->foreign('warkah_id')->references('id')->on('mst_warkah');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('log_detail_pinjam_warkah');
    }
}
