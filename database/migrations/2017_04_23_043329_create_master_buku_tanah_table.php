<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterBukuTanahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_buku_tanah', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nb_barcode', 35)->unique();
            $table->integer('hak_id')->unsigned();
            $table->string('no_hak', 15);
            $table->integer('album_id')->nullable()->unsigned();
            $table->integer('luas');
            $table->string('pemegang_hak', 50);
            $table->integer('desa_id')->unsigned();
            $table->char('status', 1)->default('1');
            $table->char('record_status', 1)->default('A');
            $table->timestamps();
        });

        Schema::table('mst_buku_tanah', function($table) {
            $table->foreign('desa_id')->references('id')->on('mst_desa');
            $table->foreign('album_id')->references('id')->on('mst_album');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mst_buku_tanah');
    }
}
