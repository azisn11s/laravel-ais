<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterPemohonTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_pemohon', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('nip', 35)->unique();
            $table->string('nama_lengkap', 50);
            $table->integer('jekel_id')->unsigned();
            $table->integer('jabatan_id')->unsigned();
            $table->string('alamat')->nullable();
            $table->char('record_status', 1)->default('A');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mst_pemohon');
    }
}
