<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterAlbumTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_album', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_album', 10)->unique();
            $table->string('lemari', 10)->nullable();
            $table->string('rak', 10)->nullable();
            $table->string('blok', 10)->nullable();
            $table->char('record_status', 1)->default('A');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mst_album');
    }
}
