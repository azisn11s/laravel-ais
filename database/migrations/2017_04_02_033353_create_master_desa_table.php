<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterDesaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_desa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_desa', 5)->unique();
            $table->string('nama_desa', 125);
            $table->char('record_status', 1)->default('A');
            $table->integer('id_kecamatan')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mst_desa');
    }

}
