<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationshipMasterDesa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mst_desa', function (Blueprint $table) {
            $table->foreign('id_kecamatan')->references('id')->on('mst_kecamatan')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mst_desa', function (Blueprint $table) {
            $table->dropForeign('mst_desa_id_kecamatan_foreign');
        });
    }
}
