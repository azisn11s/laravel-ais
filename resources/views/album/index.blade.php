@extends('layouts.app')

	@section('content')
	
		{{ csrf_field() }}
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Album</h3>
					<div class="col-md-2 box-tools">
						<a id="btn_add" class="btn btn-primary btn-block btn-sm" href="#">Tambah Album</a>
					</div>
				</div>
				<div class="box-body">
					{{-- $html->table(['class'=>'table table-bordered table-striped']) --}}
					<table id="example2" class="table table-bordered table-hover">
		                <thead>
			                <tr>
								<th>Nomor Album</th>
								<th>Lemari</th>
								<th>Rak</th>
								<th>Blok</th>
								<th></th>
			                </tr>
		                </thead>
		            </table>
	            </div>
	            <!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>


		<div class="modal fade" id="modal_album" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        				<h4 class="modal-title" id="myModalLabel">Form Data Album</h4>
					</div>
					{!! Form::open(['route' => 'album.store', 'id'=>'form-album']) !!}
						<div class="modal-body">
							<div class="box box-info">
								<div class="box-body">
									<div class="row" id="alert-report"></div>
									<div class="row">
										<div class="form-group">
						                  {!! Form::label('no_album', 'No. Album', ['class'=>'col-sm-3 control-label']) !!}
						                  <div class="col-sm-4">
						                  		{!! Form::text('no_album', null, ['class'=>'form-control','id'=>'no_album','placeholder'=>'Nomor Album']) !!}
						                  </div>
						                  <div class="col-sm-offset-3 col-sm-7 no_album error-input"></div>
						                </div>
									</div>
					                <br>
					                <div class="row">
										<div class="form-group">
						                  {!! Form::label('lemari', 'Lemari', ['class'=>'col-sm-3 control-label']) !!}
						                  <div class="col-sm-7">
						                  		{!! Form::text('lemari', null, ['class'=>'form-control','id'=>'lemari','placeholder'=>'Lemari']) !!}
						                  </div>
						                  <div class="col-sm-offset-3 col-sm-7 lemari error-input"></div>
						                </div>
									</div>
									<br>
									<div class="row">
										<div class="form-group">
						                  {!! Form::label('rak', 'Rak', ['class'=>'col-sm-3 control-label']) !!}
						                  <div class="col-sm-7">
						                  		{!! Form::text('rak', null, ['class'=>'form-control','id'=>'rak','placeholder'=>'Rak']) !!}
						                  </div>
						                  <div class="col-sm-offset-3 col-sm-7 rak error-input"></div>
						                </div>
									</div>
									<br>
									<div class="row">
										<div class="form-group">
						                  {!! Form::label('blok', 'Blok', ['class'=>'col-sm-3 control-label']) !!}
						                  <div class="col-sm-7">
						                  		{!! Form::text('blok', null, ['class'=>'form-control','id'=>'blok','placeholder'=>'Blok']) !!}
						                  </div>
						                  <div class="col-sm-offset-3 col-sm-7 blok error-input"></div>
						                </div>
									</div>
									<br>
					                
								</div>
							</div>
							
						</div>
						<div class="box-footer">
		                <button type="button" onclick="hideModal('modal_album','form-album')" class="btn btn-default" data-dismiss="modal">Cancel</button>
		                {!! Form::submit('Submit', [
		                	'class'=>'btn btn-info pull-right btn_submit',
		                	'id'=>'btn_submit',
		                	'data-loading-text'=>'Loading..',
		                	'autocomplete'=>'off']) 
		                !!}

		              </div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>

		<!-- MODAL DELETE -->
		<div class="modal fade" id="modal_album_delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        				<h4 class="modal-title" id="myModalLabel">Hapus Data Album</h4>
					</div>
					<div class="modal-body">
						{!! csrf_field() !!}
						<input type="hidden" id="HiddenDeleteId" name="HiddenDeleteId">
						<p style="">Apakah anda yakin akan menghapus baris data?</p>
					</div>
					<div class="box-footer">
		                <button type="button" onclick="del()" class="btn btn-danger pull-right" data-dismiss="modal">Delete</button>
		                <button type="button" class="btn btn-default pull-right" data-dismiss="modal" style="margin-right:10px">Batal</button>
	              	</div>
				</div>
			</div>
		</div>

	@endsection


	@section('scripts')
		
		<script>
			var table;

		  	$(function () {

			    table = $('#example2').DataTable({
			    	processing: true,
		            serverSide: true,
		            ajax: '{{ url('buku-tanah/album/bind') }}',
		            columns: [
			            {data: 'no_album', name: 'no_album'},
			            {data: 'lemari', name: 'lemari'},
			            {data: 'rak', name: 'rak'},
			            {data: 'blok', name: 'blok'},
			            {data: 'action', name: 'action', orderable: false, searchable: false}
			        ],

			        // "order": [[ 4, "desc" ]],
			    });
		    });


		  	$(document).ready(function(){

		  		//Trigger Modal PopUp
		        $("#btn_add").click(function(){
		        	$("#btn_submit").val('Submit'); 
		            $("#modal_album").modal("show");
		            $('#form-album').attr('action', '{{ route('album.store') }}').attr('method','POST');
		        });

		        //Form Save (SUBMIT CLICKED)
		        $("#form-album").on('submit', function(e){
		            e.preventDefault();
		            resetErrors();

		            var url = $(this).attr('action');
		            var method = $(this).attr('method');
		            // var dataForm = $(this).serialize();

		            // var dataForm = $(this).serializeArray();
		            var dataForm = new FormData($(this)[0]);

		            dataForm.append('_token', $('input[name=_token]').val());


		            var btn = $(".btn_submit");

	                $.ajax({
	                    url:url,
	                    type:method,
	                    data:dataForm,
	                    dataType: 'json',
	                    async:false,
	                    processData: false,
						contentType: false,
	                    beforeSend: function(){
	                        // btn.button('loading');
	                    },
	                    success: function(response){
							var dt = response;
							if(dt.errors){
								var errors = dt.errors;
								$.each(errors, function(key, value){
									$('label[for="'+ key +'"]').parent('div').addClass('has-error');
									$('.'+key+'.error-input').append('<span class="help-block">'+ value +'</span>');
								});
							}else{
								hideModal('modal_album','form-album');
								table.ajax.reload();
								toastr.success(dt.success);								
							}

							// btn.button('reset');
	                    },
	                    error: function(resp) {
	                    	//toastr.error('Problems when inserting to database.');
	                    	var errors = resp.responseJSON;
	                    	console.log(errors);
	                    	$.each(errors, function(key, value) {
					            $('label[for="'+ key +'"]').parent('div').addClass('has-error');
								$('.'+key+'.error-input').append('<span class="help-block">'+ value +'</span>');
					        });
						                    	
	                    	// btn.button('reset');
	                    }              
	                });
	                
		            
		        });

		  	});


		  	function edit(id){
		        
		        $.ajax({
		            url : '{{ url('buku-tanah/album/edit') }}/' + id,
		            type: "GET",
		            dataType: "JSON",
		            success: function(data){
		            	$("#btn_submit").val('Update');                     
		                $('#modal_album').modal('show'); 

		                $('#form-album').attr('action', data.url_action).attr('method','POST');
		                $('#no_album').val(data.album.no_album);
		                $('#lemari').val(data.album.lemari);
		                $('#rak').val(data.album.rak);
		                $('#blok').val(data.album.blok);
		                // selectize_("#kecamatan", data.album.id_kecamatan);

		            },
		            error: function (jqXHR, textStatus, errorThrown){
		                alert('Error get data from ajax');
		            }
		        });
		    }

		    function delConfirm(id){
		        var id = id;
		        $("#modal_album_delete").modal("show");
		        $("#HiddenDeleteId").val(id);
		    }

		    function del(){
		        var id = $("#HiddenDeleteId").val(); 
		        
		        $.ajax({
		            url : '{{ url('buku-tanah/album/destroy') }}',
		            type: "POST",
		            //dataType: "JSON",
		            data: {
				      '_token': $('input[name=_token]').val(),
				      'id': id
				    },
		            success: function(data){
		               $('#modal_album_delete').modal('hide');
		               table.ajax.reload();
		               toastr.success(data.success);
		            },
		            error: function (jqXHR, textStatus, errorThrown){
		                alert('Error deleting data.');
		            }
		        }); 
		    }


		  	function resetErrors() {
			    // $('form input, form select').removeClass('inputTxtError');
			    $('span.help-block').remove();
			    $('.form-group').removeClass('has-error');
			}


		</script>

	@endsection