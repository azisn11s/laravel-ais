<footer class="main-footer">
	<div class="pull-right hidden-xs">
		<b>Version</b> {!! $app_profile->app_version !!}
	</div>
	<strong>Copyright © {!! $app_profile->year !!} <a href="#">{!! $app_profile->company_name !!}</a>.</strong> All rights reserved.
</footer>