<ul class="sidebar-menu">
  <li class="header">MAIN MENU</li>

    @for($i=0; $i < count($app_menu); $i++)

      @php 
        $parent = $app_menu[$i]["parent"];
        $childs = $app_menu[$i]["childs"]; 
      @endphp

      @if(count($childs) < 1)
        <li class="{!! ($parent->uri == Request::path()) ? 'active' : '' !!}"><a href="{{ $parent->url }}"><i class="{{ $parent->icon }}"></i> <span> {{ $parent->name }}</span></a></li>
      @else
        <li class="treeview {!! (Request::segment(1) == $parent->uri) ? 'active' : '' !!}">
          <a href="#">
            <i class="{{ $parent->icon }}"></i> <span> {{ $parent->name }}</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @foreach($childs as $child)
              <li class="{!! ($child->uri == Request::path()) ? 'active' : '' !!}">
                <a href="{{ URL($child->url) }}"><i class="{{ $child->icon }}"></i> {{ $child->name }}</a>
              </li>
            @endforeach
          </ul> 
        </li>
      @endif

    @endfor

</ul>

