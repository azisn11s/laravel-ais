<!-- user panel (Optional) -->
<div class="user-panel">
  <div class="pull-left image">
    <img src="{{ (!is_null(Sentinel::getUser()->avatar)) ?  URL('images/avatar/user')."/".Sentinel::getUser()->avatar : URL('images/avatar/admin/user-default.png') }}" class="img-circle" style="background-color: white;">
  </div>
  <div class="pull-left info">
    <p>{{ (!is_null(Sentinel::getUser()->username)) ? ucfirst(Sentinel::getUser()->username) : ucfirst(Sentinel::getUser()->email) }}</p>

    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
  </div>
</div>
<!-- /.user-panel -->

<!-- Search Form (Optional) -->
<form action="#" method="get" class="sidebar-form">
  <div class="input-group">
    <input type="text" name="q" class="form-control" placeholder="Search...">
    <span class="input-group-btn">
      <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
    </span>
  </div>
</form>
<!-- /.sidebar-form -->