<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title></title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ URL::asset('assets/bootstrap/css/bootstrap.min.css') }}">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ URL::asset('assets/admin-lte/font-awesome/css/font-awesome.min.css') }}" >

    <!-- Admin LTE -->
    <link rel="stylesheet" href="{{ URL::asset('assets/admin-lte/dist/css/AdminLTE_fonts.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/admin-lte/dist/css/AdminLTE.min.css') }}"> 

    <!-- iCheck -->
    <link rel="stylesheet" href="{{ URL::asset('assets/admin-lte/plugins/iCheck/square/blue.css') }}"> 

    <!-- Custom -->
    {!! Html::style('assets/custom/css/custom.css') !!} 

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body class="hold-transition">
    
    @yield('content')   


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ URL::asset('assets/jquery/jquery-3.1.1.min.js') }}"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ URL::asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>

    <!-- AdminLTE App -->
    <script src="{{ URL::asset('assets/admin-lte/dist/js/app.min.js') }}"></script>

    <!-- iCheck -->
    <script src="{{ URL::asset('assets/admin-lte/plugins/iCheck/icheck.min.js') }}"></script>
    

    @yield('scripts')

    
    

  </body>
</html>