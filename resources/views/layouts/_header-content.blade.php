<!-- Header Content-->
<section class="content-header">
	@if(!is_null($headerContent))
		  <h1> {!! $headerContent['title'] !!} <small>{!! $headerContent['description'] !!}</small></h1>
		  @if(count($headerContent['breadcrumb']))
			<ol class="breadcrumb">
				@foreach($headerContent['breadcrumb'] as $breadcrumb)
					<li class="{!! $breadcrumb['active'] !!}">
						<a href="{!! (!empty($breadcrumb['routeName'])) ? route($breadcrumb['routeName']) : '#' !!}"><i class="{!! (!empty($breadcrumb['icon'])) ? 'fa '.$breadcrumb['icon'] : '' !!}"></i>{!! $breadcrumb['name'] !!}</a>
					</li>
				@endforeach
			</ol>
		  @endif
	@endif
</section>
