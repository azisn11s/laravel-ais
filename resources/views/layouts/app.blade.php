<!DOCTYPE html>
<html>
  <head>
    {!! Html::meta(null, null, ['charset' => 'UTF-8']) !!}
    {!! Html::meta('robots', 'noindex, nofollow') !!}
    {!! Html::meta(null, 'IE=edge', ['http-equiv'=>'X-UA-Compatible']) !!}
    {!! Html::meta('viewport', 'width=device-width, initial-scale=1') !!}
    {{-- {!! Html::meta('csrf-token', csrf_token()) !!} --}}

    <title>{!! $app_profile->app_name !!}</title>

    <!-- Bootstrap -->
    {!! Html::style('assets/bootstrap/css/bootstrap.min.css') !!} 

    <!-- Font Awesome -->
    {!! Html::style('assets/admin-lte/font-awesome/css/font-awesome.min.css') !!} 

    <!-- Admin LTE -->
    {!! Html::style('assets/admin-lte/dist/css/AdminLTE_fonts.css') !!} 
    {!! Html::style('assets/admin-lte/dist/css/AdminLTE.min.css') !!} 
    {!! Html::style('assets/admin-lte/dist/css/skins/_all-skins.min.css') !!} 

    <!-- DataTables -->
    {!! Html::style('assets/dataTables/css/dataTables.bootstrap.css') !!} 
    {!! Html::style('assets/dataTables/css/jquery.dataTables.css') !!} 

    <!-- Selectize -->
    {!! Html::style('assets/selectize/css/selectize.css') !!} 
    {!! Html::style('assets/selectize/css/selectize.bootstrap3.css') !!} 

    <!-- Typeahead -->
    {!! Html::style('assets/typeahead/typeahead.css') !!} 

    <!-- upload-style -->
    {!! Html::style('assets/upload-style/normalize.css') !!} 
    {!! Html::style('assets/upload-style/component.css') !!} 

    <!-- Bootstrap Datepicker -->
    {!! Html::style('assets/admin-lte/plugins/datepicker/datepicker3.css') !!} 

    <!-- Toastr Notification -->
    {!! Html::style('assets/toastr/toastr.css') !!}


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body class="skin-blue fixed">
    <div class="wrapper">
      <!-- Main Header -->
      @include('layouts._header')
      
      <!-- Left Sider Bar -->
      <aside class="main-sidebar">
        <!-- Inner sidebar -->
        <section class="sidebar">
          <!-- user panel (Optional) -->
          @include('layouts._user-panel')
          <!-- /.user-panel -->

          <!-- Sidebar Menu -->
          @include('layouts._left-bar')
          <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
      </aside>
      <!-- /.main-sidebar -->

      <!-- Content Wrapper -->
      <div class="content-wrapper">
        <!-- Header Content -->
        @if(isset($headerContent))
          @include('layouts._header-content', $headerContent)
        @endif
        {{-- @yield('header-content') --}}
        <!-- Main Content -->
        <div class="content">
          <div class="row">

            @yield('content')
            
          </div>
        </div>
      </div>

      <!-- Footer -->
      @include('layouts._footer')

    </div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    {!! Html::script('assets/jquery/jquery-3.1.1.min.js') !!} 

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    {!! Html::script('assets/bootstrap/js/bootstrap.min.js') !!} 

    <!-- AdminLTE App -->
    {!! Html::script('assets/admin-lte/dist/js/app.min.js') !!} 


    <!-- DataTables -->
    {!! Html::script('assets/dataTables/js/jquery.dataTables.min.js') !!} 
    {!! Html::script('assets/dataTables/js/dataTables.bootstrap.min.js') !!}

    <!-- SlimScroll -->
    {!! Html::script('assets/admin-lte/plugins/slimScroll/jquery.slimscroll.min.js') !!}

    <!-- Selectize -->
    {!! Html::script('assets/selectize/js/selectize.min.js') !!}

    <!-- Typeahead -->
    {!! Html::script('assets/typeahead/typeahead.bundle.js') !!}

    <!-- Bootstrap Datepicker -->
    {!! Html::script('assets/admin-lte/plugins/datepicker/bootstrap-datepicker.js') !!}

    <!-- Upload style -->
    {!! Html::script('assets/upload-style/custom-file-input.js') !!}

    <!-- Toastr Notification -->
    {!! Html::script('assets/toastr/toastr.js') !!}

    <!-- numeric class -->
    {!! Html::script('assets/jquery/jquery.numeric.js') !!}

    <!-- InputMask Library -->
    {!! Html::script('assets/admin-lte/plugins/input-mask/jquery.inputmask.js') !!}
    {!! Html::script('assets/admin-lte/plugins/input-mask/jquery.inputmask.date.extensions.js') !!}
    {!! Html::script('assets/admin-lte/plugins/input-mask/jquery.inputmask.extensions.js') !!}

    {!! Html::script('assets/custom/js/custom.js') !!}

    @yield('scripts')
    
  </body>
</html>