@extends('layouts.app')

	@section('content')
	
		{{ csrf_field() }}
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Warkah</h3>
					<div class="col-md-2 box-tools">
						<a id="btn_add" class="btn btn-primary btn-block btn-sm" href="#">Tambah Warkah</a>
					</div>
				</div>
				<div class="box-body">
					{{-- $html->table(['class'=>'table table-bordered table-striped']) --}}
					<table id="example2" class="table table-bordered table-hover">
		                <thead>
			                <tr>
								<th>NW_Barcode</th>
								<th>Jenis</th>
								<th>Nomor Warkah</th>
								<th>Tahun</th>
								<th>No. Album</th>
								<th>Status</th>
								<th></th>
			                </tr>
		                </thead>
		            </table>
	            </div>
	            <!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>

		
		<!-- MAIN MODAL FORM --> 
		<div class="modal fade" id="modal_warkah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        				<h4 class="modal-title" id="myModalLabel">Form Data Warkah</h4>
					</div>
					{!! Form::open(['route' => 'warkah.store', 'id'=>'form-warkah']) !!}
						<div class="modal-body">
							<div class="box box-info">
								<div class="box-body">
									<div class="row" id="alert-report"></div>
									<div class="row">
										<div class="form-group">
						                  {!! Form::label('nw_barcode', 'Barcode', ['class'=>'col-sm-3 control-label']) !!}
						                  <div class="col-sm-4">
						                  		{!! Form::text('nw_barcode', null, ['class'=>'form-control','id'=>'nw_barcode','placeholder'=>'Barcode']) !!}
						                  </div>
						                  <div class="col-sm-offset-3 col-sm-7 nw_barcode error-input"></div>
						                </div>
									</div>
					                <br>
					                <div class="row">
										<div class="form-group">
						                  {!! Form::label('jenis', 'Jenis', ['class'=>'col-sm-3 control-label']) !!}
						                  <div class="col-sm-7">
						                  		{!! Form::text('jenis', null, ['class'=>'form-control','id'=>'jenis','placeholder'=>'Jenis ']) !!}
						                  </div>
						                  <div class="col-sm-offset-3 col-sm-7 jenis error-input"></div>
						                </div>
									</div>
									<br>
									<div class="row">
										<div class="form-group">
						                  {!! Form::label('no_warkah', 'Nomor', ['class'=>'col-sm-3 control-label']) !!}
						                  <div class="col-sm-7">
						                  		{!! Form::text('no_warkah', null, ['class'=>'form-control','id'=>'no_warkah','placeholder'=>'Nomor ']) !!}
						                  </div>
						                  <div class="col-sm-offset-3 col-sm-7 no_warkah error-input"></div>
						                </div>
									</div>
									<br>
									<div class="row">
										<div class="form-group">
						                  {!! Form::label('tahun', 'Tahun', ['class'=>'col-sm-3 control-label']) !!}
						                  <div class="col-sm-4">
						                  		{!! Form::number('tahun', null, ['class'=>'form-control','id'=>'tahun','placeholder'=>'Tahun ']) !!}
						                  </div>
						                  <div class="col-sm-offset-3 col-sm-7 tahun error-input"></div>
						                </div>
									</div>
									<br>
					                <div class="row">
										<div class="form-group">
						                  {!! Form::label('album_id', 'Nomor Album', ['class'=>'col-sm-3 control-label']) !!}
						                  <div class="col-sm-7">
							                  	{!! Form::select('album_id', $data['no_album'], null, [
												  'class'=>'form-control js-selectize',
												  'id'=>'no_album',
												  'placeholder' => 'Pilih Nomor Album'
												  ]) 
												!!}
						                  </div>
						                  <div class="col-sm-offset-3 col-sm-7 album_id error-input"></div>
						                </div>
									</div>
									<div class="row">
										<div class="form-group">
						                  {!! Form::label('status', 'Status', ['class'=>'col-sm-3 control-label']) !!}
						                  <div class="col-sm-7">
						                  		{!! Form::text('status', null, ['class'=>'form-control','id'=>'status','placeholder'=>'Status ']) !!}
						                  </div>
						                  <div class="col-sm-offset-3 col-sm-7 status error-input"></div>
						                </div>
									</div>
					                <br>
					                
								</div>
							</div>
							
						</div>
						<div class="box-footer">
		                <button type="button" onclick="hideModal('modal_warkah','form-warkah')" class="btn btn-default" data-dismiss="modal">Cancel</button>
		                {!! Form::submit('Submit', [
		                	'class'=>'btn btn-info pull-right btn_submit',
		                	'id'=>'btn_submit',
		                	'data-loading-text'=>'Loading..',
		                	'autocomplete'=>'off']) 
		                !!}

		              </div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>

		<!-- MODAL DELETE -->
		<div class="modal fade" id="modal_warkah_delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        				<h4 class="modal-title" id="myModalLabel">Hapus Data Warkah</h4>
					</div>
					<div class="modal-body">
						{!! csrf_field() !!}
						<input type="hidden" id="HiddenDeleteId" name="HiddenDeleteId">
						<p style="">Apakah anda yakin akan menghapus baris data?</p>
					</div>
					<div class="box-footer">
		                <button type="button" onclick="del()" class="btn btn-danger pull-right" data-dismiss="modal">Delete</button>
		                <button type="button" class="btn btn-default pull-right" data-dismiss="modal" style="margin-right:10px">Batal</button>
	              	</div>
				</div>
			</div>
		</div>

	@endsection


	@section('scripts')
		
		<script>
			var table;

		  	$(function () {

			    table = $('#example2').DataTable({
			    	processing: true,
		            serverSide: true,
		            ajax: '{{ url('warkah/bind') }}',
		            columns: [
			            {data: 'nw_barcode', name: 'nw_barcode'},
			            {data: 'jenis', name: 'jenis'},
			            {data: 'no_warkah', name: 'no_warkah'},
			            {data: 'tahun', name: 'tahun'},
			            {data: 'album.no_album', name: 'album.no_album'},
			            {data: 'status', name: 'status'},
			            {data: 'action', name: 'action', orderable: false, searchable: false}
			        ],

			        // "order": [[ 4, "desc" ]],
			    });
		    });


		  	$(document).ready(function(){

		  		//Trigger Modal PopUp
		        $("#btn_add").click(function(){
		        	$("#btn_submit").val('Submit'); 
		        	combo_clear('#modal_warkah');
		            $("#modal_warkah").modal("show");
		            $('#form-warkah').attr('action', '{{ route('warkah.store') }}').attr('method','POST');
		        });

		        //Form Save (SUBMIT CLICKED)
		        $("#form-warkah").on('submit', function(e){
		            e.preventDefault();
		            resetErrors();

		            var url = $(this).attr('action');
		            var method = $(this).attr('method');
		            // var dataForm = $(this).serialize();

		            // var dataForm = $(this).serializeArray();
		            var dataForm = new FormData($(this)[0]);

		            dataForm.append('_token', $('input[name=_token]').val());


		            var btn = $(".btn_submit");

	                $.ajax({
	                    url:url,
	                    type:method,
	                    data:dataForm,
	                    dataType: 'json',
	                    async:false,
	                    processData: false,
						contentType: false,
	                    beforeSend: function(){
	                        // btn.button('loading');
	                    },
	                    success: function(response){
							var dt = response;
							if(dt.errors){
								var errors = dt.errors;
								$.each(errors, function(key, value){
									$('label[for="'+ key +'"]').parent('div').addClass('has-error');
									$('.'+key+'.error-input').append('<span class="help-block">'+ value +'</span>');
								});
							}else{
								hideModal('modal_warkah','form-warkah');
								table.ajax.reload();
								toastr.success(dt.success);								
							}

							// btn.button('reset');
	                    },
	                    error: function(resp) {
	                    	//toastr.error('Problems when inserting to database.');
	                    	var errors = resp.responseJSON;
	                    	console.log(errors);
	                    	$.each(errors, function(key, value) {
					            $('label[for="'+ key +'"]').parent('div').addClass('has-error');
								$('.'+key+'.error-input').append('<span class="help-block">'+ value +'</span>');
					        });
						                    	
	                    	// btn.button('reset');
	                    }              
	                });
	                
		            
		        });

		  	});


		  	function edit(id){
		        
		        $.ajax({
		            url : '{{ url('warkah/edit') }}/' + id,
		            type: "GET",
		            dataType: "JSON",
		            success: function(data){
		            	$("#btn_submit").val('Update');                     
		                $('#modal_warkah').modal('show'); 

		                $('#form-warkah').attr('action', data.url_action).attr('method','POST');
		                $('#nw_barcode').val(data.warkah.nw_barcode);
		                $('#jenis').val(data.warkah.jenis);
		                $('#no_warkah').val(data.warkah.no_warkah);
		                $('#tahun').val(data.warkah.tahun);
		                selectize_("#no_album", data.warkah.album_id);
		                $('#status').val(data.warkah.status);

		            },
		            error: function (jqXHR, textStatus, errorThrown){
		                alert('Error get data from ajax');
		            }
		        });
		    }

		    function delConfirm(id){
		        var id = id;
		        $("#modal_warkah_delete").modal("show");
		        $("#HiddenDeleteId").val(id);
		    }

		    function del(){
		        var id = $("#HiddenDeleteId").val(); 
		        
		        $.ajax({
		            url : '{{ url('warkah/destroy') }}',
		            type: "POST",
		            //dataType: "JSON",
		            data: {
				      '_token': $('input[name=_token]').val(),
				      'id': id
				    },
		            success: function(data){
		               $('#modal_warkah_delete').modal('hide');
		               table.ajax.reload();
		               toastr.success(data.success);
		            },
		            error: function (jqXHR, textStatus, errorThrown){
		                alert('Error deleting data.');
		            }
		        }); 
		    }


		  	function resetErrors() {
			    // $('form input, form select').removeClass('inputTxtError');
			    $('span.help-block').remove();
			    $('.form-group').removeClass('has-error');
			}


		</script>

	@endsection