@extends('layouts.auth-base')

@section('content')
    <div class="login-box">
      <div class="login-logo">
        {{-- <i class="fa fa-gg"></i> --}}
        <a href="#">{!! $app_profile->app_name !!}<b>Land</b> </a>
      </div>

      {{-- Flash Notification --}}
      @if(session()->has('flash_notification.message'))
        <div class="alert alert-{{ session()->get('flash_notification.level') }} alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <h4><i class="icon fa fa-warning"></i> Alert!</h4>
            {!! session()->get('flash_notification.message') !!}
        </div>
      @endif
      
      <!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>

        <form action="{{ url('/login') }}" method="post" role="form">
        {{ csrf_field() }}
          <div class="form-group has-feedback{{ $errors->has('username') ? ' has-error' : '' }}">
            <input class="form-control" name="username" placeholder="Username" value="{{ old('username') }}">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            @if ($errors->has('username'))
                <span class="help-block">
                    <strong>{{ $errors->first('username') }}</strong>
                </span>
            @endif
          </div>
          <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
            <input id="password" name="password" class="form-control" placeholder="Password" type="password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
          </div>
          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="remember"> Remember Me
                </label>
              </div>
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div>
            <!-- /.col -->
          </div>
        </form>


        <!-- /.social-auth-links -->

        <a href="{{ url('/password/reset') }}">I forgot my password</a><br>
        {{-- <a href="{{ url('/register') }}" class="text-center">Register a new membership</a> --}}

      </div>
      <!-- /.login-box-body -->
    </div>

@endsection
