@extends('layouts.app')

	@section('content')
	
		{{ csrf_field() }}
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Buku Tanah</h3>
					<div class="col-md-2 box-tools">
						<a id="btn_add" class="btn btn-primary btn-block btn-sm" href="#">Tambah Buku Tanah</a>
					</div>
				</div>
				<div class="box-body">
					{{-- $html->table(['class'=>'table table-bordered table-striped']) --}}
					<table id="example2" class="table table-bordered table-hover">
		                <thead>
			                <tr>
								<th>NB_Barcode</th>
								<th>Nomor Hak</th>
								<th>Nomor Album</th>
								<th>Jenis</th>
								<th>Desa</th>
								<th>Kecamatan</th>
								<th>Status</th>
								<th></th>
			                </tr>
		                </thead>
		            </table>
	            </div>
	            <!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>

		
		<!-- MAIN MODAL FORM --> 
		<div class="modal fade" id="modal_buku_tanah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        				<h4 class="modal-title" id="myModalLabel">Form Data Pemohon</h4>
					</div>
					{!! Form::open(['route' => 'buku-tanah.store', 'id'=>'form-buku-tanah']) !!}
						<div class="modal-body">
							<div class="box box-info">
								<div class="box-body">
									<div class="row" id="alert-report"></div>
									<div class="row">
										<div class="form-group">
						                  {!! Form::label('nb_barcode', 'Barcode', ['class'=>'col-sm-3 control-label']) !!}
						                  <div class="col-sm-4">
						                  		{!! Form::text('nb_barcode', null, ['class'=>'form-control','id'=>'nb_barcode','placeholder'=>'Barcode']) !!}
						                  </div>
						                  <div class="col-sm-offset-3 col-sm-7 nb_barcode error-input"></div>
						                </div>
									</div>
					                <br>
					                <div class="row">
										<div class="form-group">
						                  {!! Form::label('no_hak', 'No. Hak', ['class'=>'col-sm-3 control-label']) !!}
						                  <div class="col-sm-7">
						                  		{!! Form::text('no_hak', null, ['class'=>'form-control','id'=>'no_hak','placeholder'=>'Nomor Hak']) !!}
						                  </div>
						                  <div class="col-sm-offset-3 col-sm-7 no_hak error-input"></div>
						                </div>
									</div>
									<br>
					                <div class="row">
										<div class="form-group">
						                  {!! Form::label('album_id', 'Album', ['class'=>'col-sm-3 control-label']) !!}
						                  <div class="col-sm-7">
							                  	{!! Form::select('album_id', $data['album'], null, [
												  'class'=>'form-control js-selectize',
												  'id'=>'album',
												  'placeholder' => 'Pilih'
												  ]) 
												!!}
						                  </div>
						                  <div class="col-sm-offset-3 col-sm-7 album_id error-input"></div>
						                </div>
									</div>
					                <br>
					                <div class="row">
										<div class="form-group">
						                  {!! Form::label('hak_id', 'Jenis Hak', ['class'=>'col-sm-3 control-label']) !!}
						                  <div class="col-sm-7">
							                  	{!! Form::select('hak_id', $data['hak'], null, [
												  'class'=>'form-control js-selectize',
												  'id'=>'jenis_hak',
												  'placeholder' => 'Pilih Jenis Hak'
												  ]) 
												!!}
						                  </div>
						                  <div class="col-sm-offset-3 col-sm-7 hak_id error-input"></div>
						                </div>
									</div>
					                <br>
					                <div class="row">
										<div class="form-group">
						                  {!! Form::label('kecamatan_id', 'Kecamatan', ['class'=>'col-sm-3 control-label']) !!}
						                  <div class="col-sm-7">
							                  	{!! Form::select('kecamatan_id', $data['kecamatan'], null, [
												  'class'=>'form-control js-selectize',
												  'id'=>'kecamatan',
												  'placeholder' => 'Pilih Kecamatan'
												  ]) 
												!!}
						                  </div>
						                  <div class="col-sm-offset-3 col-sm-7 kecamatan_id error-input"></div>
						                </div>
									</div>
					                <br>
					                <div class="row">
										<div class="form-group">
						                  {!! Form::label('desa_id', 'Desa', ['class'=>'col-sm-3 control-label']) !!}
						                  <div class="col-sm-7" id="desa_wrap">
							                  	{!! Form::select('desa_id', $data['desa'], null, [
												  'class'=>'form-control js-selectize',
												  'id'=>'desa',
												  'placeholder' => 'Pilih Desa',
												  'disabled' => 'disabled'
												  ]) 
												!!}
						                  </div>
						                  <div class="col-sm-offset-3 col-sm-7 desa_id error-input"></div>
						                </div>
									</div>
					                <br>
					                <div class="row">
										<div class="form-group">
						                  {!! Form::label('luas', 'Luas', ['class'=>'col-sm-3 control-label']) !!}
						                  <div class="col-sm-7">
						                  		{!! Form::text('luas', null, ['class'=>'form-control','id'=>'luas','placeholder'=>'Luas']) !!}
						                  </div>
						                  <div class="col-sm-offset-3 col-sm-7 luas error-input"></div>
						                </div>
									</div>
									<br>
									<div class="row">
										<div class="form-group">
						                  {!! Form::label('pemegang_hak', 'Pemegang Hak', ['class'=>'col-sm-3 control-label']) !!}
						                  <div class="col-sm-7">
						                  		{!! Form::text('pemegang_hak', null, ['class'=>'form-control','id'=>'pemegang_hak','placeholder'=>'Pemegang Hak']) !!}
						                  </div>
						                  <div class="col-sm-offset-3 col-sm-7 pemegang_hak error-input"></div>
						                </div>
									</div>
									<br>

					                
								</div>
							</div>
							
						</div>
						<div class="box-footer">
		                <button type="button" onclick="hideModal('modal_buku_tanah','form-buku-tanah')" class="btn btn-default" data-dismiss="modal">Cancel</button>
		                {!! Form::submit('Submit', [
		                	'class'=>'btn btn-info pull-right btn_submit',
		                	'id'=>'btn_submit',
		                	'data-loading-text'=>'Loading..',
		                	'autocomplete'=>'off']) 
		                !!}

		              </div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>

		<!-- MODAL DELETE -->
		<div class="modal fade" id="modal_buku_tanah_delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        				<h4 class="modal-title" id="myModalLabel">Hapus Data Buku Tanah</h4>
					</div>
					<div class="modal-body">
						{!! csrf_field() !!}
						<input type="hidden" id="HiddenDeleteId" name="HiddenDeleteId">
						<p style="">Apakah anda yakin akan menghapus baris data?</p>
					</div>
					<div class="box-footer">
		                <button type="button" onclick="del()" class="btn btn-danger pull-right" data-dismiss="modal">Delete</button>
		                <button type="button" class="btn btn-default pull-right" data-dismiss="modal" style="margin-right:10px">Batal</button>
	              	</div>
				</div>
			</div>
		</div>

	@endsection


	@section('scripts')
		
		<script>
			var table;

		  	$(function () {

			    table = $('#example2').DataTable({
			    	processing: true,
		            serverSide: true,	
		            ajax: '{{ url('buku-tanah/bind') }}',
		            columns: [
			            {data: 'nb_barcode', name: 'nb_barcode'},
			            {data: 'no_hak', name: 'no_hak'},
			            {data: 'album.no_album', name: 'album.no_album'},
			            {data: 'jenis_hak.nama_hak', name: 'jenis_hak.nama_hak'},
			            {data: 'desa.nama_desa', name: 'desa.nama_desa'},
			            {data: 'desa.kecamatan.nama_kecamatan', name: 'desa.kecamatan.nama_kecamatan'},
			            {data: 'status', name: 'status'},
			            {data: 'action', name: 'action', orderable: false, searchable: false}
			        ],

			        // "order": [[ 4, "desc" ]],
			    });
		    });


		  	$(document).ready(function(){

		  		//Trigger Modal PopUp
		        $("#btn_add").click(function(){
		        	$("#btn_submit").val('Submit');
		        	combo_clear('#modal_buku_tanah'); 
		            $("#modal_buku_tanah").modal("show");
		            $('#form-buku-tanah').attr('action', '{{ route('buku-tanah.store') }}').attr('method','POST');
		        });

		        //Form Save (SUBMIT CLICKED)
		        $("#form-buku-tanah").on('submit', function(e){
		            e.preventDefault();
		            resetErrors();

		            var url = $(this).attr('action');
		            var method = $(this).attr('method');
		            // var dataForm = $(this).serialize();

		            // var dataForm = $(this).serializeArray();
		            var dataForm = new FormData($(this)[0]);

		            dataForm.append('_token', $('input[name=_token]').val());


		            var btn = $(".btn_submit");

	                $.ajax({
	                    url:url,
	                    type:method,
	                    data:dataForm,
	                    dataType: 'json',
	                    async:false,
	                    processData: false,
						contentType: false,
	                    beforeSend: function(){
	                        // btn.button('loading');
	                    },
	                    success: function(response){
							var dt = response;
							if(dt.errors){
								var errors = dt.errors;
								$.each(errors, function(key, value){
									$('label[for="'+ key +'"]').parent('div').addClass('has-error');
									$('.'+key+'.error-input').append('<span class="help-block">'+ value +'</span>');
								});
							}else{
								hideModal('modal_buku_tanah','form-buku-tanah');
								table.ajax.reload();
								toastr.success(dt.success);								
							}

							// btn.button('reset');
	                    },
	                    error: function(resp) {
	                    	//toastr.error('Problems when inserting to database.');
	                    	var errors = resp.responseJSON;
	                    	console.log(errors);
	                    	$.each(errors, function(key, value) {
					            $('label[for="'+ key +'"]').parent('div').addClass('has-error');
								$('.'+key+'.error-input').append('<span class="help-block">'+ value +'</span>');
					        });
						                    	
	                    	// btn.button('reset');
	                    }              
	                });
	                
		            
		        });


		        //Province on change
			    $("#kecamatan").change(function(){
			    	var kecamatan_id = $(this).val();

			    	getKecamatan(kecamatan_id);			    	
				});


		  	});


		  	function edit(id){
		        
		        $.ajax({
		            url : '{{ url('buku-tanah/edit') }}/' + id,
		            type: "GET",
		            dataType: "JSON",
		            success: function(data){
		            	$("#btn_submit").val('Update');                     
		                $('#modal_buku_tanah').modal('show'); 

		                $('#form-buku-tanah').attr('action', data.url_action).attr('method','POST');
		                $('#nb_barcode').val(data.buku_tanah.nb_barcode);
		                $('#no_hak').val(data.buku_tanah.no_hak);
		                $('#luas').val(data.buku_tanah.luas);
		                $('#pemegang_hak').val(data.buku_tanah.pemegang_hak);
		                // $('#status').val(data.buku_tanah.status);
		                selectize_("#jenis_hak", data.buku_tanah.hak_id);
		                selectize_("#album", data.buku_tanah.album_id);
		                selectize_("#kecamatan", data.buku_tanah.desa.id_kecamatan);
		                selectize_("#desa", data.buku_tanah.desa_id);

		            },
		            error: function (jqXHR, textStatus, errorThrown){
		                alert('Error get data from ajax');
		            }
		        });
		    }

		    function getKecamatan(kecamatan_id, selected=false, desaVal='') {
		    	$.get("{{ url('buku-tanah/desalist') }}/" + kecamatan_id, function(data) {

			        if(data != ""){
			          	$("#desa_wrap").html(data);
			          	$("#desa").removeAttr("disabled", "disabled");
			        }
			    });

			    if(selected && desaVal!='') selectize_("#desa", desaVal);
		    }

		    function delConfirm(id){
		        var id = id;
		        $("#modal_buku_tanah_delete").modal("show");
		        $("#HiddenDeleteId").val(id);
		    }

		    function del(){
		        var id = $("#HiddenDeleteId").val(); 
		        
		        $.ajax({
		            url : '{{ url('buku-tanah/destroy') }}',
		            type: "POST",
		            //dataType: "JSON",
		            data: {
				      '_token': $('input[name=_token]').val(),
				      'id': id
				    },
		            success: function(data){
		               $('#modal_buku_tanah_delete').modal('hide');
		               table.ajax.reload();
		               toastr.success(data.success);
		            },
		            error: function (jqXHR, textStatus, errorThrown){
		                alert('Error deleting data.');
		            }
		        }); 
		    }


		  	function resetErrors() {
			    // $('form input, form select').removeClass('inputTxtError');
			    $('span.help-block').remove();
			    $('.form-group').removeClass('has-error');
			}


		</script>

	@endsection