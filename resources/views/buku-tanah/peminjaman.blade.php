@extends('layouts.app')

	@section('content')
	
		{{ csrf_field() }}
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Peminjaman Buku Tanah</h3>
					<div class="col-md-2 box-tools"></div>
				</div>
				<div class="box-body">
					{!! Form::open(['route' => 'peminjaman-bt.store', 'id'=>'form-peminjaman']) !!}
					<div class="row">
					
						<div class="col-md-5">
							<div class="box-body">
								<div class="form-group">
									<label for="nomor_pinjam_form1" class="col-sm-4 control-label">Nomor Pinjam</label>
									<div class="col-sm-8">
										<input type="text" class="form-control input-sm" id="nomor_pinjam_form1" name="nomor_pinjam_form1" value="{!! $data['nomor_pinjam'] !!}" readonly="readonly">
									</div>
				                </div>
				                <br>
				                <div class="form-group">
									<label for="nip_form1" class="col-sm-4 control-label">NIP</label>
									<div class="col-sm-8">
										<input type="text" class="form-control input-sm" id="nip_form1" name="nip_form1">
									</div>
				                </div>
				                <br>
				                <div class="form-group">
									<label for="tanggal_pinjam_form1" class="col-sm-4 control-label">Tanggal Pinjam</label>
									<div class="col-sm-5">
										<input type="text" class="form-control input-sm" id="tanggal_pinjam_form1" name="tanggal_pinjam_form1" value="{!! $data['tanggal_pinjam'] !!}" readonly="readonly">
									</div>
				                </div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="box-body">
				                <div class="form-group">
									<label for="note_form1" class="col-sm-2 control-label">Note</label>
									<div class="col-sm-10">
										<textarea class="form-control" id="note_form1" rows="2" name="note_form1" placeholder="Catatan..."></textarea>
									</div>
				                </div>
				                <br>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-offset-3 col-md-5">
							<div class="box-body">
								<div class="form-group">
					                <div class="input-group">
					                  <div class="input-group-addon">
					                    <i class="fa fa-search"></i>
					                  </div>
					                  <input type="hidden" name="hdn_buku_tanah_id_form1" id="hdn_buku_tanah_id_form1">
					                  <input type="text" class="form-control" id="search_buku_tanah" name="search_buku_tanah" placeholder="Pencarian Buku Tanah...">
					                  <span class="input-group-btn">
								        <button class="btn btn-primary" id="btn_pinjam_form1" type="button">Pinjam</button>
								      </span>
					                </div>
					                <!-- /.input group -->
					              </div>
							</div>						
						</div>
					</div>
					{!! Form::close() !!}

					<table id="example2" class="table table-bordered table-hover">
		                <thead>
			                <tr>
								<th>NB Barcode</th>
								<th>Nomor Hak</th>
								<th>Jenis Hak</th>
								<th>Desa</th>
								<th>Kecamatan</th>
								<th>Jumlah</th>
								<th></th>
			                </tr>
		                </thead>
		            </table>
	            </div>
	            <!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>

		<!-- Tabel List Peminjaman -->
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Histori Semua Peminjaman Buku Tanah</h3>
				</div>
				<div class="box-body">
					<table id="example3" class="table table-bordered table-hover">
		                <thead>
			                <tr>
								<th>Nomor Pinjam</th>
								<th>NIP</th>
								<th>Nama Pemohon</th>
								<th>Tanggal Pinjam</th>
								<th>Jumlah Buku</th>
								<th>Note</th>
								{{-- <th></th> --}}
			                </tr>
		                </thead>
		            </table>
	            </div>
	            <!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- end - Tabel List Peminjaman -->

		<!-- MODAL DELETE -->
		<div class="modal fade" id="modal_detail_peminjaman_delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        				<h4 class="modal-title" id="myModalLabel">Hapus Data Peminjaman</h4>
					</div>
					<div class="modal-body">
						{!! csrf_field() !!}
						<input type="hidden" id="hdn_nomor_pinjam" name="hdn_nomor_pinjam">
						<input type="hidden" id="hdn_buku_tanah" name="hdn_buku_tanah">
						<p style="">Apakah anda yakin akan menghapus data Peminjaman?</p>
					</div>
					<div class="box-footer">
		                <button type="button" onclick="del()" class="btn btn-danger pull-right" data-dismiss="modal">Delete</button>
		                <button type="button" class="btn btn-default pull-right" data-dismiss="modal" style="margin-right:10px">Batal</button>
	              	</div>
				</div>
			</div>
		</div>

	@endsection


	@section('scripts')
		
		<script>

			var table, table2;
			var nomor_pinjam = $("#nomor_pinjam_form1").val();

		  	$(function () {

			    table = $('#example2').DataTable({
			    	processing: true,
		            serverSide: true,
		            ajax: '{{ url('transaksi/peminjaman/buku-tanah') }}/'+nomor_pinjam,
		            columns: [
			            {data: 'buku_tanah.nb_barcode', name: 'buku_tanah.nb_barcode'},
			            {data: 'buku_tanah.no_hak', name: 'buku_tanah.no_hak'},
			            {data: 'buku_tanah.jenis_hak.kode_hak', name: 'buku_tanah.jenis_hak.kode_hak'},
			            {data: 'buku_tanah.desa.nama_desa', name: 'buku_tanah.desa.nama_desa'},
			            {data: 'buku_tanah.desa.kecamatan.nama_kecamatan', name: 'buku_tanah.desa.kecamatan.nama_kecamatan'},
			            {data: 'jumlah_buku', name: 'jumlah_buku'},
			            {data: 'action', name: 'action', orderable: false, searchable: false}
			        ],
			        dom: '<"top"i>rt<"bottom"flp><"clear">',

			    });

			    table2 = $('#example3').DataTable({
			    	processing: true,
		            serverSide: true,
		            ajax: '{{ url('transaksi/peminjaman/all_peminjaman') }}',
		            columns: [
			            {data: 'nomor_pinjam', name: 'nomor_pinjam'},
			            {data: 'pemohon.nip', name: 'pemohon.nip'},
			            {data: 'pemohon.nama_lengkap', name: 'pemohon.nama_lengkap'},
			            {data: 'tanggal_pinjam', name: 'tanggal_pinjam'},
			            {data: 'total_pinjam', name: 'total_pinjam'},
			            {data: 'note', name: 'note'}
			            // {data: 'action', name: 'action', orderable: false, searchable: false}
			        ],
			        order: [ [3, 'desc'] ]

			    });

		    });


		  	$(document).ready(function(){

		  		// Form Tambah Buku untuk dipinjam
		  		$("#btn_pinjam_form1").on("click", function(){

		  			var nomor_pinjam = $("#nomor_pinjam_form1").val();
		  			var tanggal_pinjam = $("#tanggal_pinjam_form1").val();
		  			var nip = $("#nip_form1").val();
		  			var note = $("#note_form1").val();

		  			var buku_tanah_id = $("#hdn_buku_tanah_id_form1").val();
		  			var jumlah_buku = 1;

		  			if(nip==""){
		  				toastr.error("NIP harus diisi.");
		  				return;	
		  			}
		  			if(buku_tanah_id==""){
		  				toastr.error("Tidak ada hasil pencarian Buku Tanah.");
		  				return;	
		  			}

		  			var url = $("#form-peminjaman").attr('action');
		            var method = $("#form-peminjaman").attr('method');
		            var btn = $("#btn_pinjam_form1");

		            var dataForm = new FormData();
		            dataForm.append('_token', $('input[name=_token]').val());
		            dataForm.append('nomor_pinjam', nomor_pinjam);
		            dataForm.append('tanggal_pinjam', tanggal_pinjam);
		            dataForm.append('nip', nip);
		            dataForm.append('note', note);
		            dataForm.append('buku_tanah_id', buku_tanah_id);
		            dataForm.append('jumlah_buku', jumlah_buku);


		            $.ajax({
	                    url:url,
	                    type:method,
	                    data:dataForm,
	                    dataType: 'json',
	                    async:false,
	                    processData: false,
						contentType: false,
	                    beforeSend: function(){
	                        // btn.button('loading');
	                    },
	                    success: function(response){
							var dt = response;
							if(dt.errors){
								var errors = dt.errors;
								console.log(errors);
								toastr.error(errors);
								/*$.each(errors, function(key, value){
									$('label[for="'+ key +'"]').parent('div').addClass('has-error');
									$('.'+key+'.error-input').append('<span class="help-block">'+ value +'</span>');
								});*/
							}else{
								// hideModal('modal_kecamatan','form-kecamatan');
								
								toastr.success(dt.success);								
							}

							table.ajax.reload();
							table2.ajax.reload();
							// btn.button('reset');
	                    },
	                    error: function(resp) {
	                    	//toastr.error('Problems when inserting to database.');
	                    	var errors = resp.responseJSON;
	                    	console.log(errors);
	                    	/*$.each(errors, function(key, value) {
					            $('label[for="'+ key +'"]').parent('div').addClass('has-error');
								$('.'+key+'.error-input').append('<span class="help-block">'+ value +'</span>');
					        });*/
						                    	
	                    	// btn.button('reset');
	                    }              
	                });

		  		});


		        
		        // Pencarian Buku Tanah
				$('input#search_buku_tanah').typeahead({
					highlight: true,
				},
				{
					name: 'search_buku_tanah',
					display: 'value',
					source: function(term, syncResults, asyncResults) {
						$.get("{{ url('transaksi/peminjaman/buku-tanah/search') }}/" + term, function(data) {
							asyncResults(data);
						});
					},
					templates: {
				      notFound: [
				        "<div class=empty-message>",
				        "Tidak ada data Buku Tanah yang cocok dengan kata kunci anda.",
				        "</div>"
				      ].join("\n"),
				      suggestion: function(data) {
				        // `data` : `suggest` property of object passed at `Bloodhound`
				        return "<div>" + data.value + "</div>"
				      }
				    }
				});

				// Assign Hasil Pencarian Buku Tanah
				$('input#search_buku_tanah').on('typeahead:selected', function(evt, item) {
				    $("#hdn_buku_tanah_id_form1").val(item.id);
				});


		  	});


		  	
		    function delConfirm(id, buku_tanah_id){
		        var id = id;
		        var buku_tanah_id = buku_tanah_id;
		        $("#modal_detail_peminjaman_delete").modal("show");
		        $("#hdn_nomor_pinjam").val(id);
		        $("#hdn_buku_tanah").val(buku_tanah_id);
		    }

		    function del(){
		        var id = $("#hdn_nomor_pinjam").val(); 
		        var buku_tanah_id = $("#hdn_buku_tanah").val(); 
		        
		        $.ajax({
		            url : '{{ url('transaksi/peminjaman/buku-tanah/delete') }}',
		            type: "POST",
		            //dataType: "JSON",
		            data: {
				      '_token': $('input[name=_token]').val(),
				      'id': id,
				      'buku_tanah_id': buku_tanah_id
				    },
		            success: function(data){
		               $('#modal_detail_peminjaman_delete').modal('hide');
		               table.ajax.reload();
		               toastr.success(data.success);
		            },
		            error: function (jqXHR, textStatus, errorThrown){
		                toastr.error('Error deleting data.');
		            }
		        }); 
		    }


		  	function resetErrors() {
			    // $('form input, form select').removeClass('inputTxtError');
			    $('span.help-block').remove();
			    $('.form-group').removeClass('has-error');
			}


		</script>

	@endsection