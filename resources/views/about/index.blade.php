@extends('layouts.app')

	@section('content')
		
		<div class="col-md-6 col-md-offset-3">
			<div class="box box-info">
	            <div class="box-header with-border">
	              <h3 class="box-title">About</h3>
	            </div>

	            <!-- form start -->
				{!! Form::model($profile, ['url' => route('about.update', $profile->id), 'method'=>'put', 'files'=>'true', 'class'=>'form-horizontal']) !!}
				    <div class="box-body">
				    	@include('about._form')
				    </div>
				{!! Form::close() !!}

	            
	          </div>
		</div>

	@endsection