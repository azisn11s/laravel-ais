<div class="box-body">
    <div class="form-group {{ $errors->has('app_name') ? ' has-error' : '' }}">
    	{!! Form::label('app_name', 'Application Name', ['class'=>'col-sm-3 control-label']) !!}
    	{{ csrf_field() }}
		<div class="col-sm-8">
			{!! Form::text('app_name', null, ['class'=>'form-control']) !!}
			{!! $errors->first('app_name', '<p class="help-block">:message</p>') !!}
		</div>
    </div>
    <div class="form-group {{ $errors->has('jargon') ? ' has-error' : '' }}">
    	{!! Form::label('jargon', 'Jargon', ['class'=>'col-sm-3 control-label']) !!}
		<div class="col-sm-8">
			{!! Form::text('jargon', null, ['class'=>'form-control']) !!}
			{!! $errors->first('jargon', '<p class="help-block">:message</p>') !!}
		</div>
    </div>
    <div class="form-group {{ $errors->has('company_name') ? ' has-error' : '' }}">
    	{!! Form::label('company_name', 'Company Name', ['class'=>'col-sm-3 control-label']) !!}
		<div class="col-sm-8">
			{!! Form::text('company_name', null, ['class'=>'form-control']) !!}
			{!! $errors->first('company_name', '<p class="help-block">:message</p>') !!}
		</div>
    </div>
    <div class="form-group {{ $errors->has('year') ? ' has-error' : '' }}">
    	{!! Form::label('year', 'Year', ['class'=>'col-sm-3 control-label']) !!}
		<div class="col-sm-3">
			{!! Form::text('year', null, ['class'=>'form-control']) !!}
			{!! $errors->first('year', '<p class="help-block">:message</p>') !!}
		</div>
    </div>
    <div class="form-group {{ $errors->has('app_version') ? ' has-error' : '' }}">
    	{!! Form::label('app_version', 'Version', ['class'=>'col-sm-3 control-label']) !!}
		<div class="col-sm-3">
			{!! Form::text('app_version', null, ['class'=>'form-control']) !!}
			{!! $errors->first('app_version', '<p class="help-block">:message</p>') !!}
		</div>
    </div>
</div>

<div class="box-footer">
	<a class="btn btn-default" href="#">Edit Mode</a>
	{!! Form::submit('Save', ['class'=>'btn btn-info pull-right']) !!}
</div>
<!-- /.box-footer -->