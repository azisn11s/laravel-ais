@extends('layouts.app')

	@section('content')
		
		<div class="col-md-6 col-md-offset-3">
			<div class="box box-info">
	            <div class="box-header with-border">
	              <h3 class="box-title">Add New Menu</h3>
	            </div>

	            <!-- form start -->
				{!! Form::open(['url' => route('menus.store'), 'class'=>'form-horizontal']) !!}
				{{ csrf_field() }}
				    <div class="box-body">
				    	@include('menus._form', $data)
				    </div>
				{!! Form::close() !!}

	            
	          </div>
		</div>

	@endsection