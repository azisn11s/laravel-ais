
  <div class="box-body">
    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
    	{!! Form::label('name', 'Menu Name', ['class'=>'col-sm-3 control-label']) !!}
		<div class="col-sm-8">
			{!! Form::text('name', null, ['class'=>'form-control']) !!}
			{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
		</div>
    </div>
	<div class="form-group {{ $errors->has('url') ? ' has-error' : '' }}">
		{!! Form::label('url', 'URL', ['class'=>'col-sm-3 control-label']) !!}
		<div class="col-sm-8">
			{!! Form::text('url', null, ['class'=>'form-control']) !!}
			{!! $errors->first('url', '<p class="help-block">:message</p>') !!}
		</div>
	</div>
	<div class="form-group">
		{!! Form::label('uri', 'URI', ['class'=>'col-sm-3 control-label']) !!}
		<div class="col-sm-8">
			{!! Form::text('uri', null, ['class'=>'form-control']) !!}
		</div>
	</div>
	<div class="form-group">
		{!! Form::label('icon', 'Icon', ['class'=>'col-sm-3 control-label']) !!}
		<div class="col-sm-8">
			{!! Form::text('icon', 'fa fa-circle-o', ['class'=>'form-control']) !!}
		</div>
	</div>
	<div class="form-group">
		{!! Form::label('is_root', 'Is Root', ['class'=>'col-sm-3 control-label']) !!}
		<div class="col-sm-8">
			{!! Form::checkbox('is_root', true, null, ['class'=>'checkbox']) !!}
		</div>
	</div>
	<div class="form-group">
		{!! Form::label('parent_id', 'Parent', ['class'=>'col-sm-3 control-label']) !!}
		<div class="col-sm-8">
			{!! Form::select('parent_id', $data["parents_menu"], null, [
			  'class'=>'js-selectize',
			  'placeholder' => 'Select Parent']) 
			!!}
		</div>
	</div>
	<div class="form-group">
		{!! Form::label('sort', 'Sort', ['class'=>'col-sm-3 control-label']) !!}
		<div class="col-sm-3">
			{!! Form::number('sort', null, ['class'=>'form-control', 'min'=>1]) !!}
		</div>
	</div>
	<div class="form-group">
		{!! Form::label('is_active', 'Is Active', ['class'=>'col-sm-3 control-label']) !!}
		<div class="col-sm-8">
			{!! Form::checkbox('is_active', true, null, ['class'=>'checkbox']) !!}
		</div>
	</div>
  </div>
  <!-- /.box-body -->
  <div class="box-footer">
  	<a class="btn btn-default" href="{{ route('menus.index') }}">Cancel</a>
  	{!! Form::submit('Save', ['class'=>'btn btn-info pull-right']) !!}
  </div>
  <!-- /.box-footer -->