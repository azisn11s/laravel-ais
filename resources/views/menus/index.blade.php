@extends('layouts.app')



	@section('content')
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">All Menu</h3>
					<div class="col-md-2 box-tools">
						<a class="btn btn-primary btn-block btn-sm" href="{{ route('menus.create') }}">Add Menu</a>
					</div>
				</div>
				<div class="box-body">
					{{-- {!! $html->table(['class'=>'table table-bordered table-striped']) !!} --}}

					<table id="menu-list" class="table table-bordered table-hover">
		                <thead>
		                <tr>
		                
		                  <th>Nama Menu</th>
		                  <th>URL</th>
		                  <th>URI</th>
		                  <th>Icon</th>
		                  <th>Sort</th>
		                  <th>Menu ID</th>
		                  <th>Parent</th>
		                  <th>Is Root</th>
		                  <th>Active</th>
		                </tr>
		                </thead>
		                
		              </table>

				</div>
			</div>
		</div>
	@endsection

	

	@section('scripts')
		<script type="text/javascript">
			var table;

		  	$(function () {

			    table = $('#menu-list').DataTable({
			    	processing: true,
		            serverSide: true,
		            ajax: '{{ url('administrator/menus/bind') }}',
		            columns: [
			            // {data: 'DT_Row_Index', name: 'number'},
			            {data: 'name', name: 'name'},
			            {data: 'url', name: 'url'},
			            {data: 'uri', name: 'uri'},
			            {data: 'icon', name: 'icon'},
			            {data: 'sort', name: 'sort'},
			            {data: 'id', name: 'id'},
			            {data: 'parent_id', name: 'parent_id'},
			            {data: 'is_root', name: 'is_root'},
			            {data: 'is_active', name: 'is_active'}
			        ]
			    });
		    });
		</script>
	@endsection