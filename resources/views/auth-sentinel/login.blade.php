@extends('layouts.auth-base')

@section('content')

  <div class="login-box">
    <div class="login-logo">
      <img src="{{asset('images/logo/logobpn.png')}}" >
      {{-- <a href="#">{-- $app_profile->app_name --}</a> --}}
    </div>

    {{-- Flash Notification --}}
    @if(session()->has('flash_notification.message'))
      <div class="alert alert-{{ session()->get('flash_notification.level') }} alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning"></i> Alert!</h4>
          {!! session()->get('flash_notification.message') !!}
      </div>
    @endif

    <div class="login-box-body">
      <p class="app-login-title">{!! $app_profile->jargon !!}</p>

      <form action="{{ url('/login') }}" method="post" role="form">
        {!! csrf_field() !!}
        <div class="form-group has-feedback{{ $errors->has('login') ? ' has-error' : '' }}">
          <input class="form-control" name="login" placeholder="Username atau Email" value="{{ old('login') }}">
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          @if ($errors->has('username'))
            <span class="help-block">
                <strong>{{ $errors->first('login') }}</strong>
            </span>
          @endif
        </div>
        <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
          <input id="password" name="password" class="form-control" placeholder="Password" type="password">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
          @endif
        </div>
        <div class="row">
            <div class="col-xs-8">
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="remember_me"> Ingat saya
                </label>
              </div>
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div>
            <!-- /.col -->
          </div>

      </form>

      <a href="{{ url('/password/reset') }}">Lupa password</a><br>

    </div>

  </div>

@endsection
