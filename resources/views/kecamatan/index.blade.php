@extends('layouts.app')

	@section('content')
	
		{{ csrf_field() }}
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Kecamatan</h3>
					<div class="col-md-2 box-tools">
						<a id="btn_add" class="btn btn-primary btn-block btn-sm" href="#">Tambah Data</a>
					</div>
				</div>
				<div class="box-body">
					{{-- $html->table(['class'=>'table table-bordered table-striped']) --}}
					<table id="example2" class="table table-bordered table-hover">
		                <thead>
			                <tr>
								<th>Kode</th>
								<th>Nama Kecamatan</th>
								<th></th>
			                </tr>
		                </thead>
		            </table>
	            </div>
	            <!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>


		<div class="modal fade" id="modal_kecamatan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        				<h4 class="modal-title" id="myModalLabel">Form Data Kecamatan</h4>
					</div>
					{!! Form::open(['route' => 'kecamatan.store', 'id'=>'form-kecamatan']) !!}
						<div class="modal-body">
							<div class="box box-info">
								<div class="box-body">
									<div class="row" id="alert-report"></div>
									<div class="row">
										<div class="form-group">
						                  {!! Form::label('kode_kecamatan', 'Kode', ['class'=>'col-sm-3 control-label']) !!}
						                  <div class="col-sm-4">
						                  		{!! Form::text('kode_kecamatan', null, ['class'=>'form-control','id'=>'kode_kecamatan','placeholder'=>'Kode Kecamatan']) !!}
						                  </div>
						                  <div class="col-sm-offset-3 col-sm-7 kode_kecamatan error-input"></div>
						                </div>
									</div>
					                <br>
					                <div class="row">
										<div class="form-group">
						                  {!! Form::label('nama_kecamatan', 'Nama Kecamatan', ['class'=>'col-sm-3 control-label']) !!}
						                  <div class="col-sm-7">
						                  		{!! Form::text('nama_kecamatan', null, ['class'=>'form-control','id'=>'nama_kecamatan','placeholder'=>'Nama Kecamatan']) !!}
						                  </div>
						                  <div class="col-sm-offset-3 col-sm-7 nama_kecamatan error-input"></div>
						                </div>
									</div>
					                <br>
					                
								</div>
							</div>
							
						</div>
						<div class="box-footer">
		                <button type="button" onclick="hideModal('modal_kecamatan','form-kecamatan')" class="btn btn-default" data-dismiss="modal">Cancel</button>
		                {!! Form::submit('Submit', [
		                	'class'=>'btn btn-info pull-right btn_submit',
		                	'id'=>'btn_submit',
		                	'data-loading-text'=>'Loading..',
		                	'autocomplete'=>'off']) 
		                !!}

		              </div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>

		<!-- MODAL DELETE -->
		<div class="modal fade" id="modal_kecamatan_delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        				<h4 class="modal-title" id="myModalLabel">Hapus Data Kecamatan</h4>
					</div>
					<div class="modal-body">
						{!! csrf_field() !!}
						<input type="hidden" id="HiddenDeleteId" name="HiddenDeleteId">
						<p style="">Apakah anda yakin akan menghapus baris data?</p>
					</div>
					<div class="box-footer">
		                <button type="button" onclick="del()" class="btn btn-danger pull-right" data-dismiss="modal">Delete</button>
		                <button type="button" class="btn btn-default pull-right" data-dismiss="modal" style="margin-right:10px">Batal</button>
	              	</div>
				</div>
			</div>
		</div>

	@endsection


	@section('scripts')
		
		<script>
			var table;

		  	$(function () {

			    table = $('#example2').DataTable({
			    	processing: true,
		            serverSide: true,
		            ajax: '{{ url('utility/lokasi/kecamatan/bind') }}',
		            columns: [
			            {data: 'kode_kecamatan', name: 'kode_kecamatan'},
			            {data: 'nama_kecamatan', name: 'nama_kecamatan'},
			            {data: 'action', name: 'action', orderable: false, searchable: false}
			        ],

			        // "order": [[ 4, "desc" ]],
			    });
		    });


		  	$(document).ready(function(){

		  		//Trigger Modal PopUp
		        $("#btn_add").click(function(){
		        	$("#btn_submit").val('Submit'); 
		            $("#modal_kecamatan").modal("show");
		            $('#form-kecamatan').attr('action', '{{ route('kecamatan.store') }}').attr('method','POST');
		        });

		        //Form Save (SUBMIT CLICKED)
		        $("#form-kecamatan").on('submit', function(e){
		            e.preventDefault();
		            resetErrors();

		            var url = $(this).attr('action');
		            var method = $(this).attr('method');
		            // var dataForm = $(this).serialize();

		            // var dataForm = $(this).serializeArray();
		            var dataForm = new FormData($(this)[0]);

		            dataForm.append('_token', $('input[name=_token]').val());


		            var btn = $(".btn_submit");

	                $.ajax({
	                    url:url,
	                    type:method,
	                    data:dataForm,
	                    dataType: 'json',
	                    async:false,
	                    processData: false,
						contentType: false,
	                    beforeSend: function(){
	                        // btn.button('loading');
	                    },
	                    success: function(response){
							var dt = response;
							if(dt.errors){
								var errors = dt.errors;
								$.each(errors, function(key, value){
									$('label[for="'+ key +'"]').parent('div').addClass('has-error');
									$('.'+key+'.error-input').append('<span class="help-block">'+ value +'</span>');
								});
							}else{
								hideModal('modal_kecamatan','form-kecamatan');
								table.ajax.reload();
								toastr.success(dt.success);								
							}

							// btn.button('reset');
	                    },
	                    error: function(resp) {
	                    	//toastr.error('Problems when inserting to database.');
	                    	var errors = resp.responseJSON;
	                    	console.log(errors);
	                    	$.each(errors, function(key, value) {
					            $('label[for="'+ key +'"]').parent('div').addClass('has-error');
								$('.'+key+'.error-input').append('<span class="help-block">'+ value +'</span>');
					        });
						                    	
	                    	// btn.button('reset');
	                    }              
	                });
	                
		            
		        });

		  	});


		  	function edit(id){
		        
		        $.ajax({
		            url : '{{ url('utility/lokasi/kecamatan/edit') }}/' + id,
		            type: "GET",
		            dataType: "JSON",
		            success: function(data){
		            	$("#btn_submit").val('Update');                     
		                $('#modal_kecamatan').modal('show'); 

		                $('#form-kecamatan').attr('action',data.url_action).attr('method','POST');
		                $('#kode_kecamatan').val(data.kecamatan.kode_kecamatan);
		                $('#nama_kecamatan').val(data.kecamatan.nama_kecamatan);

		            },
		            error: function (jqXHR, textStatus, errorThrown){
		                alert('Error get data from ajax');
		            }
		        });
		    }

		    function delConfirm(id){
		        var id = id;
		        $("#modal_kecamatan_delete").modal("show");
		        $("#HiddenDeleteId").val(id);
		    }

		    function del(){
		        var id = $("#HiddenDeleteId").val(); 
		        
		        $.ajax({
		            url : '{{ url('utility/lokasi/kecamatan/destroy') }}',
		            type: "POST",
		            //dataType: "JSON",
		            data: {
				      '_token': $('input[name=_token]').val(),
				      'id': id
				    },
		            success: function(data){
		               $('#modal_kecamatan_delete').modal('hide');
		               table.ajax.reload();
		               toastr.success(data.success);
		            },
		            error: function (jqXHR, textStatus, errorThrown){
		                alert('Error deleting data.');
		            }
		        }); 
		    }


		  	function resetErrors() {
			    // $('form input, form select').removeClass('inputTxtError');
			    $('span.help-block').remove();
			    $('.form-group').removeClass('has-error');
			}


		</script>

	@endsection