{{-- {{ dd($data) }} --}}

@extends('layouts.app')

	@section('content')

		<section class="content">
			<div class="row">
				<div class="col-md-3">
					<!-- Profile Image -->
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title">Profile Data Saat Ini</h3>
						</div>

						<div class="box-body box-profile">
							<img class="profile-user-img img-responsive img-circle" src="{{ (!is_null(Sentinel::getUser()->avatar)) ?  URL('images/avatar/user')."/".Sentinel::getUser()->avatar : URL('images/avatar/admin/user-default.png') }}" alt="User profile picture">

							<h3 class="profile-username text-center">{!! $data['full_name'] !!}</h3>

							<p class="text-muted text-center">{!! $data['user_type'] !!}</p>

							<div class="box-body">
								<strong><i class="fa  fa-user"></i> Username</strong>

								<p class="text-muted">
									{!! $data['username'] !!}
								</p>

								<hr>

								<strong><i class="fa fa-paper-plane-o"></i> Email</strong>

								<p class="text-muted">
									{!! $data['email'] !!}
								</p>

								<hr>
								@if(Sentinel::getUser()->type != 'admin')
								<strong><i class="fa fa-phone margin-r-5"></i> Telepon / HP</strong>

								<p class="text-muted">
									{!! $data['phone'] !!}
								</p>

								<hr>

								

								<strong><i class="fa fa-map-marker margin-r-5"></i> Alamat</strong>

								<p class="text-muted">{!! $data['address'] !!}</p>
								@endif

							</div>
							
						</div>
					<!-- /.box-body -->
					</div>
				</div>

				<div class="col-md-9">
					<!-- Profile Update Form -->
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title">Form Update Profile</h3>
						</div>
						<div class="box-body">
								
							{!! Form::open(['route' => 'user.update', 'id'=>'form-project-owner', 'class'=>'form-horizontal', 'files' => true]) !!}
							<div class="row">
								<div class="col-md-8">
									@if(Session::has('error_message')) 
										{!! '<div class="alert alert-warning">'. Session::get("wrong_old_password").'</div>' !!}
									@endif
									<div class="box-body">
										<div class="form-group">
											{!! Form::label('username', 'Username', ['class'=>'col-sm-2 control-label']) !!}
											<div class="col-sm-8">
												{!! Form::text('username', $data['username'], ['class'=>'form-control','id'=>'username','placeholder'=>'Username']) !!}
											</div>
											<div class="col-sm-offset-2 col-sm-7 username error-input"></div>
										</div>
										@if(Sentinel::getUser()->type != 'admin')
										<div class="form-group">
											{!! Form::label('full_name', 'Nama', ['class'=>'col-sm-2 control-label']) !!}
											<div class="col-sm-10">
												{!! Form::text('full_name', $data['full_name'], ['class'=>'form-control','id'=>'username','placeholder'=>'Nama Lengkap']) !!}
											</div>
											<div class="col-sm-offset-2 col-sm-7 full_name error-input"></div>
										</div>
										<div class="form-group">
											{!! Form::label('address', 'Alamat', ['class'=>'col-sm-2 control-label']) !!}
											<div class="col-sm-10">
												{!! Form::textarea('address', $data['address'], ['class'=>'form-control','id'=>'address','placeholder'=>'Alamat lengkap...','rows'=>'3']) !!}
											</div>
											<div class="col-sm-offset-2 col-sm-7 address error-input"></div>
										</div>
										<div class="form-group">
											{!! Form::label('province_id', 'Provinsi', ['class'=>'col-sm-2 control-label']) !!}
											<div class="col-sm-10">
												{!! Form::select('province_id', $data["provinces"], $data['province_id'], [
												  'class'=>'form-control js-selectize',
												  'id'=>'province',
												  'placeholder' => 'Select Province'
												  ]) 
												!!}
											</div>
											<div class="col-sm-offset-2 col-sm-7 province_id error-input"></div>
										</div>
										<div class="form-group">
											{!! Form::label('city_id', 'Kota / Kabupaten', ['class'=>'col-sm-2 control-label']) !!}
											<div class="col-sm-10"  id="city_wrap">
												{!! Form::select('user_city_id', $data["cities"], $data['city_id'], [
												  'class'=>'form-control js-selectize',
												  'id'=>'city',
												  'placeholder' => 'Select City'
												  ]) 
												!!}
											</div>
											<div class="col-sm-offset-2 col-sm-7 city_id error-input"></div>
										</div>
										<div class="form-group">
											{!! Form::label('phone', 'Telp./HP', ['class'=>'col-sm-2 control-label']) !!}
											<div class="col-sm-5">
												<div class="input-group">
							                  		<div class="input-group-addon">
														<i class="fa fa-phone"></i>
													</div>
													{!! Form::text('phone', null, ['class'=>'form-control','id'=>'phone', 'data-inputmask'=>'"mask": "99 9999-9999-9999"', 'data-mask']) !!}
							                  </div>
											</div>
											<div class="col-sm-offset-2 col-sm-7 phone error-input"></div>
										</div>
										@endif
										<div class="form-group">
											{!! Form::label('email', 'Email', ['class'=>'col-sm-2 control-label']) !!}
											<div class="col-sm-7">
												{!! Form::email('email', $data['email'], ['class'=>'form-control','id'=>'email', 'placeholder'=>'Email']) !!}
											</div>
											<div class="col-sm-offset-3 col-sm-7 email error-input"></div>
										</div>

										<div class="form-group">
										  <div class="col-sm-offset-2 col-sm-9">
										    <a href="#" id="btnChangePassword" class="btn btn-default btn-xs">Perbaharui Password</a>
										  </div>
										</div>

										<div id="change_password" style="display: none;">
											<div class="form-group">
												{!! Form::label('old_password', 'Password Lama', ['class'=>'col-sm-2 control-label']) !!}
												<div class="col-sm-7">
													{!! Form::password('old_password', ['class'=>'form-control','id'=>'old_password', 'disabled'=>'disabled', 'placeholder'=>'Password saat ini']) !!}
												</div>
												<div class="col-sm-offset-3 col-sm-7 old_password error-input"></div>
											</div>
											<div class="form-group">
												{!! Form::label('new_password', 'Password Baru', ['class'=>'col-sm-2 control-label']) !!}
												<div class="col-sm-7">
													{!! Form::password('new_password', ['class'=>'form-control','id'=>'new_password', 'disabled'=>'disabled', 'placeholder'=>'Password baru']) !!}
												</div>
												<div class="col-sm-offset-3 col-sm-7 new_password error-input"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
							                  {!! Form::label('avatar', 'Foto Profil', ['class'=>'']) !!}
							                  		<img src="{{ (!is_null(Sentinel::getUser()->avatar)) ?  URL('images/avatar/user')."/".Sentinel::getUser()->avatar : URL('images/avatar/admin/user-default.png') }}" class="col-sm-12 img-responsive preview-img"></span>
							                  		{!! Form::file('avatar', ['id'=>'avatar', 'class'=>'image-input col-sm-12']) !!}
							                  
							                  <div class="col-sm-offset-3 col-sm-7 avatar error-input"></div>
							                </div>
										</div>
									</div>
								</div>
							</div>
				              
				              <!-- /.box-body -->
				              <div class="box-footer">
				                <!-- <button type="submit" class="btn btn-default">Cancel</button> -->
				                {{-- <button type="submit" class="btn btn-info">Submit</button> --}}
				                {!! Form::submit('Submit', ['class'=>'btn btn-info']); !!}
				              </div>
				              <!-- /.box-footer -->
				            {!! Form::close() !!}

							
						</div>
					</div>
				</div>
			</div>
		</section>

	@endsection


	@section('scripts')

		<script type="text/javascript">
			$(document).ready(function(){

				// Show/Hide Change Password
				$('#btnChangePassword').click(function(){
					if($('#change_password').is(':visible')){
						$('#old_password, #new_password').attr('disabled','disabled');
						$('#change_password').hide();
						$('#btnChangePassword').removeClass('btn-primary').addClass('btn-default');
					}else{
						$('#old_password, #new_password').prop('disabled', false);
						$('#change_password').show();
						$('#btnChangePassword').removeClass('btn-default').addClass('btn-primary');
					}
				});

				// Image Preview in Modal using global function
				$("#avatar").change(function(){
				    imagePreview(this, '.preview-img');
				});

				var user_type = "{!! $data['user_type'] !!}";

				if(user_type !== 'admin'){
					// Initialize the selectize control
					var province = $('#province').selectize();
					province[0].selectize.setValue("{!! $data['province_id'] !!}");

					var city = $('#city').selectize();
					city[0].selectize.setValue("{!! $data['city_id'] !!}");
					

					$('#phone').val("{!! $data['phone'] !!}");

					//Province on change
				    $("#province").change(function(){
				    	var province_id = $(this).val();

				    	getCity(province_id);			    	
					});

					//Get cities
					function getCity(province_id, selected=false, cityVal='') {
			    		$.get("{{ url('user/citylist') }}/" + province_id, function(data) {

					        if(data != ""){
					          	$("#city_wrap").html(data);
					        }
					    });

					    if(selected && cityVal!='') selectize_("#city", cityVal);
				    }

			    }
				
			});

		</script>
		
	@endsection