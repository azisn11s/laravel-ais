<!DOCTYPE html>
<html>
<head>
	
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Kajian Center</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap -->
	<link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="/assets/admin-lte/font-awesome/css/font-awesome.min.css" >
	<!-- Admin LTE -->
	<link rel="stylesheet" href="/assets/admin-lte/dist/css/AdminLTE_fonts.css">
	<link rel="stylesheet" href="/assets/admin-lte/dist/css/AdminLTE.min.css">
	<link rel="stylesheet" href="/assets/admin-lte/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>
<body class="layout-top-nav skin-black-light fixed">
	<div class="wrapper">
		<header class="main-header">
			<nav class="navbar navbar-static-top">
				<div class="container">
					<div class="navbar-header">
						<a href="../../index2.html" class="navbar-brand"><b>Kajian</b>Center</a>
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
							<i class="fa fa-bars"></i>
						</button>
					</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse pull-left" id="navbar-collapse">
					<ul class="nav navbar-nav">
						<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Dropdown <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#">Action</a></li>
								<li><a href="#">Another action</a></li>
								<li><a href="#">Something else here</a></li>
								<li class="divider"></li>
								<li><a href="#">Separated link</a></li>
								<li class="divider"></li>
								<li><a href="#">One more separated link</a></li>
							</ul>
						</li>
					</ul>
					<form class="navbar-form navbar-left" role="search">
						<div class="form-group">
							<input class="form-control" id="navbar-search-input" placeholder="Search" type="text">
						</div>
					</form>
				</div>
				<!-- /.navbar-collapse -->
				<!-- Navbar Right Menu -->
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<!-- Messages: style can be found in dropdown.less-->
						<li class="dropdown messages-menu">
							<!-- Menu toggle button -->
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="fa fa-envelope-o"></i>
								<span class="label label-success">4</span>
							</a>
							<ul class="dropdown-menu">
								<li class="header">You have 4 messages</li>
								<li>
									<!-- inner menu: contains the messages -->
									<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 200px;"><ul class="menu" style="overflow: hidden; width: 100%; height: 200px;">
									<li><!-- start message -->
										<a href="#">
											<div class="pull-left">
												<!-- User Image -->
												<img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
											</div>
											<!-- Message title and timestamp -->
											<h4>
											  Support Team
											  <small><i class="fa fa-clock-o"></i> 5 mins</small>
											</h4>
											<!-- The message -->
											<p>Why not buy a new awesome theme?</p>
										</a>
									</li>
									<!-- end message -->
									</ul><div class="slimScrollBar" style="background: rgb(0, 0, 0) none repeat scroll 0% 0%; width: 3px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div><div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div></div>
									<!-- /.menu -->
								</li>
								<li class="footer"><a href="#">See All Messages</a></li>
							</ul>
						</li>
						<!-- /.messages-menu -->

						<!-- Notifications Menu -->
						<li class="dropdown notifications-menu">
							<!-- Menu toggle button -->
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="fa fa-bell-o"></i>
								<span class="label label-warning">10</span>
							</a>
							<ul class="dropdown-menu">
								<li class="header">You have 10 notifications</li>
								<li>
								<!-- Inner Menu: contains the notifications -->
								<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 200px;"><ul class="menu" style="overflow: hidden; width: 100%; height: 200px;">
								<li><!-- start notification -->
								<a href="#">
								<i class="fa fa-users text-aqua"></i> 5 new members joined today
								</a>
								</li>
								<!-- end notification -->
								</ul><div class="slimScrollBar" style="background: rgb(0, 0, 0) none repeat scroll 0% 0%; width: 3px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div><div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div></div>
								</li>
								<li class="footer"><a href="#">View all</a></li>
							</ul>
						</li>
						<!-- User Account Menu -->
						<li class="dropdown user user-menu">
							<!-- Menu Toggle Button -->
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<!-- The user image in the navbar-->
								<img src="../../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
								<!-- hidden-xs hides the username on small devices so only the image appears. -->
								<span class="hidden-xs">Alexander Pierce</span>
							</a>
							<ul class="dropdown-menu">
								<!-- The user image in the menu -->
								<li class="user-header">
									<img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

									<p>
										Alexander Pierce - Web Developer
										<small>Member since Nov. 2012</small>
									</p>
								</li>
								<!-- Menu Body -->
								<li class="user-body">
									<div class="row">
										<div class="col-xs-4 text-center">
											<a href="#">Followers</a>
										</div>
										<div class="col-xs-4 text-center">
											<a href="#">Sales</a>
										</div>
										<div class="col-xs-4 text-center">
											<a href="#">Friends</a>
										</div>
									</div>
									<!-- /.row -->
								</li>

								<!-- Menu Footer-->
								<li class="user-footer">
									<div class="pull-left">
										<a href="#" class="btn btn-default btn-flat">Profile</a>
									</div>
									<div class="pull-right">
										<a href="#" class="btn btn-default btn-flat">Sign out</a>
									</div>
								</li>
							</ul>
						</li>
					</ul>
				</div>
				<!-- /.navbar-custom-menu -->
				</div>
				<!-- /.container-fluid -->
			</nav>
		</header>
	<div class="content-wrapper">
	<div class="container">

	<!-- Main content -->
	<section class="content">
		<div class="col-lg-6 col-lg-offset-3">
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Activity</a></li>
					<li class=""><a href="#timeline" data-toggle="tab" aria-expanded="false">Timeline</a></li>
					<li class=""><a href="#settings" data-toggle="tab" aria-expanded="false">Settings</a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="activity">
						<!-- Post -->
						<div class="post">
							<div class="user-block">
								<img class="img-circle img-bordered-sm" src="../../dist/img/user1-128x128.jpg" alt="user image">
								<span class="username">
									<a href="#">Jonathan Burke Jr.</a>
									<a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
								</span>
								<span class="description">Shared publicly - 7:30 PM today</span>
							</div>
							<!-- /.user-block -->
							<p>
								Lorem ipsum represents a long-held tradition for designers,
								typographers and the like. Some people hate it and argue for
								its demise, but others ignore the hate as they create awesome
								tools to help create filler text for everyone from bacon lovers
								to Charlie Sheen fans.
							</p>
							<ul class="list-inline">
								<li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Share</a></li>
								<li><a href="#" class="link-black text-sm"><i class="fa fa-thumbs-o-up margin-r-5"></i> Like</a>
								</li>
								<li class="pull-right">
								<a href="#" class="link-black text-sm"><i class="fa fa-comments-o margin-r-5"></i> Comments
								(5)</a></li>
							</ul>
							<input class="form-control input-sm" placeholder="Type a comment" type="text">
						</div>
						<!-- /.post -->

						<!-- Post -->
						<div class="post">
							<div class="user-block">
								<img class="img-circle img-bordered-sm" src="../../dist/img/user6-128x128.jpg" alt="User Image">
								<span class="username">
									<a href="#">Adam Jones</a>
									<a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
								</span>
								<span class="description">Posted 5 photos - 5 days ago</span>
							</div>
							<!-- /.user-block -->
							<div class="row margin-bottom">
								<div class="col-sm-6">
									<img class="img-responsive" src="../../dist/img/photo1.png" alt="Photo">
								</div>
								<!-- /.col -->
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-6">
											<img class="img-responsive" src="../../dist/img/photo2.png" alt="Photo">
											<br>
											<img class="img-responsive" src="../../dist/img/photo3.jpg" alt="Photo">
										</div>
										<!-- /.col -->
										<div class="col-sm-6">
											<img class="img-responsive" src="../../dist/img/photo4.jpg" alt="Photo">
											<br>
											<img class="img-responsive" src="../../dist/img/photo1.png" alt="Photo">
										</div>
										<!-- /.col -->
									</div>
									<!-- /.row -->
								</div>
								<!-- /.col -->
							</div>
							<!-- /.row -->

							<ul class="list-inline">
								<li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Share</a></li>
								<li><a href="#" class="link-black text-sm"><i class="fa fa-thumbs-o-up margin-r-5"></i> Like</a></li>
								<li class="pull-right">
									<a href="#" class="link-black text-sm"><i class="fa fa-comments-o margin-r-5"></i> Comments (5)</a>
								</li>
							</ul>
							<input class="form-control input-sm" placeholder="Type a comment" type="text">
						</div>
						<!-- /.post -->
					</div>
					<!-- /.tab-pane -->
					<div class="tab-pane" id="timeline">
						<!-- The timeline -->
						<ul class="timeline timeline-inverse">
							<!-- timeline time label -->
							<li class="time-label">
								<span class="bg-red">
									10 Feb. 2014
								</span>
							</li>
							<!-- /.timeline-label -->
							<!-- timeline item -->
							<li>
								<i class="fa fa-envelope bg-blue"></i>
								<div class="timeline-item">
									<span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
									<h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>
									<div class="timeline-body">
										Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
										weebly ning heekya handango imeem plugg dopplr jibjab, movity
										jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
										quora plaxo ideeli hulu weebly balihoo...
									</div>
									<div class="timeline-footer">
										<a class="btn btn-primary btn-xs">Read more</a>
										<a class="btn btn-danger btn-xs">Delete</a>
									</div>
								</div>
							</li>
							<!-- END timeline item -->
							<!-- timeline item -->
							<li>
								<i class="fa fa-user bg-aqua"></i>
								<div class="timeline-item">
									<span class="time"><i class="fa fa-clock-o"></i> 5 mins ago</span>
									<h3 class="timeline-header no-border"><a href="#">Sarah Young</a> accepted your friend request</h3>
								</div>
							</li>
							<!-- END timeline item -->
							<!-- timeline item -->
							<li>
								<i class="fa fa-comments bg-yellow"></i>
								<div class="timeline-item">
									<span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>

									<h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>
									<div class="timeline-body">
										Take me to your leader!
										Switzerland is small and neutral!
										We are more like Germany, ambitious and misunderstood!
									</div>
									<div class="timeline-footer">
										<a class="btn btn-warning btn-flat btn-xs">View comment</a>
									</div>
								</div>
							</li>
							<!-- END timeline item -->
							<!-- timeline time label -->
							<li class="time-label">
								<span class="bg-green">
									3 Jan. 2014
								</span>
							</li>
							<!-- /.timeline-label -->
							<!-- timeline item -->
							<li>
								<i class="fa fa-camera bg-purple"></i>

								<div class="timeline-item">
									<span class="time"><i class="fa fa-clock-o"></i> 2 days ago</span>

									<h3 class="timeline-header"><a href="#">Mina Lee</a> uploaded new photos</h3>

									<div class="timeline-body">
										<img src="http://placehold.it/150x100" alt="..." class="margin">
										<img src="http://placehold.it/150x100" alt="..." class="margin">
										<img src="http://placehold.it/150x100" alt="..." class="margin">
										<img src="http://placehold.it/150x100" alt="..." class="margin">
									</div>
								</div>
							</li>
							<!-- END timeline item -->
							<li>
								<i class="fa fa-clock-o bg-gray"></i>
							</li>
						</ul>
					</div>
					<!-- /.tab-pane -->

					<div class="tab-pane" id="settings">
					<form class="form-horizontal">
					<div class="form-group">
					<label for="inputName" class="col-sm-2 control-label">Name</label>

					<div class="col-sm-10">
					<input class="form-control" id="inputName" placeholder="Name" type="email">
					</div>
					</div>
					<div class="form-group">
					<label for="inputEmail" class="col-sm-2 control-label">Email</label>

					<div class="col-sm-10">
					<input class="form-control" id="inputEmail" placeholder="Email" type="email">
					</div>
					</div>
					<div class="form-group">
					<label for="inputName" class="col-sm-2 control-label">Name</label>

					<div class="col-sm-10">
					<input class="form-control" id="inputName" placeholder="Name" type="text">
					</div>
					</div>
					<div class="form-group">
					<label for="inputExperience" class="col-sm-2 control-label">Experience</label>

					<div class="col-sm-10">
					<textarea class="form-control" id="inputExperience" placeholder="Experience"></textarea>
					</div>
					</div>
					<div class="form-group">
					<label for="inputSkills" class="col-sm-2 control-label">Skills</label>

					<div class="col-sm-10">
					<input class="form-control" id="inputSkills" placeholder="Skills" type="text">
					</div>
					</div>
					<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
					<div class="checkbox">
					<label>
					<input type="checkbox"> I agree to the <a href="#">terms and conditions</a>
					</label>
					</div>
					</div>
					</div>
					<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-danger">Submit</button>
					</div>
					</div>
					</form>
					</div>
					<!-- /.tab-pane -->
				</div>
				<!-- /.tab-content -->
			</div>
		</div>
		<!-- /.content -->
		</div>
	</div>


	<!-- footer -->
	<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.8
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>
	</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/assets/jquery/jquery-3.1.1.min.js"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>

    <!-- SlimScroll | Mandatory when using fixed layout-->
	<script src="/assets/admin-lte/plugins/slimScroll/jquery.slimscroll.min.js"></script>

    <!-- AdminLTE App -->
    <script src="/assets/admin-lte/dist/js/app.min.js"></script>

    <!-- DataTables -->
    <script src="/assets/dataTables/js/jquery.dataTables.min.js"></script>
    <script src="/assets/dataTables/js/dataTables.bootstrap.min.js"></script>

    <!-- Selectize -->
    <script src="/assets/selectize/js/selectize.min.js"></script>

    <script src="/assets/custom/js/custom.js"></script>
    

    @yield('scripts')

</body>
</html>