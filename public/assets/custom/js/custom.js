  
 
$(document).ready(function () { 


  // confirm delete
  $(document.body).on('submit', '.js-confirm', function () {
    var $el = $(this)
    var text = $el.data('confirm') ? $el.data('confirm') : 'Anda yakin melakukan tindakan ini?'
    var c = confirm(text);
    return c;
  });

  // add selectize to select element
  $('.js-selectize').selectize({
    sortField: 'text'
  });

  // delete review book
  $(document.body).on('submit', '.js-review-delete', function () {
    var $el  = $(this);
    var text = $el.data('confirm') ? $el.data('confirm') : 'Anda yakin melakukan tindakan ini?';
    var c    = confirm(text);
    // cancel delete
    if (c === false) return c;

    // delete via ajax
    // disable behaviour default dari tombol submit
    event.preventDefault();
    // hapus data buku dengan ajax
    $.ajax({
      type     : 'POST',
      url      : $(this).attr('action'),
      dataType : 'json',
      data     : {
        _method : 'DELETE',
        // menambah csrf token dari Laravel
        _token  : $( this ).children( 'input[name=_token]' ).val()
      }
    }).done(function(data) {
      // cari baris yang dihapus
      baris = $('#form-'+data.id).closest('tr');
      // hilangkan baris (fadeout kemudian remove)
      baris.fadeOut(300, function() {$(this).remove()});
    });
  });

  //Data Mask
  $("[data-mask]").inputmask(); 

  //Format Datepicker
  $('input.datepicker').datepicker({
    autoclose: true,
    todayHighlight: true,
    format: 'dd/mm/yyyy' 
  });

  load_numeric();

});

function selectize_(id,data){
  var $select = $(id).selectize();
  var selectize = $select[0].selectize;
  selectize.setValue(data);
}

function combo_clear(id){
  $(id).find( 'select' ).each( function () {
      var $select = $( this ).selectize();
      var selectize = $select[0].selectize;
      selectize.clear(true);
  });
}


function hideModal(modalId, formId='') {
  $("#"+modalId).modal("hide");

  $("#"+modalId).on('hidden.bs.modal', function (e) {
    if(formId!='') $(':input', '#'+formId).not(':button').not(':submit').val('');

    if($("#"+modalId).has('img')){
      $('img.preview-img').attr('src','').hide();
    }

  });
}

function load_numeric(){
    $(".numeric").numeric({ decimal : "." , negative : false });
}

function imagePreview(input, target) {

  if($('#div').is(':visible') == false)
    $(target).show();

  if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
          $(target).attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
  }
}

function dateSqlToDatepicker(date) {
      var sqlDate = "";

      var dateString = String(date);

      var day = dateString.substr(8, 2);
      var month = dateString.substr(5, 2);
      var year = dateString.substr(0, 4);

      sqlDate = day + "/" + month + "/" + year;

      return sqlDate;
}












