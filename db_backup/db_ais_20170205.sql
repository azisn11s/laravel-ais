-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.21-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table app_template.app_profiles
CREATE TABLE IF NOT EXISTS `app_profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `app_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `jargon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `year` int(11) DEFAULT NULL,
  `app_icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `app_version` char(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table app_template.app_profiles: ~0 rows (approximately)
/*!40000 ALTER TABLE `app_profiles` DISABLE KEYS */;
INSERT INTO `app_profiles` (`id`, `app_name`, `jargon`, `company_name`, `year`, `app_icon`, `app_version`, `created_at`, `updated_at`) VALUES
	(1, 'A .I. S', 'The Best Web Administrator', 'HyBrings', 2017, NULL, '1.0.1', NULL, '2017-01-27 17:34:31');
/*!40000 ALTER TABLE `app_profiles` ENABLE KEYS */;

-- Dumping structure for table app_template.menus
CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fa fa-circle-o',
  `sort` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `is_root` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table app_template.menus: ~19 rows (approximately)
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` (`id`, `name`, `url`, `uri`, `icon`, `sort`, `parent_id`, `is_root`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Administrator', NULL, 'administrator', 'fa fa-home', 100, 0, 1, 1, NULL, NULL),
	(2, 'Menu', '/administrator/menus', 'administrator/menus', 'fa fa-circle-o', 1, 1, 0, 1, NULL, NULL),
	(3, 'About', '/administrator/about', 'administrator/about', 'fa fa-circle-o', 2, 1, 0, 1, '2017-01-27 15:40:48', '2017-01-27 15:40:48'),
	(4, 'Pemohon', '', 'pemohon', 'fa fa-child', 1, 0, 1, 1, '2017-01-27 17:38:43', '2017-01-27 17:38:43'),
	(5, 'Input Data', '/pemohon/input-data', 'pemohon/input-data', 'fa fa-circle-o', 1, 4, 0, 1, '2017-01-27 17:40:48', '2017-01-27 17:40:48'),
	(6, 'Search Data', '/pemohon/search', 'pemohon/search', 'fa fa-circle-o', 1, 4, 0, 1, '2017-01-27 17:42:58', '2017-01-27 17:42:58'),
	(7, 'Buku Tanah', '', 'buku-tanah', 'fa fa-book', 2, 0, 1, 1, '2017-01-27 17:44:25', '2017-01-27 17:44:25'),
	(8, 'Transaksi Peminjaman', '/buku-tanah/transaksi-peminjaman', 'buku-tanah/transaksi-peminjaman', 'fa fa-circle-o', 1, 7, 0, 1, '2017-01-27 17:45:22', '2017-01-27 17:45:22'),
	(9, 'Transaksi Pengembalian', '/buku-tanah/transaksi-pengembalian', 'buku-tanah/transaksi-pengembalian', 'fa fa-circle-o', 2, 7, 0, 1, '2017-01-27 17:46:37', '2017-01-27 17:46:37'),
	(10, 'Search Data', '/buku-tanah/search-data', 'buku-tanah/search-data', 'fa fa-circle-o', 3, 7, 0, 1, '2017-01-27 17:49:06', '2017-01-27 17:49:06'),
	(11, 'Warkah', '', 'warkah', 'fa fa-file-archive-o', 3, 0, 1, 1, '2017-01-27 17:50:35', '2017-01-27 17:50:35'),
	(12, 'Input Data', '/warkah/input-data', 'warkah/input-data', 'fa fa-circle-o', 1, 11, 0, 1, '2017-01-27 17:51:52', '2017-01-27 17:51:52'),
	(13, 'Input Album Warkah', '/warkah/input-album-warkah', 'warkah/input-album-warkah', 'fa fa-circle-o', 2, 11, 0, 1, '2017-01-27 17:53:42', '2017-01-27 17:53:42'),
	(14, 'Transaksi Peminjaman', '/warkah/transaksi-peminjaman', 'warkah/transaksi-peminjaman', 'fa fa-circle-o', 3, 11, 0, 1, '2017-01-27 17:54:35', '2017-01-27 17:54:35'),
	(15, 'Transaksi Pengembalian', '/warkah/transaksi-pengembalian', 'warkah/transaksi-pengembalian', 'fa fa-circle-o', 4, 11, 0, 1, '2017-01-27 17:55:58', '2017-01-27 17:55:58'),
	(16, 'Search Data', '/warkah/search-data', 'warkah/search-data', 'fa fa-circle-o', 5, 11, 0, 1, '2017-01-27 17:56:41', '2017-01-27 17:56:41'),
	(17, 'Utility', '', 'utility', 'fa fa-suitcase', 4, 0, 1, 1, '2017-01-27 17:59:17', '2017-01-27 17:59:17'),
	(18, 'Data Desa', '/utility/data-desa', 'utility/data-desa', 'fa fa-circle-o', 1, 17, 0, 1, '2017-01-27 18:00:05', '2017-01-27 18:00:05'),
	(19, 'Data Kecamatan', '/utility/data-kecamatan', 'utility/data-kecamatan', 'fa fa-circle-o', 2, 17, 0, 1, '2017-01-27 18:00:39', '2017-01-27 18:00:39');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;

-- Dumping structure for table app_template.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table app_template.migrations: ~2 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`migration`, `batch`) VALUES
	('2017_01_14_150618_create_menus_table', 1),
	('2017_01_27_094016_create_app_profiles_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
