-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.21-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table db_ais.activations
CREATE TABLE IF NOT EXISTS `activations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_ais.app_profiles
CREATE TABLE IF NOT EXISTS `app_profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `app_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `jargon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `year` int(11) DEFAULT NULL,
  `app_icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `app_version` char(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_ais.log_detail_kembali_buku_tanah
CREATE TABLE IF NOT EXISTS `log_detail_kembali_buku_tanah` (
  `nomor_kembali` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `buku_tanah_id` bigint(20) unsigned NOT NULL,
  `jumlah_buku` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `log_detail_kembali_buku_tanah_nomor_kembali_foreign` (`nomor_kembali`),
  KEY `log_detail_kembali_buku_tanah_buku_tanah_id_foreign` (`buku_tanah_id`),
  CONSTRAINT `log_detail_kembali_buku_tanah_buku_tanah_id_foreign` FOREIGN KEY (`buku_tanah_id`) REFERENCES `mst_buku_tanah` (`id`),
  CONSTRAINT `log_detail_kembali_buku_tanah_nomor_kembali_foreign` FOREIGN KEY (`nomor_kembali`) REFERENCES `log_kembali` (`nomor_kembali`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_ais.log_detail_kembali_warkah
CREATE TABLE IF NOT EXISTS `log_detail_kembali_warkah` (
  `nomor_kembali` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `warkah_id` bigint(20) unsigned NOT NULL,
  `jumlah_buku` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `log_detail_kembali_warkah_nomor_kembali_foreign` (`nomor_kembali`),
  KEY `log_detail_kembali_warkah_warkah_id_foreign` (`warkah_id`),
  CONSTRAINT `log_detail_kembali_warkah_nomor_kembali_foreign` FOREIGN KEY (`nomor_kembali`) REFERENCES `log_kembali` (`nomor_kembali`),
  CONSTRAINT `log_detail_kembali_warkah_warkah_id_foreign` FOREIGN KEY (`warkah_id`) REFERENCES `mst_warkah` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_ais.log_detail_pinjam_buku_tanah
CREATE TABLE IF NOT EXISTS `log_detail_pinjam_buku_tanah` (
  `nomor_pinjam` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `buku_tanah_id` bigint(20) unsigned NOT NULL,
  `jumlah_buku` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `log_detail_pinjam_buku_tanah_nomor_pinjam_foreign` (`nomor_pinjam`),
  KEY `log_detail_pinjam_buku_tanah_buku_tanah_id_foreign` (`buku_tanah_id`),
  CONSTRAINT `log_detail_pinjam_buku_tanah_buku_tanah_id_foreign` FOREIGN KEY (`buku_tanah_id`) REFERENCES `mst_buku_tanah` (`id`),
  CONSTRAINT `log_detail_pinjam_buku_tanah_nomor_pinjam_foreign` FOREIGN KEY (`nomor_pinjam`) REFERENCES `log_pinjam` (`nomor_pinjam`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_ais.log_detail_pinjam_warkah
CREATE TABLE IF NOT EXISTS `log_detail_pinjam_warkah` (
  `nomor_pinjam` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `warkah_id` bigint(20) unsigned NOT NULL,
  `jumlah_buku` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `log_detail_pinjam_warkah_nomor_pinjam_foreign` (`nomor_pinjam`),
  KEY `log_detail_pinjam_warkah_warkah_id_foreign` (`warkah_id`),
  CONSTRAINT `log_detail_pinjam_warkah_nomor_pinjam_foreign` FOREIGN KEY (`nomor_pinjam`) REFERENCES `log_pinjam` (`nomor_pinjam`),
  CONSTRAINT `log_detail_pinjam_warkah_warkah_id_foreign` FOREIGN KEY (`warkah_id`) REFERENCES `mst_warkah` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_ais.log_kembali
CREATE TABLE IF NOT EXISTS `log_kembali` (
  `nomor_kembali` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal_kembali` datetime NOT NULL,
  `total_kembali` int(11) NOT NULL,
  `pemohon_id` bigint(20) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`nomor_kembali`),
  KEY `log_kembali_pemohon_id_foreign` (`pemohon_id`),
  KEY `log_kembali_user_id_foreign` (`user_id`),
  CONSTRAINT `log_kembali_pemohon_id_foreign` FOREIGN KEY (`pemohon_id`) REFERENCES `mst_pemohon` (`id`),
  CONSTRAINT `log_kembali_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_ais.log_pinjam
CREATE TABLE IF NOT EXISTS `log_pinjam` (
  `nomor_pinjam` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal_pinjam` datetime NOT NULL,
  `total_pinjam` int(11) NOT NULL,
  `pemohon_id` bigint(20) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`nomor_pinjam`),
  KEY `log_pinjam_pemohon_id_foreign` (`pemohon_id`),
  KEY `log_pinjam_user_id_foreign` (`user_id`),
  CONSTRAINT `log_pinjam_pemohon_id_foreign` FOREIGN KEY (`pemohon_id`) REFERENCES `mst_pemohon` (`id`),
  CONSTRAINT `log_pinjam_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_ais.menus
CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fa fa-circle-o',
  `sort` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `is_root` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_ais.menu_role
CREATE TABLE IF NOT EXISTS `menu_role` (
  `role_id` int(10) unsigned NOT NULL,
  `menu_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`menu_id`),
  KEY `menu_role_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_role_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `menu_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_ais.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_ais.mst_album
CREATE TABLE IF NOT EXISTS `mst_album` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `no_album` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `lemari` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rak` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blok` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `record_status` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mst_album_no_album_unique` (`no_album`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_ais.mst_buku_tanah
CREATE TABLE IF NOT EXISTS `mst_buku_tanah` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nb_barcode` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `hak_id` int(10) unsigned NOT NULL,
  `no_hak` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `album_id` int(10) unsigned DEFAULT NULL,
  `luas` int(11) NOT NULL,
  `pemegang_hak` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `desa_id` int(10) unsigned NOT NULL,
  `status` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `record_status` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mst_buku_tanah_nb_barcode_unique` (`nb_barcode`),
  KEY `mst_buku_tanah_desa_id_foreign` (`desa_id`),
  KEY `mst_buku_tanah_album_id_foreign` (`album_id`),
  KEY `mst_buku_tanah_hak_id_foreign` (`hak_id`),
  CONSTRAINT `mst_buku_tanah_album_id_foreign` FOREIGN KEY (`album_id`) REFERENCES `mst_album` (`id`),
  CONSTRAINT `mst_buku_tanah_desa_id_foreign` FOREIGN KEY (`desa_id`) REFERENCES `mst_desa` (`id`),
  CONSTRAINT `mst_buku_tanah_hak_id_foreign` FOREIGN KEY (`hak_id`) REFERENCES `mst_jenis_hak` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_ais.mst_desa
CREATE TABLE IF NOT EXISTS `mst_desa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kode_desa` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `nama_desa` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `record_status` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A',
  `id_kecamatan` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mst_desa_kode_desa_unique` (`kode_desa`),
  KEY `mst_desa_id_kecamatan_foreign` (`id_kecamatan`),
  CONSTRAINT `mst_desa_id_kecamatan_foreign` FOREIGN KEY (`id_kecamatan`) REFERENCES `mst_kecamatan` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_ais.mst_gender
CREATE TABLE IF NOT EXISTS `mst_gender` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kode_jekel` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `nama_jekel` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `record_status` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_ais.mst_jabatan
CREATE TABLE IF NOT EXISTS `mst_jabatan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kode_jabatan` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `nama_jabatan` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `record_status` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mst_jabatan_kode_jabatan_unique` (`kode_jabatan`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_ais.mst_jenis_hak
CREATE TABLE IF NOT EXISTS `mst_jenis_hak` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kode_hak` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `nama_hak` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `record_status` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_ais.mst_kecamatan
CREATE TABLE IF NOT EXISTS `mst_kecamatan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kode_kecamatan` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `nama_kecamatan` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `record_status` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mst_kecamatan_kode_kecamatan_unique` (`kode_kecamatan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_ais.mst_pemohon
CREATE TABLE IF NOT EXISTS `mst_pemohon` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nip` char(35) COLLATE utf8_unicode_ci NOT NULL,
  `nama_lengkap` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `jekel_id` int(10) unsigned NOT NULL,
  `jabatan_id` int(10) unsigned DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `record_status` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mst_pemohon_nip_unique` (`nip`),
  KEY `mst_pemohon_jekel_id_foreign` (`jekel_id`),
  KEY `mst_pemohon_jabatan_id_foreign` (`jabatan_id`),
  CONSTRAINT `mst_pemohon_jabatan_id_foreign` FOREIGN KEY (`jabatan_id`) REFERENCES `mst_jabatan` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `mst_pemohon_jekel_id_foreign` FOREIGN KEY (`jekel_id`) REFERENCES `mst_gender` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_ais.mst_wallboard
CREATE TABLE IF NOT EXISTS `mst_wallboard` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `berita` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_ais.mst_warkah
CREATE TABLE IF NOT EXISTS `mst_warkah` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nw_barcode` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `jenis` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_warkah` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `tahun` char(4) COLLATE utf8_unicode_ci NOT NULL,
  `album_id` int(10) unsigned NOT NULL,
  `status` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `record_status` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mst_warkah_nw_barcode_unique` (`nw_barcode`),
  KEY `mst_warkah_album_id_foreign` (`album_id`),
  CONSTRAINT `mst_warkah_album_id_foreign` FOREIGN KEY (`album_id`) REFERENCES `mst_album` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_ais.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_ais.persistences
CREATE TABLE IF NOT EXISTS `persistences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `persistences_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_ais.reminders
CREATE TABLE IF NOT EXISTS `reminders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_ais.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_ais.role_users
CREATE TABLE IF NOT EXISTS `role_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_ais.throttle
CREATE TABLE IF NOT EXISTS `throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_ais.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('super-admin','admin','operator','peminjaman','divisi-lain') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'admin',
  `permissions` text COLLATE utf8_unicode_ci,
  `avatar` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_username_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
